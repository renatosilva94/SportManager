<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Times extends Model
{

    use SoftDeletes;

    public $timestamps = false;
    protected $fillable = array('nome_time', 'usuario_id', 'vitoria', 'empate', 'derrota');

    public function jogadores()
    {
        return $this->belongsToMany('App\Jogadores', 'time_jogador');
    }

    public function ligas()
    {
        return $this->belongsToMany('App\Ligas', 'liga_time');
    }

}
