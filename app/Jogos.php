<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Jogos extends Model
{

    use SoftDeletes;


    public $timestamps = false;
    protected $fillable = array('usuario_id', 'data_jogo', 'placar_time_casa', 'placar_time_adversario', 'time_adversario_id', 'time_casa_id', 'liga_id');

    public function time_adversario_nome()
    {
        return $this->hasOne('App\Times', 'id', 'time_adversario_id');
    }

    public function time_casa_nome()
    {
        return $this->hasOne('App\Times', 'id', 'time_casa_id');
    }

    public function ligas()
    {
        return $this->hasOne('App\Ligas', 'id', 'liga_id');
    }

    public function jogadores()
    {
        return $this->belongsToMany('App\Jogadores', 'jogo_jogador')->withPivot('gols', 'cartao_amarelo', 'cartao_vermelho', 'ausente');
    }

}
