<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mail;
use App\Jogos;
use App\Jogadores;
use Illuminate\Support\Facades\DB;


class EnviarEmailJogadoresCasa extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:convidar_casa';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Convida todos os jogadores da partida via e-mail';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $jogadores = DB::table('jogos')
        ->join('times', 'jogos.time_casa_id', '=', 'times.id')
        ->join('time_jogador', 'times.id', '=', 'time_jogador.times_id')
        ->join('jogadores', 'jogadores.id', '=', 'time_jogador.jogadores_id')
        ->whereMonth('jogos.data_jogo', '=', date('m'))->whereDay('jogos.data_jogo', '=', date('d'))
        ->select('jogadores.*', 'times.*', 'jogos.*')->get();     

        //dd($jogadores);      

// foreach($jogos as $jogo) {
foreach($jogadores as $jogador) {
    
    // Send the email to user
    Mail::send('convidar_jogadores_email', ['jogador' => $jogador],  function ($mail) use ($jogador) {
        $mail->to($jogador->email)
            ->from('aviso@sportmanager.io', 'Avisos SportManager')
            ->subject('Jogo em Breve! (SportManager)');
    });
//}


}
 
    $this->info('Aviso enviado aos jogadores do time da casa!');




























    }
}
