<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Usuarios;
use Mail;


class EnviarEmailUsuarios extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:aniversario';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Mensagem de Aniversário';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $usuarios = Usuarios::whereMonth('data_nascimento', '=', date('m'))->whereDay('data_nascimento', '=', date('d'))->get();//->whereYear('data_jogo', '=', date('y'))->get();
 
    foreach($usuarios as $usuario) {
 
 
        
        // Send the email to user
        Mail::send('aniversario_usuarios_email', ['usuario' => $usuario], function ($mail) use ($usuario) {
            $mail->to($usuario['email'])
                ->from('aviso@sportmanager.io', 'Avisos SportManager')
                ->subject('Feliz Aniversário');
        });
 
    }
 
    $this->info('Mensagem Enviada!');
    }
}
