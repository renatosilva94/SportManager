<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Usuarios;
use App\Eventos;
use Mail;


class EnviarEmailLembreteEvento extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:lembrete_evento';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envia um e-mail lembrando o usuário dos seus eventos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $eventos = DB::table('eventos')
        ->join('usuarios', 'eventos.usuario_id', '=', 'usuarios.id')
        ->whereMonth('eventos.data_inicial', '=', date('m'))->whereDay('eventos.data_inicial', '=', date('d'))->whereYear('eventos.data_inicial', '=', date('Y'))
        ->select('eventos.*', 'usuarios.*')->get(); 


        foreach($eventos as $evento) {
     
     
            
            // Send the email to user
            Mail::send('lembrete_usuarios_email', ['evento' => $evento], function ($mail) use ($evento) {
                $mail->to($evento->email)
                    ->from('aviso@sportmanager.io', 'Avisos SportManager')
                    ->subject('Lembrete de Evento');
            });
     
        }
     
        $this->info('Mensagem Enviada!');
    }
}
