<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
                Commands\EnviarEmailJogadoresCasa::class,
                Commands\EnviarEmailJogadoresAdversario::class,
                Commands\EnviarEmailUsuarios::class,
                Commands\EnviarEmailLembreteEvento::class,


    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
                $schedule->command('email:convidar_casa')->dailyAt('00:01');
                $schedule->command('email:aniversario')->dailyAt('00:01');
                $schedule->command('email:convidar_adversario')->dailyAt('00:01');
                $schedule->command('email:lembrete_evento')->dailyAt('00:01');

                                

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
