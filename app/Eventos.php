<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Eventos extends Model
{

    public $timestamps = false;

    protected $fillable = array('nome_evento', 'data_inicial', 'data_final', 'mensagem', 'usuario_id');

   

}
