<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Jogadores extends Model
{

    use SoftDeletes;


    public $timestamps = false;
    protected $fillable = array('ausente','nome', 'sobrenome', 'funcao', 'email', 'rg', 'cpf', 'cartao_vermelho', 'gols', 'cartao_amarelo', 'usuario_id');

    public function times()
    {
        return $this->belongsToMany('App\Times', 'time_jogador');
    }

    public function ligas()
    {
        return $this->hasOne('App\Ligas', 'id', 'liga_id');
    }

    public function jogos()
    {
        return $this->belongsToMany('App\Jogos', 'jogo_jogador')->withPivot('gols', 'cartao_amarelo', 'cartao_vermelho', 'ausente');
    }

}
