<?php

namespace App;

//use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class Usuarios extends Authenticable
{

    use Notifiable, HasApiTokens;

    public $timestamps = false;

    protected $fillable = array('provider', 'provider_id', 'nome', 'sobrenome', 'email', 'password', 'avatar', 'data_nascimento', 'sexo_id');

    protected $hidden = array('password', 'remember_token');

    public function sexos()
    {
        return $this->hasOne('App\Sexos');
    }

}
