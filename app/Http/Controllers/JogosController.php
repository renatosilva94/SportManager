<?php

namespace App\Http\Controllers;

use App\Jogadores;
use App\Jogos;
use App\Ligas;
use App\Times;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Sexos;
use \View;

class JogosController extends Controller {

    public function paginaListaJogos() {

        if (!auth()->guard()->user()) {
            return redirect('pagina.login');
        }




        //$nome_liga = Ligas::find($id)->nome_liga;



        $usuario_autenticado_id = Auth::guard()->user()->id;
        $usuario_autenticado_nome = Auth::guard()->user()->nome;

        $jogadores = Jogadores::orderBy('nome')->where('usuario_id', $usuario_autenticado_id)->where('deleted_at', null)->get();

        $ligas = Ligas::orderBy('nome_liga')->where('usuario_id', $usuario_autenticado_id)->where('deleted_at', null)->get();
        $times = Times::orderBy('nome_time')->where('usuario_id', $usuario_autenticado_id)->where('deleted_at', null)->get();

        //$shuffleTimes = Times::all();

        $jogos = Jogos::where('usuario_id', $usuario_autenticado_id)->where('deleted_at', null)->paginate(5);

        $eventosNotificacoes = DB::table('eventos')->where('usuario_id', $usuario_autenticado_id)->whereDate('data_inicial', Carbon::today())->get();
        $eventosNotificacoesContagem = DB::table('eventos')->where('usuario_id', $usuario_autenticado_id)->whereDate('data_inicial', Carbon::today())->count();

        $sexos = Sexos::orderBy('nome_sexo')->get();


        return view('lista_jogos', compact('sexos', 'eventosNotificacoes', 'eventosNotificacoesContagem', 'nome_liga', 'id', 'shuffleTimes2', 'shuffleTimes', 'ligas', 'jogadores', 'subligas', 'usuario_autenticado_id', 'usuario_autenticado_nome', 'times', 'jogos'));
    }

    public function salvarJogo(Request $request) {

        if (!auth()->guard()->user()) {
            return redirect('pagina.login');
        }

        $request->validate([
            'data_jogo' => 'required',
            'placatar_time_casa' => 'integer',
            'placar_time_adversario' => 'integer',
            'time_casa_id' => 'required',
            'time_adversario_id' => 'required',
            'liga_id' => 'required',    
            'cartao_amarelo' => 'integer|between:0,2',
            'cartao_vermelho' => 'integer|between:0,1',
        ]);


        $usuario_autenticado_id = Auth::guard()->user()->id;

        $jogos = Jogos::create([
                    'data_jogo' => $request['data_jogo'],
                    'time_casa_id' => $request['time_casa_id'],
                    'placar_time_casa' => $request['placar_time_casa'],
                    'time_adversario_id' => $request['time_adversario_id'],
                    'placar_time_adversario' => $request['placar_time_adversario'],
                    'liga_id' => $request['liga_id'],
                    'usuario_id' => $usuario_autenticado_id,
        ]);

        if ($jogos) {




            $gols_pro_time_casa = DB::table('placares')
                    ->where('time_id', $request['time_casa_id'])->where('liga_id', $request['liga_id'])
                    ->increment('gols_pro', $request['placar_time_casa']);

            $gols_pro_time_adversario = DB::table('placares')
                    ->where('time_id', $request['time_adversario_id'])->where('liga_id', $request['liga_id'])
                    ->increment('gols_pro', $request['placar_time_adversario']);


            $gols_contra_time_casa = DB::table('placares')
                    ->where('time_id', $request['time_casa_id'])->where('liga_id', $request['liga_id'])
                    ->increment('gols_contra', $request['placar_time_adversario']);

            $gols_contra_time_adversario = DB::table('placares')
                    ->where('time_id', $request['time_adversario_id'])->where('liga_id', $request['liga_id'])
                    ->increment('gols_contra', $request['placar_time_casa']);

            /* Contador do Total de Jogos do time da casa */

            $jogos_time_casa = DB::table('placares')
                    ->where('time_id', $request['time_casa_id'])->where('liga_id', $request['liga_id'])
                    ->increment('jogos', 1);

            /* Contador do Total de Jogos do time adversário */

            $jogos_time_adversario = DB::table('placares')
                    ->where('time_id', $request['time_adversario_id'])->where('liga_id', $request['liga_id'])
                    ->increment('jogos', 1);

            if ($request['placar_time_casa'] == $request['placar_time_adversario']) {

                /* Pontos em caso de empate = 1 para cada time */

                $pontos_time_casa = DB::table('placares')
                        ->where('time_id', $request['time_casa_id'])->where('liga_id', $request['liga_id'])
                        ->increment('pontos', 1);

                $pontos_time_adversario = DB::table('placares')
                        ->where('time_id', $request['time_adversario_id'])->where('liga_id', $request['liga_id'])
                        ->increment('pontos', 1);

                /* Contador de empate de cada time */

                $empates_time_casa = DB::table('placares')
                        ->where('time_id', $request['time_casa_id'])->where('liga_id', $request['liga_id'])
                        ->increment('empates', 1);

                $empates_time_adversario = DB::table('placares')
                        ->where('time_id', $request['time_adversario_id'])->where('liga_id', $request['liga_id'])
                        ->increment('empates', 1);

                $adicionarEmpateTimeCasa = DB::table('times')
                                ->where('id', $request['time_casa_id'])->increment('empate', 1);

                $adicionarEmpateTimeAdversario = DB::table('times')
                                ->where('id', $request['time_adversario_id'])->increment('empate', 1);
            } else if ($request['placar_time_casa'] > $request['placar_time_adversario']) {

                /* Pontos para o time da casa caso o seu placar for maior que o seu adversário = 3 */

                $pontos_time_casa = DB::table('placares')
                        ->where('time_id', $request['time_casa_id'])->where('liga_id', $request['liga_id'])
                        ->increment('pontos', 3);

                /* Contador de vitórias do time da casa */

                $vitorias_time_casa = DB::table('placares')
                        ->where('time_id', $request['time_casa_id'])->where('liga_id', $request['liga_id'])
                        ->increment('vitorias', 1);

                /* Contador de derrotas do time adversário */

                $derrotas_time_adversario = DB::table('placares')
                        ->where('time_id', $request['time_adversario_id'])->where('liga_id', $request['liga_id'])
                        ->increment('derrotas', 1);

                $adicionarVitoriaTimeCasa = DB::table('times')
                                ->where('id', $request['time_casa_id'])->increment('vitoria', 1);

                $adicionarDerrotaTimeAdversario = DB::table('times')
                                ->where('id', $request['time_adversario_id'])->increment('derrota', 1);
            } else {

                /* Pontos para o time adversario caso o seu placar for maior que o da casa = 3 */

                $pontos_time_adversario = DB::table('placares')
                        ->where('time_id', $request['time_adversario_id'])->where('liga_id', $request['liga_id'])
                        ->increment('pontos', 3);

                /* Contador de vitórias do time da adversario */

                $vitorias_time_adversario = DB::table('placares')
                        ->where('time_id', $request['time_adversario_id'])->where('liga_id', $request['liga_id'])
                        ->increment('vitorias', 1);

                /* Contador de derrotas do time da casa */

                $derrotas_time_casa = DB::table('placares')
                        ->where('time_id', $request['time_casa_id'])->where('liga_id', $request['liga_id'])
                        ->increment('derrotas', 1);

                $adicionarDerrotaTimeCasa = DB::table('times')
                                ->where('id', $request['time_casa_id'])->increment('derrota', 1);

                $adicionarVitoriaTimeAdversario = DB::table('times')
                                ->where('id', $request['time_adversario_id'])->increment('vitoria', 1);
            }

           $jogadoresId = $request->input('jogadorescasa.*.jogador_id');
            $jogadoresGols = $request->input('jogadorescasa.*.gols');
$jogadoresCartaoAmarelo = $request->input('jogadorescasa.*.cartao_amarelo');
$jogadoresCartaoVermelho = $request->input('jogadorescasa.*.cartao_vermelho');
$jogadoresAusente = $request->input('jogadorescasa.*.ausente');


            for ($i = 0; $i < count($jogadoresId); $i++) {

        if($jogadoresId[$i]==0){

        }else{

                DB::table('jogadores')->where('id', $jogadoresId[$i])->increment('gols', $jogadoresGols[$i]);

                DB::table('jogadores')->where('id', $jogadoresId[$i])->increment('cartao_amarelo', $jogadoresCartaoAmarelo[$i]);

                DB::table('jogadores')->where('id', $jogadoresId[$i])->increment('cartao_vermelho', $jogadoresCartaoVermelho[$i]);

                DB::table('jogadores')->where('id', $jogadoresId[$i])->increment('ausente', $jogadoresAusente[$i]);



                $jogos->jogadores()->attach($jogadoresId[$i], ['gols' => $jogadoresGols[$i], 'cartao_amarelo' => $jogadoresCartaoAmarelo[$i], 'cartao_vermelho' => $jogadoresCartaoVermelho[$i], 'ausente' => $jogadoresAusente[$i]]);
        }
            }


 $jogadoresId2 = $request->input('jogadoresadversario.*.jogador_id');
            $jogadoresGols2 = $request->input('jogadoresadversario.*.gols');
$jogadoresCartaoAmarelo2 = $request->input('jogadoresadversario.*.cartao_amarelo');
$jogadoresCartaoVermelho2 = $request->input('jogadoresadversario.*.cartao_vermelho');
$jogadoresAusente2 = $request->input('jogadoresadversario.*.ausente');


            for ($i = 0; $i < count($jogadoresId); $i++) {

        if($jogadoresId2[$i]==0){

        }else{

                DB::table('jogadores')->where('id', $jogadoresId2[$i])->increment('gols', $jogadoresGols2[$i]);

                DB::table('jogadores')->where('id', $jogadoresId2[$i])->increment('cartao_amarelo', $jogadoresCartaoAmarelo2[$i]);

                DB::table('jogadores')->where('id', $jogadoresId2[$i])->increment('cartao_vermelho', $jogadoresCartaoVermelho2[$i]);

                DB::table('jogadores')->where('id', $jogadoresId2[$i])->increment('ausente', $jogadoresAusente2[$i]);



                $jogos->jogadores()->attach($jogadoresId2[$i], ['gols' => $jogadoresGols2[$i], 'cartao_amarelo' => $jogadoresCartaoAmarelo2[$i], 'cartao_vermelho' => $jogadoresCartaoVermelho2[$i], 'ausente' => $jogadoresAusente2[$i]]);
        }
            }

           
                


            alert()->success('Jogo Cadastrado Com Sucesso.');


            return redirect()->route('pagina.inicial', compact('ligas', 'times', 'usuario_autenticado_nome', 'usuario_autenticado_id'));
        }
    }

    public function desfazerJogoSalvar(Request $request, $id) {

        if (!auth()->guard()->user()) {
            return redirect('pagina.login');
        }



        $this->validate($request, [
            'data_jogo' => 'required',
            'liga_id' => 'required',
            'time_casa_id' => 'required',
            'placar_time_casa' => 'required',
            'time_adversario_id' => 'required',
            'placar_time_adversario' => 'required',
        ]);

        $dados = $request->all();

        $jogoId = Jogos::find($id);

        $alt = $jogoId->update($dados);

        if ($alt) {

            /* Gols PRO - TIME DA CASA */

            $gols_pro_time_casa = DB::table('placares')
                    ->where('time_id', $request['time_casa_id'])->where('liga_id', $request['liga_id'])
                    ->decrement('gols_pro', $request['placar_time_casa']);

            $gols_pro_time_adversario = DB::table('placares')
                    ->where('time_id', $request['time_adversario_id'])->where('liga_id', $request['liga_id'])
                    ->decrement('gols_pro', $request['placar_time_adversario']);

            /* Gols CONTRA - TIME ADVERSARIO */

            $gols_contra_time_casa = DB::table('placares')
                    ->where('time_id', $request['time_casa_id'])->where('liga_id', $request['liga_id'])
                    ->decrement('gols_contra', $request['placar_time_adversario']);

            $gols_contra_time_adversario = DB::table('placares')
                    ->where('time_id', $request['time_adversario_id'])->where('liga_id', $request['liga_id'])
                    ->decrement('gols_contra', $request['placar_time_casa']);

            if ($request['placar_time_casa'] == $request['placar_time_adversario']) {

                /* Pontos em caso de empate = 1 para cada time */

                $pontos_time_casa = DB::table('placares')
                        ->where('time_id', $request['time_casa_id'])->where('liga_id', $request['liga_id'])
                        ->decrement('pontos', 1);

                $pontos_time_adversario = DB::table('placares')
                        ->where('time_id', $request['time_adversario_id'])->where('liga_id', $request['liga_id'])
                        ->decrement('pontos', 1);

                /* Contador de empate de cada time */

                $empates_time_casa = DB::table('placares')
                        ->where('time_id', $request['time_casa_id'])->where('liga_id', $request['liga_id'])
                        ->decrement('empates', 1);

                $empates_time_adversario = DB::table('placares')
                        ->where('time_id', $request['time_adversario_id'])->where('liga_id', $request['liga_id'])
                        ->decrement('empates', 1);


                $adicionarEmpateTimeCasa = DB::table('times')
                                ->where('id', $request['time_casa_id'])->decrement('empate', 1);

                $adicionarEmpateTimeAdversario = DB::table('times')
                                ->where('id', $request['time_adversario_id'])->decrement('empate', 1);
            } else if ($request['placar_time_casa'] > $request['placar_time_adversario']) {

                /* Pontos para o time da casa caso o seu placar for maior que o seu adversário = 3 */

                $pontos_time_casa = DB::table('placares')
                        ->where('time_id', $request['time_casa_id'])->where('liga_id', $request['liga_id'])
                        ->decrement('pontos', 3);

                /* Contador de vitórias do time da casa */

                $vitorias_time_casa = DB::table('placares')
                        ->where('time_id', $request['time_casa_id'])->where('liga_id', $request['liga_id'])
                        ->decrement('vitorias', 1);

                /* Contador de derrotas do time adversário */

                $derrotas_time_adversario = DB::table('placares')
                        ->where('time_id', $request['time_adversario_id'])->where('liga_id', $request['liga_id'])
                        ->decrement('derrotas', 1);

                $adicionarVitoriaTimeCasa = DB::table('times')
                                ->where('id', $request['time_casa_id'])->decrement('vitoria', 1);

                $adicionarDerrotaTimeAdversario = DB::table('times')
                                ->where('id', $request['time_adversario_id'])->decrement('derrota', 1);
            } else {

                /* Pontos para o time adversario caso o seu placar for maior que o da casa = 3 */

                $pontos_time_adversario = DB::table('placares')
                        ->where('time_id', $request['time_adversario_id'])->where('liga_id', $request['liga_id'])
                        ->decrement('pontos', 3);

                /* Contador de vitórias do time da adversario */

                $vitorias_time_adversario = DB::table('placares')
                        ->where('time_id', $request['time_adversario_id'])->where('liga_id', $request['liga_id'])
                        ->decrement('vitorias', 1);

                /* Contador de derrotas do time da casa */

                $derrotas_time_casa = DB::table('placares')
                        ->where('time_id', $request['time_casa_id'])->where('liga_id', $request['liga_id'])
                        ->decrement('derrotas', 1);

                $adicionarDerrotaTimeCasa = DB::table('times')
                                ->where('id', $request['time_casa_id'])->decrement('derrota', 1);

                $adicionarVitoriaTimeAdversario = DB::table('times')
                                ->where('id', $request['time_adversario_id'])->decrement('vitoria', 1);
            }

            /* Contador do Total de Jogos do time da casa */

            $jogos_time_casa = DB::table('placares')
                    ->where('time_id', $request['time_casa_id'])->where('liga_id', $request['liga_id'])
                    ->decrement('jogos', 1);

            /* Contador do Total de Jogos do time adversário */

            $jogos_time_adversario = DB::table('placares')
                    ->where('time_id', $request['time_adversario_id'])->where('liga_id', $request['liga_id'])
                    ->decrement('jogos', 1);

            //DB::table('jogos')->where('id', $request['id'])->delete();
            DB::table('jogos')
            ->where('id', $request['id'])
            ->update(['deleted_at' => Carbon::now()]);


            alert()->error('Jogo Deletado Com Sucesso.');


            return redirect()->route('pagina.inicial');
        }
    }

    public function pdfListarJogos() {

        if (!auth()->guard()->user()) {
            return redirect('pagina.login');
        }



        $usuario_autenticado_id = Auth::guard()->user()->id;

        $jogos = Jogos::orderBy('data_jogo')->where('usuario_id', $usuario_autenticado_id)->get();

        $pdf = \App::make('dompdf.wrapper');
        $view = View::make('pdf_lista_jogos', compact('jogos'))->render();
        $pdf->loadHTML($view);
        return $pdf->stream();
    }






    public function pdfListaDadosJogo($id) {

        if (!auth()->guard()->user()) {
            return redirect('pagina.login');
        }

        $usuario_autenticado_id = Auth::guard()->user()->id;


        $id = Jogos::find($id)->id;

        $jogos = Jogos::where('id', $id)->where('usuario_id', $usuario_autenticado_id)->get();

        $pdf = \App::make('dompdf.wrapper');
        $view = View::make('pdf_lista_dados_jogo', compact('id', 'jogos'))->render();
        $pdf->loadHTML($view);
        return $pdf->stream();
    }

}
