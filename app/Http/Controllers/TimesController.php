<?php

namespace App\Http\Controllers;

use App\Jogadores;
use App\Ligas;
use App\Times;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use \View;
use App\Sexos;

class TimesController extends Controller {

    public function paginaListaTimes() {

        if (!auth()->guard()->user()) {
            return redirect('pagina.login');
        }

        $usuario_autenticado_id = Auth::guard()->user()->id;
        $usuario_autenticado_nome = Auth::guard()->user()->nome;

        $times = Times::where('usuario_id', $usuario_autenticado_id)->where('deleted_at', null)->paginate(5);
        $ligas = Ligas::orderBy('nome_liga')->where('usuario_id', $usuario_autenticado_id)->where('deleted_at', null)->get();
        $jogadores = Jogadores::orderBy('nome')->where('usuario_id', $usuario_autenticado_id)->where('deleted_at', null)->get();
        $timesDeletados = Times::withTrashed()->where('usuario_id', $usuario_autenticado_id)->get();


        $eventosNotificacoes = DB::table('eventos')->where('usuario_id', $usuario_autenticado_id)->whereDate('data_inicial', Carbon::today())->get();
        $eventosNotificacoesContagem = DB::table('eventos')->where('usuario_id', $usuario_autenticado_id)->whereDate('data_inicial', Carbon::today())->count();

        $sexos = Sexos::orderBy('nome_sexo')->get();


        return view('lista_times', compact('timesDeletados','sexos', 'eventosNotificacoes', 'eventosNotificacoesContagem', 'times', 'ligas', 'usuario_autenticado_id', 'usuario_autenticado_nome', 'jogadores'));
    }

    public function salvarTime(Request $request) {

        if (!auth()->guard()->user()) {
            return redirect('pagina.login');
        }

        $usuario_autenticado_id = Auth::guard()->user()->id;

        $request->validate([
            'nome_time' => 'required',
            'ligas_id' => 'required',
            'jogadores_id' => 'required',
            'imagem_time' => 'mimes:jpeg,bmp,png,jpg',

        ]);

        $times = Times::create([
                    'nome_time' => $request['nome_time'],
                    'usuario_id' => $usuario_autenticado_id,
                    'vitoria' => 0,
                    'empate' => 0,
                    'derrota' => 0,
        ]);

//            dd($times);

        if ($times) {

            $times->jogadores()->sync($request['jogadores_id']);
            $times->ligas()->sync($request['ligas_id']);

            if (Input::file('imagem_time')) {

                $ultimoId = $times->id;
                $imagemTimeId = $ultimoId . '.png';
                $request->imagem_time->move(public_path('imagens_times/'), $imagemTimeId);
            } else {
                
            }

            $ligas = $request['ligas_id'];

            for ($i = 0; $i < count($ligas); $i++) {

                DB::table('placares')->insert(['time_id' => $times->id, 'pontos' => 0, 'jogos' => 0, 'vitorias' => 0, 'empates' => 0, 'derrotas' => 0, 'gols_pro' => 0, 'gols_contra' => 0, 'saldo_gols' => 0, 'usuario_id' => $usuario_autenticado_id, 'liga_id' => $ligas[$i]]);
            }

            alert()->success('Time Cadastrado Com Sucesso.');


            return redirect()->route('lista.times');
        }
    }

    public function getTimesPorLigaAjax(Request $request) {

        $timesPorLigaAjax = DB::table("times")
                ->join("liga_time", "times.id", "=", "liga_time.times_id")
                ->where("liga_time.ligas_id", $request->liga_id)->where('times.deleted_at', null)
                ->pluck("nome_time", "id");

        return response()->json($timesPorLigaAjax);
    }

    public function getJogadoresPorTimeAjax(Request $request) {

        $jogadoresPorTimeAjax = DB::table("jogadores")
                ->join("time_jogador", "jogadores.id", "=", "time_jogador.jogadores_id")
                ->where("time_jogador.times_id", $request->time_casa_id)->where('jogadores.deleted_at', null)
                ->pluck("nome", "id");

        return response()->json($jogadoresPorTimeAjax);
    }

    public function getJogadoresAdversariosPorTimeAjax(Request $request) {

        $jogadoresAdversariosPorTimeAjax = DB::table("jogadores")
                ->join("time_jogador", "jogadores.id", "=", "time_jogador.jogadores_id")
                ->where("time_jogador.times_id", $request->time_adversario_id)->where('jogadores.deleted_at', null)->where('deleted_at', null)
                ->pluck("nome", "id");

        return response()->json($jogadoresAdversariosPorTimeAjax);
    }

    public function atualizarTime(Request $request, $id) {


        if (!auth()->guard()->user()) {
            return redirect('pagina.login');
        }




        $usuario_autenticado_id = Auth::guard()->user()->id;

        $request->validate([
            'nome_time' => 'required',
            'imagem_time' => 'mimes:jpeg,bmp,png,jpg',

        ]);


        $dados = $request->all();

        $timeId = Times::find($id);

        $alt = $timeId->update($dados);

        if ($alt) {

            // $timeId->jogadores()->sync($request['jogadores_id']);
            // $timeId->ligas()->sync($request['ligas_id']);

            if (Input::file('imagem_time')) {

                $ultimoId = $id;
                $imagemTimeId = $ultimoId . '.png';
                $request->imagem_time->move(public_path('imagens_times/'), $imagemTimeId);
            } else {
                
            }

            /* $ligas = $request['ligas_id'];

              for ($i = 0; $i < count($ligas); $i++) {

              DB::table('placares')->insert(['time_id' => $id, 'pontos' => 0, 'jogos' => 0, 'vitorias' => 0, 'empates' => 0, 'derrotas' => 0, 'gols_pro' => 0, 'gols_contra' => 0, 'saldo_gols' => 0, 'usuario_id' => $usuario_autenticado_id, 'liga_id' => $ligas[$i]]);

              } */

            alert()->info('Time Atualizado Com Sucesso.');


            return redirect()->route('lista.times');
        }
    }

    public function atualizarTimeLiga(Request $request, $id) {

        if (!auth()->guard()->user()) {
            return redirect('pagina.login');
        }



        $usuario_autenticado_id = Auth::guard()->user()->id;

        $dados = $request->all();

        $timeId = Times::find($id);

        $alt = $timeId->update($dados);

        if ($alt) {

            // $timeId->jogadores()->sync($request['jogadores_id']);
            $timeId->ligas()->attach($request['ligas_id']);



            $ligas = $request['ligas_id'];

            for ($i = 0; $i < count($ligas); $i++) {

                DB::table('placares')->insert(['time_id' => $id, 'pontos' => 0, 'jogos' => 0, 'vitorias' => 0, 'empates' => 0, 'derrotas' => 0, 'gols_pro' => 0, 'gols_contra' => 0, 'saldo_gols' => 0, 'usuario_id' => $usuario_autenticado_id, 'liga_id' => $ligas[$i]]);
            }

            alert()->info('Liga(s) do Time Atualizada(s) Com Sucesso.');


            return redirect()->route('lista.times');
        }
    }

    public function atualizarTimeJogador(Request $request, $id) {

        if (!auth()->guard()->user()) {
            return redirect('pagina.login');
        }



        $usuario_autenticado_id = Auth::guard()->user()->id;

        $dados = $request->all();

        $timeId = Times::find($id);

        $alt = $timeId->update($dados);

        if ($alt) {

            $timeId->jogadores()->attach($request['jogadores_id']);
            //$timeId->ligas()->sync($request['ligas_id']);

            alert()->info('Jogador(es)/Jogadora(s) do Time Atualizado(s)/Atualizada(s) Com Sucesso.');


            return redirect()->route('lista.times');
        }
    }

    public function removerTimeLiga(Request $request, $id) {

        if (!auth()->guard()->user()) {
            return redirect('pagina.login');
        }



        $usuario_autenticado_id = Auth::guard()->user()->id;

        $dados = $request->all();

        $timeId = Times::find($id);

        $alt = $timeId->update($dados);

        if ($alt) {

            // $timeId->jogadores()->sync($request['jogadores_id']);
            $timeId->ligas()->detach($request['ligas_id']);


            $ligas = $request['ligas_id'];

            for ($i = 0; $i < count($ligas); $i++) {

                DB::table('placares')->where(['time_id' => $id, 'usuario_id' => $usuario_autenticado_id, 'liga_id' => $ligas[$i]])->delete();
            }


            alert()->info('Liga do Time Removida Com Sucesso.');


            return redirect()->route('lista.times');
        }
    }

    public function removerTimeJogador(Request $request, $id) {

        if (!auth()->guard()->user()) {
            return redirect('pagina.login');
        }



        $usuario_autenticado_id = Auth::guard()->user()->id;

        $dados = $request->all();

        $timeId = Times::find($id);

        $alt = $timeId->update($dados);

        if ($alt) {

            $timeId->jogadores()->detach($request['jogadores_id']);
            //$timeId->ligas()->sync($request['ligas_id']);

            alert()->info('Jogador(a) do Time Removido(a) Com Sucesso.');


            return redirect()->route('lista.times');
        }
    }

    public function paginaListaRankingTimes(Request $request) {

        $usuario_autenticado_id = Auth::guard()->user()->id;
        $usuario_autenticado_nome = Auth::guard()->user()->nome;

        $ligas = Ligas::orderBy('nome_liga')->where('usuario_id', $usuario_autenticado_id)->get();
        $times = Times::where('usuario_id', $usuario_autenticado_id)->orderBy($request->query('order', 'nome_time'), 'desc')->paginate(5);

        //$consultaVitorias = DB::table('placares')->where('time_id')->sum('vitorias');

        $eventosNotificacoes = DB::table('eventos')->where('usuario_id', $usuario_autenticado_id)->whereDate('data_inicial', Carbon::today())->get();
        $eventosNotificacoesContagem = DB::table('eventos')->where('usuario_id', $usuario_autenticado_id)->whereDate('data_inicial', Carbon::today())->count();

        $sexos = Sexos::orderBy('nome_sexo')->get();


        return view('lista_ranking_times', compact('sexos', 'eventosNotificacoes', 'eventosNotificacoesContagem', 'consultaVitorias', 'jogadores', 'ligas', 'times', 'usuario_autenticado_id', 'usuario_autenticado_nome'));
    }

    public function deletarTime($id) {
        $time = Times::find($id);
        if ($time->delete()) {

            DB::table('placares')
                    ->where('time_id', $id)
                    ->update(['deleted_at' => Carbon::now()]);

                    DB::table('jogos')
                    ->where('time_casa_id', $id)
                    ->update(['deleted_at' => Carbon::now()]);

                    DB::table('jogos')
                    ->where('time_adversario_id', $id)
                    ->update(['deleted_at' => Carbon::now()]);

                alert()->error('Time Deletado Com Sucesso.');


            return redirect()->route('lista.times');
        }
    }




    public function restaurarTime($id) {

        if (!auth()->guard()->user()) {
            return redirect('pagina.login');
        }



        $time = Times::withTrashed()->find($id);

        //dd($liga);
        
        if ($time->restore()) {

            /*DB::table('placares')
            ->where('time_id', $id)
            ->update(['deleted_at' => null]);

            DB::table('jogos')
            ->where('time_casa_id', $id)
            ->update(['deleted_at' => null]);

            DB::table('jogos')
            ->where('time_adversario_id', $id)
            ->update(['deleted_at' => null]);
            */

            alert()->success('Time Restaurado Com Sucesso.');


            return redirect()->route('lista.times');
        }
    }


    public function pdfListarTimes() {

        $usuario_autenticado_id = Auth::guard()->user()->id;

        $times = Times::orderBy('nome_time')->where('usuario_id', $usuario_autenticado_id)->get();

        $pdf = \App::make('dompdf.wrapper');
        $view = View::make('pdf_lista_times', compact('times'))->render();
        $pdf->loadHTML($view);
        return $pdf->stream();
    }

}
