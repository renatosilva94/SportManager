<?php

namespace App\Http\Controllers;

use App\Ligas;
use App\Placares;
use App\Times;
use Auth;
use \View;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Sexos;

class PlacaresController extends Controller {

    public function paginaListaPlacares($id) {

        if (!auth()->guard()->user()) {
            return redirect('pagina.login');
        }


        $id = Ligas::find($id)->id;
$ligaUsuarioId = Ligas::find($id)->usuario_id;


        $nome_liga = Ligas::find($id)->nome_liga;

        $usuario_autenticado_id = Auth::guard()->user()->id;
        $usuario_autenticado_nome = Auth::guard()->user()->nome;

        if ($usuario_autenticado_id == $ligaUsuarioId) {

        }else{
        
            return redirect('pagina.inicial');
        
        }

        $ligas = Ligas::orderBy('nome_liga')->where('usuario_id', $usuario_autenticado_id)->where('deleted_at', null)->get();
        $times = Times::orderBy('nome_time')->where('usuario_id', $usuario_autenticado_id)->where('deleted_at', null)->get();

        //Criterios de Desempate

        $placares = Placares::with('ligas', 'times')->where('usuario_id', '=', $usuario_autenticado_id)->where('liga_id', '=', $id)->orderBy('pontos', 'desc')->orderBy('vitorias', 'desc')->orderBy('saldo_gols', 'desc')->orderBy('gols_pro', 'desc')->get();

        $eventosNotificacoes = DB::table('eventos')->where('usuario_id', $usuario_autenticado_id)->whereDate('data_inicial', Carbon::today())->get();
        $eventosNotificacoesContagem = DB::table('eventos')->where('usuario_id', $usuario_autenticado_id)->whereDate('data_inicial', Carbon::today())->count();

        $sexos = Sexos::orderBy('nome_sexo')->get();


        return view('lista_placares', compact('sexos', 'eventosNotificacoes', 'eventosNotificacoesContagem', 'times', 'placares', 'ligas', 'id', 'nome_liga', 'usuario_autenticado_id', 'usuario_autenticado_nome'));
    }

    public function pdfListarPlacares($id) {

        if (!auth()->guard()->user()) {
            return redirect('pagina.login');
        }

        $usuario_autenticado_id = Auth::guard()->user()->id;


        $id = Ligas::find($id)->id;
        $nome_liga = Ligas::find($id)->nome_liga;

        $times = Times::orderBy('nome_time')->where('usuario_id', $usuario_autenticado_id)->where('deleted_at', null)->get();
        $ligas = Ligas::orderBy('nome_liga')->where('usuario_id', $usuario_autenticado_id)->where('deleted_at', null)->get();

        //Criterios de Desempate

        $placares = Placares::with('ligas', 'times')->where('liga_id', '=', $id)->orderBy('pontos', 'desc')->orderBy('vitorias', 'desc')->orderBy('saldo_gols', 'desc')->orderBy('gols_pro', 'desc')->get();

        $pdf = \App::make('dompdf.wrapper');
        $view = View::make('pdf_lista_placares', compact('placares', 'ligas', 'times', 'id', 'nome_liga'))->render();
        $pdf->loadHTML($view);
        return $pdf->stream();
    }

}
