<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use Illuminate\Http\Request;

class BuscaAjaxController extends Controller {
    /* Página Em Que A Busca de Ligas Vai Ser Direcionada */

    public function paginaBuscaLigas() {

        $usuario_autenticado_id = Auth::guard()->user()->id;

        return view('lista.ligas', compact('usuario_autenticado_id'));
    }

    /* Função Para Buscar As Ligas */

    public function buscaLigasAjax(Request $request) {

        $usuario_autenticado_id = Auth::guard()->user()->id;

        if ($request->ajax()) {

            $saida = "";

            $ligas = DB::table('ligas')->where('nome_liga', 'LIKE', '%' . $request->buscaLigasAjax . "%")->where('usuario_id', $usuario_autenticado_id)->where('deleted_at', null)->get();

            if ($ligas) {

                foreach ($ligas as $key => $liga) {

                    if (file_exists(public_path('imagens_ligas/' . $liga->id . '.png'))) {
                        $imagem_liga = '../imagens_ligas/' . $liga->id . '.png';
                    } else {
                        $imagem_liga = '../imagens_ligas/sem_foto.png';
                    }

                    $saida .= '<tr>' .
                            '<td style="text-align: center">' . $liga->nome_liga . '</td>' .
                            '<td style="text-align: center">

    <a href=' . $imagem_liga . ' data-lightbox="roadtrip" data-lightbox-gallery="gallery1" data-lightbox-hidpi=' . $imagem_liga . '>

    <img src=' . $imagem_liga . ' id="imagem" class="img-circle" width="50" height="50" alt="Imagem da Liga" data-zoom-image=' . $imagem_liga . '> </a> </td>' .
                            '<td style="text-align: center"> <img title="Editar Liga" src="icones/editar.png" width="30px" height="30px" data-toggle="modal" data-target="#modalEditarLiga' . $liga->id . '">' . '<form method="post" action="{{route("deletar.liga,' . $liga->id . ')}}">           <input title="Deletar Liga" type="image" src="icones/deletar.png" width="30px" height="30px" >                                                                ' . ' </form> </td>' .
                            '</tr>';
                }

                return Response($saida);
            }
        }
    }

    /* Página Em Que A Busca de Times Vai Ser Direcionada */

    public function paginaBuscaTimes() {

        $usuario_autenticado_id = Auth::guard()->user()->id;

        return view('lista.times', compact('usuario_autenticado_id'));
    }

    /* Função Para Buscar Os Times */

    public function buscaTimesAjax(Request $request) {

        $usuario_autenticado_id = Auth::guard()->user()->id;

        if ($request->ajax()) {

            $saida = "";

            $times = DB::table('times')->where('nome_time', 'LIKE', '%' . $request->buscaTimesAjax . "%")->where('usuario_id', $usuario_autenticado_id)->where('deleted_at', null)->get();

            if ($times) {

                foreach ($times as $key => $time) {

                    if (file_exists(public_path('imagens_times/' . $time->id . '.png'))) {
                        $imagem_time = '../imagens_times/' . $time->id . '.png';
                    } else {
                        $imagem_time = '../imagens_times/sem_foto.png';
                    }

                    $saida .= '<tr>' .
                            '<td style="text-align: center">

    <a href=' . $imagem_time . ' data-lightbox="roadtrip" data-lightbox-gallery="gallery1" data-lightbox-hidpi=' . $imagem_time . '>

    <img src=' . $imagem_time . ' id="imagem" class="img-circle" width="50" height="50" alt="Imagem do Time" data-zoom-image=' . $imagem_time . '> </a> </td>' .
                            '<td style="text-align: center">' . $time->nome_time . '</td>' .
                            '<td style="text-align: center"> <form method="post" action="' . route("deletar.time", $time->id) . '" onsubmit="return confirm (' . " 'Confirmar Exclusao' " . ')">                        ' . method_field("delete") . csrf_field() . '             <input title="Deletar Time" type="image" src="icones/deletar.png" width="30px" height="30px" >                                                 </form>                                                                                   ' . '                                               <img title="Editar Outras Informações" src="icones/editar_extras.png" width="30px" height="30px" data-toggle="modal" data-target="#modalEditarOutrasInformacoes' . $time->id . '"> </td>' .
                            '</tr>';
                }

                return Response($saida);
            }
        }
    }

    /* Página Em Que A Busca de Jogadores Vai Ser Direcionada */

    public function paginaBuscaJogadores() {

        $usuario_autenticado_id = Auth::guard()->user()->id;

        return view('lista.jogadores', compact('usuario_autenticado_id'));
    }

    /* Função Para Buscar Os Jogadores */

    public function buscaJogadoresAjax(Request $request) {

        $usuario_autenticado_id = Auth::guard()->user()->id;

        if ($request->ajax()) {

            $saida = "";

            $jogadores = DB::table('jogadores')->where('nome', 'LIKE', '%' . $request->buscaJogadoresAjax . "%")->where('usuario_id', $usuario_autenticado_id)->where('deleted_at', null)->get();

            if ($jogadores) {

                foreach ($jogadores as $key => $jogador) {

                    if (file_exists(public_path('imagens_jogadores/' . $jogador->id . '.png'))) {
                        $imagem_jogador = '../imagens_jogadores/' . $jogador->id . '.png';
                    } else {
                        $imagem_jogador = '../imagens_jogadores/sem_foto.png';
                    }

                    $saida .= '<tr>' .
                            '<td style="text-align: center">

    <a href=' . $imagem_jogador . ' data-lightbox="roadtrip" data-lightbox-gallery="gallery1" data-lightbox-hidpi=' . $imagem_jogador . '>

    <img src=' . $imagem_jogador . ' id="imagem" class="img-circle" width="50" height="50" alt="Imagem do Jogador" data-zoom-image=' . $imagem_jogador . '> </a> </td>' .
                            '<td style="text-align: center">' . $jogador->nome . '</td>' .
                            '<td style="text-align: center">' . $jogador->sobrenome . '</td>' .
                            '<td style="text-align: center">' . $jogador->funcao . '</td>' .
                            '<td style="text-align: center; color: #007f00"> <a href="' . route("remover.ausencia", $jogador->id) . '" class="btn btn-success btn-xs">-</a>&nbsp;<b>' . $jogador->ausente . ' <a href="' . route("adicionar.ausencia", $jogador->id) . '" class="btn btn-success btn-xs">+</a>   </td>' .
                            '<td style="text-align: center; color: #ccac00"> <a href="' . route("remover.cartao.amarelo", $jogador->id) . '" class="btn btn-warning btn-xs">-</a>&nbsp;<b>' . $jogador->cartao_amarelo . ' <a href="' . route("adicionar.cartao.amarelo", $jogador->id) . '" class="btn btn-warning btn-xs">+</a>   </td>' .
                            '<td style="text-align: center; color: #cc0000"> <a href="' . route("remover.cartao.vermelho", $jogador->id) . '" class="btn btn-danger btn-xs">-</a>&nbsp;<b>' . $jogador->cartao_vermelho . ' <a href="' . route("adicionar.cartao.vermelho", $jogador->id) . '" class="btn btn-danger btn-xs">+</a>   </td>' .
                            '<td style="text-align: center; color: #120a8f"> <a href="' . route("remover.gol", $jogador->id) . '" class="btn btn-info btn-xs">-</a>&nbsp;<b>' . $jogador->gols . ' <a href="' . route("adicionar.gol", $jogador->id) . '" class="btn btn-info btn-xs">+</a>   </td>' .
                            '<td style="text-align: center">' . $jogador->email . '</td>' .
                            '<td style="text-align: center"> <form method="post" action="' . route("deletar.jogador", $jogador->id) . '" onsubmit="return confirm (' . " 'Confirmar Exclusao' " . ')">                        ' . method_field("delete") . csrf_field() . '             <input title="Deletar Time" type="image" src="icones/deletar.png" width="30px" height="30px" >                                                 </form>                                                                                   ' . '                         <img title="Ver Times Vinculadas" src="icones/times.png" width="30px" height="30px" data-toggle="modal" data-target="#modalVerTimes' . $jogador->id . '" >                        <img title="Editar Time" src="icones/editar.png" width="30px" height="30px" data-toggle="modal" data-target="#modalEditarJogador' . $jogador->id . '">       </td>' .
                            '</tr>';
                }

                return Response($saida);
            }
        }
    }

}
