<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Notifications\RecuperarSenhaRequisicao;
use App\Notifications\RecuperarSenhaSucesso;
use App\RecuperarSenha;
use App\Usuarios;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Session;
use Redirect;
use Alert;

class RecuperarSenhaController extends Controller {

    /**
     * Create token password reset
     *
     * @param  [string] email
     * @return [string] message
     */
    public function enviarEmailRecuperacao(Request $request) {
        $request->validate([
            'email' => 'required|string|email',
        ]);

        $user = Usuarios::where('email', $request->email)->first();

        if (!$user) {


            alert()->error("E-Mail não existe em nossa base de dados!");

            return redirect('pagina.recuperar.senha');

            // return redirect('pagina.recuperar.senha')->with(['error' => 'Não conseguimos encontrar um usuário com esse e-mail.']);
        }

        $passwordReset = RecuperarSenha::updateOrCreate(
                        ['email' => $user->email], [
                    'email' => $user->email,
                    'token' => str_random(60),
                        ]
        );
        if ($user && $passwordReset) {
            $user->notify(
                    new RecuperarSenhaRequisicao($passwordReset->token)
            );

            alert()->success("E-Mail Enviado!", "Verifique o Seu E-Mail");
        }

        return redirect('pagina.login'); //->with('success', "E-Mail Para Recuperação Enviado !");
    }

    /**
     * Find token password reset
     *
     * @param  [string] $token
     * @return [string] message
     * @return [json] passwordReset object
     */
    public function encontrarToken($token) {
        $passwordReset = RecuperarSenha::where('token', $token)
                ->first();
        if (!$passwordReset) {
            // return response()->json([
            //    'message' => 'Esse token de recuperação é invalido.',
            // ], 404);
        }

        if (Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
            $passwordReset->delete();
            //return response()->json([
            //    'message' => 'Esse token de recuperação é invalido.',
            //], 404);
        }
        return response()->json($passwordReset);
    }

    /**
     * Reset password
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @param  [string] token
     * @return [string] message
     * @return [json] user object
     */
    public function resetarSenha(Request $request) {

        $user = Usuarios::where('email', $request->email)->first();

        if (!$user) {
            // return response()->json([
            //    'message' => 'Não conseguimos encontrar um usuário com esse e-mail.',
            //], 404);
        }

        $passwordReset = RecuperarSenha::where([
                        ['token', $request->token],
                        ['email', $request->email],
                ])->first();

        if (!$passwordReset) {
            
        } else {

            $user->password = bcrypt($request->password);
            $user->save();

            $passwordReset->delete();

            $user->notify(new RecuperarSenhaSucesso($passwordReset));

            alert()->success("Senha Alterada", "Você Já Pode Fazer Login!");

            // dd($alerta);

            return redirect('pagina.login');
        }
    }

}
