<?php

namespace App\Http\Controllers;

use App\Jogadores;
use App\Ligas;
use App\Times;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use App\Sexos;
use \View;
 

class JogadoresController extends Controller
{

    public function paginaListaJogadores()
    {

        if ( !auth()->guard()->user() )
        {
        return redirect('pagina.login');
        }



        $usuario_autenticado_id = Auth::guard()->user()->id;
        $usuario_autenticado_nome = Auth::guard()->user()->nome;

        $ligas = Ligas::orderBy('nome_liga')->where('usuario_id', $usuario_autenticado_id)->where('deleted_at', null)->get();
        $times = Times::orderBy('nome_time')->where('usuario_id', $usuario_autenticado_id)->where('deleted_at', null)->get();

        $jogadores = Jogadores::where('usuario_id', $usuario_autenticado_id)->where('deleted_at', null)->paginate(4);
        $jogadoresDeletados = Jogadores::withTrashed()->where('usuario_id', $usuario_autenticado_id)->get();


        $eventosNotificacoes = DB::table('eventos')->where('usuario_id', $usuario_autenticado_id)->whereDate('data_inicial', Carbon::today())->get();
        $eventosNotificacoesContagem = DB::table('eventos')->where('usuario_id', $usuario_autenticado_id)->whereDate('data_inicial', Carbon::today())->count();

        $sexos = Sexos::orderBy('nome_sexo')->get();


        return view('lista_jogadores', compact('jogadoresDeletados','sexos','eventosNotificacoes','eventosNotificacoesContagem','jogadores', 'ligas', 'times', 'usuario_autenticado_id', 'usuario_autenticado_nome'));

    }

    public function salvarJogador(Request $request)
    {

        if ( !auth()->guard()->user() )
        {
        return redirect('pagina.login');
        }



        $usuario_autenticado_id = Auth::guard()->user()->id;

        $request->validate([
            'nome' => 'required|max:255',
            'sobrenome' => 'required|max:255',
            'email' => 'required|max:255',
            'imagem_jogador' => 'mimes:jpeg,bmp,png,jpg',
        ]);

        $jogadores = Jogadores::create([
            'nome' => $request['nome'],
            'sobrenome' => $request['sobrenome'],
            'email' => $request['email'],
            'cpf' => $request['cpf'],
            'rg' => $request['rg'],
            'funcao' => $request['funcao'],
            'usuario_id' => $usuario_autenticado_id,

            // 'time_id' => $request['time_id'],
            'cartao_amarelo' => 0,
            'cartao_vermelho' => 0,
            'gols' => 0,
            'ausente' => 0,

        ]);

        if ($jogadores) {

            if (Input::file('imagem_jogador')) {

                $ultimoId = $jogadores->id;
                $imagemJogadorId = $ultimoId . '.png';
                $request->imagem_jogador->move(public_path('imagens_jogadores/'), $imagemJogadorId);

            } else {

            }

            alert()->success('Jogador Cadastrado Com Sucesso.');


            return redirect()->route('lista.jogadores');
        }

    }

    public function adicionarAusencia(Request $request, $id)
    {

        if ( !auth()->guard()->user() )
        {
        return redirect('pagina.login');
        }



        $dados = $request->all();

        $jogadorId = Jogadores::find($id);

        $alt = $jogadorId->update($dados);

        if ($alt) {

            $adicionar_ausencia = DB::table('jogadores')
                ->where('id', $request['id'])->increment('ausente', 1);

                alert()->toast('Ausencia Adicionada !', 'success');


            return redirect()->route('lista.jogadores');

        }

    }

    public function removerAusencia(Request $request, $id)
    {

        if ( !auth()->guard()->user() )
        {
        return redirect('pagina.login');
        }



        $dados = $request->all();

        $jogadorId = Jogadores::find($id);

        $alt = $jogadorId->update($dados);

        if ($alt) {

            $remover_cartao_vermelho = DB::table('jogadores')
                ->where('id', $request['id'])->decrement('ausente', 1);

                alert()->toast('Ausencia Removida !', 'success');


            return redirect()->route('lista.jogadores');

        }

    }

    public function adicionarCartaoVermelho(Request $request, $id)
    {

        if ( !auth()->guard()->user() )
        {
        return redirect('pagina.login');
        }



        $dados = $request->all();

        $jogadorId = Jogadores::find($id);

        $alt = $jogadorId->update($dados);

        if ($alt) {

            $adicionar_cartao_vermelho = DB::table('jogadores')
                ->where('id', $request['id'])->increment('cartao_vermelho', 1);

                alert()->toast('Cartão Vermelho Adicionado !', 'success');


            return redirect()->route('lista.jogadores');

        }

    }

    public function removerCartaoVermelho(Request $request, $id)
    {

        if ( !auth()->guard()->user() )
        {
        return redirect('pagina.login');
        }



        $dados = $request->all();

        $jogadorId = Jogadores::find($id);

        $alt = $jogadorId->update($dados);

        if ($alt) {

            $remover_cartao_vermelho = DB::table('jogadores')
                ->where('id', $request['id'])->decrement('cartao_vermelho', 1);

                alert()->toast('Cartão Vermelho Removido !', 'success');


            return redirect()->route('lista.jogadores');

        }

    }

    public function adicionarCartaoAmarelo(Request $request, $id)
    {

        if ( !auth()->guard()->user() )
        {
        return redirect('pagina.login');
        }



        $dados = $request->all();

        $jogadorId = Jogadores::find($id);

        $alt = $jogadorId->update($dados);

        if ($alt) {

            $adicionar_cartao_amarelo = DB::table('jogadores')
                ->where('id', $request['id'])->increment('cartao_amarelo', 1);

                alert()->toast('Cartão Amarelo Adicionado !', 'success');


            return redirect()->route('lista.jogadores');

        }

    }

    public function removerCartaoAmarelo(Request $request, $id)
    {

        if ( !auth()->guard()->user() )
        {
        return redirect('pagina.login');
        }



        $dados = $request->all();

        $jogadorId = Jogadores::find($id);

        $alt = $jogadorId->update($dados);

        if ($alt) {

            $remover_cartao_amarelo = DB::table('jogadores')
                ->where('id', $request['id'])->decrement('cartao_amarelo', 1);

                alert()->toast('Cartão Amarelo Removido !', 'success');


            return redirect()->route('lista.jogadores');

        }

    }

    public function adicionarGol(Request $request, $id)
    {

        if ( !auth()->guard()->user() )
        {
        return redirect('pagina.login');
        }



        $dados = $request->all();

        $jogadorId = Jogadores::find($id);

        $alt = $jogadorId->update($dados);

        if ($alt) {

            $adicionar_cartao_vermelho = DB::table('jogadores')
                ->where('id', $request['id'])->increment('gols', 1);

                alert()->toast('Gol Adicionado !', 'success');


            return redirect()->route('lista.jogadores');

        }

    }

    public function removerGol(Request $request, $id)
    {

        if ( !auth()->guard()->user() )
        {
        return redirect('pagina.login');
        }



        $dados = $request->all();

        $jogadorId = Jogadores::find($id);

        $alt = $jogadorId->update($dados);

        if ($alt) {

            $remover_cartao_amarelo = DB::table('jogadores')
                ->where('id', $request['id'])->decrement('gols', 1);

                alert()->toast('Gol Removido !', 'success');


            return redirect()->route('lista.jogadores');

        }

    }

    public function atualizarJogador(Request $request, $id)
    {

        if ( !auth()->guard()->user() )
        {
        return redirect('pagina.login');
        }

        $request->validate([
            'nome' => 'required|max:255',
            'sobrenome' => 'required|max:255',
            'email' => 'required|max:255',
            'imagem_jogador' => 'mimes:jpeg,bmp,png,jpg',
        ]);

        $dados = $request->all();

        $jogadorId = Jogadores::find($id);

        $alt = $jogadorId->update($dados);

        if ($alt) {

            if (Input::file('imagem_jogador')) {

                $ultimoId = $id;
                $imagemJogadorId = $ultimoId . '.png';
                $request->imagem_jogador->move(public_path('imagens_jogadores/'), $imagemJogadorId);
            } else {
                
            }

            alert()->info('Jogador Atualizado Com Sucesso.');


            return redirect()->route('lista.jogadores');

        }

    }

    public function deletarJogador($id)
    {

        if ( !auth()->guard()->user() )
        {
        return redirect('pagina.login');
        }




        $jogador = Jogadores::find($id);
        if ($jogador->delete()) {

            DB::table('jogadores')
                ->where('id', $id)
                ->update(['deleted_at' => Carbon::now()]);

                alert()->error('Jogador Deletado Com Sucesso.');


            return redirect()->route('lista.jogadores');
        }
    }



    public function restaurarJogador($id) {

        if (!auth()->guard()->user()) {
            return redirect('pagina.login');
        }



        $jogador = Jogadores::withTrashed()->find($id);


        if ($jogador->restore()) {

            
            alert()->success('Jogador Restaurado Com Sucesso.');


            return redirect()->route('lista.jogadores');
        }
    }


    public function paginaListaRankingJogadores(Request $request)
    {

        if ( !auth()->guard()->user() )
        {
        return redirect('pagina.login');
        }



        $usuario_autenticado_id = Auth::guard()->user()->id;
        $usuario_autenticado_nome = Auth::guard()->user()->nome;

        $ligas = Ligas::orderBy('nome_liga')->where('usuario_id', $usuario_autenticado_id)->get();
        $times = Times::orderBy('nome_time')->where('usuario_id', $usuario_autenticado_id)->get();

        $jogadores = Jogadores::where('usuario_id', $usuario_autenticado_id)->orderBy($request->query('order', 'nome'), 'desc')->paginate(5);

        $eventosNotificacoes = DB::table('eventos')->where('usuario_id', $usuario_autenticado_id)->whereDate('data_inicial', Carbon::today())->get();
        $eventosNotificacoesContagem = DB::table('eventos')->where('usuario_id', $usuario_autenticado_id)->whereDate('data_inicial', Carbon::today())->count();

        $sexos = Sexos::orderBy('nome_sexo')->get();


        return view('lista_ranking_jogadores', compact('sexos','eventosNotificacoes','eventosNotificacoesContagem','jogadores', 'ligas', 'times', 'usuario_autenticado_id', 'usuario_autenticado_nome'));

    }




    public function pdfListarJogadores()
    {

        if ( !auth()->guard()->user() )
        {
        return redirect('pagina.login');
        }



        $usuario_autenticado_id = Auth::guard()->user()->id;

        $jogadores = Jogadores::orderBy('nome')->where('usuario_id', $usuario_autenticado_id)->where('deleted_at', null)->get();

        $pdf = \App::make('dompdf.wrapper');
        $view = View::make('pdf_lista_jogadores', compact('jogadores'))->render();

        $pdf->setOptions(['isRemoteEnabled' => true]);

        $pdf->loadHTML($view);        
        return $pdf->stream();
    }








}
