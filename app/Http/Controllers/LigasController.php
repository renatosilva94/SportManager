<?php

namespace App\Http\Controllers;

use App\Ligas;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use \View;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Alert;
use App\Sexos;

class LigasController extends Controller {

    public function paginaListaLigas() {
        if (!auth()->guard()->user()) {
            return redirect('pagina.login');
        }



        $usuario_autenticado_id = Auth::guard()->user()->id;
        $usuario_autenticado_nome = Auth::guard()->user()->nome;

        $ligas = Ligas::where('usuario_id', $usuario_autenticado_id)->where('deleted_at', null)->paginate(5);
        $ligasDeletadas = Ligas::withTrashed()->where('usuario_id', $usuario_autenticado_id)->get();

        $eventosNotificacoes = DB::table('eventos')->where('usuario_id', $usuario_autenticado_id)->whereDate('data_inicial', Carbon::today())->get();
        $eventosNotificacoesContagem = DB::table('eventos')->where('usuario_id', $usuario_autenticado_id)->whereDate('data_inicial', Carbon::today())->count();

        $sexos = Sexos::orderBy('nome_sexo')->get();



        return view('lista_ligas', compact('ligasDeletadas','sexos', 'eventosNotificacoes', 'eventosNotificacoesContagem', 'ligas', 'subligas', 'usuario_autenticado_id', 'usuario_autenticado_nome'));
    }

    public function salvarLiga(Request $request) {

        if (!auth()->guard()->user()) {
            return redirect('pagina.login');
        }



        $usuario_autenticado_id = Auth::guard()->user()->id;

        $request->validate([
            'nome_liga' => 'required|max:255',
            'imagem_liga' => 'mimes:jpeg,bmp,png,jpg',
        ]);

        $ligas = Ligas::create([
                    'nome_liga' => $request['nome_liga'],
                    'usuario_id' => $usuario_autenticado_id,
        ]);

        if ($ligas) {

            if (Input::file('imagem_liga')) {

                $ultimoId = $ligas->id;
                $imagemLigaId = $ultimoId . '.png';
                $request->imagem_liga->move(public_path('imagens_ligas/'), $imagemLigaId);
            } else {
                
            }

            alert()->success('Liga Cadastrada Com Sucesso.');


            return redirect()->route('lista.ligas');
        }
    }

    public function atualizarLiga(Request $request, $id) {

        if (!auth()->guard()->user()) {
            return redirect('pagina.login');
        }

        $request->validate([
            'nome_liga' => 'required|max:255',
            'imagem_liga' => 'mimes:jpeg,bmp,png,jpg',
        ]);



        $dados = $request->all();

        $ligaId = Ligas::find($id);

        $alt = $ligaId->update($dados);

        if ($alt) {

            if (Input::file('imagem_liga')) {

                $ultimoId = $id;
                $imagemLigaId = $ultimoId . '.png';
                $request->imagem_liga->move(public_path('imagens_ligas/'), $imagemLigaId);
            } else {
                
            }

            alert()->info('Liga Atualizada Com Sucesso.');


            return redirect()->route('lista.ligas');
        }
    }

    public function deletarLiga($id) {

        if (!auth()->guard()->user()) {
            return redirect('pagina.login');
        }



        $liga = Ligas::find($id);
        if ($liga->delete()) {

            DB::table('placares')
            ->where('liga_id', $id)
            ->update(['deleted_at' => Carbon::now()]);

            DB::table('jogos')
            ->where('liga_id', $id)
            ->update(['deleted_at' => Carbon::now()]);



            alert()->error('Liga Deletada Com Sucesso.');


            return redirect()->route('lista.ligas');
        }
    }



    public function restaurarLiga($id) {

        if (!auth()->guard()->user()) {
            return redirect('pagina.login');
        }



        $liga = Ligas::withTrashed()->find($id);

        //dd($liga);
        
        if ($liga->restore()) {

            /*DB::table('placares')
            ->where('liga_id', $id)
            ->update(['deleted_at' => null]);

            DB::table('jogos')
            ->where('liga_id', $id)
            ->update(['deleted_at' => null]);
            */


            alert()->success('Liga Restaurada Com Sucesso.');


            return redirect()->route('lista.ligas');
        }
    }












    public function pdfListarLigas() {

        if (!auth()->guard()->user()) {
            return redirect('pagina.login');
        }



        $usuario_autenticado_id = Auth::guard()->user()->id;

        $ligas = Ligas::orderBy('nome_liga')->where('usuario_id', $usuario_autenticado_id)->where('deleted_at', null)->get();

        $pdf = \App::make('dompdf.wrapper');
        $view = View::make('pdf_lista_ligas', compact('ligas'))->render();

        $pdf->setOptions(['isRemoteEnabled' => true]);

        $pdf->loadHTML($view);
        return $pdf->stream();
    }

}
