<?php

namespace App\Http\Controllers;

use App\Jogos;
use App\Ligas;
use App\Sexos;
use App\Times;
use Auth;
use App\Eventos;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Alert;

class ViewController extends Controller {

    public function paginaInicialDashboard() {


        if (!auth()->guard()->user()) {
            return redirect('pagina.login');
        }

        $usuario_autenticado_id = Auth::guard()->user()->id;
        $usuario_autenticado_nome = Auth::guard()->user()->nome;

        $ligas = Ligas::orderBy('nome_liga')->where('usuario_id', $usuario_autenticado_id)->where('deleted_at', null)->get();
        $times = Times::orderBy('nome_time')->where('usuario_id', $usuario_autenticado_id)->where('deleted_at', null)->get();
        $jogos = Jogos::orderBy('data_jogo')->where('usuario_id', $usuario_autenticado_id)->where('deleted_at', null)->get();
        $eventos = Eventos::orderBy('data_inicial')->where('usuario_id', $usuario_autenticado_id)->get();

        $contaLigas = DB::table('ligas')->where('usuario_id', $usuario_autenticado_id)->where('deleted_at', null)->count();
        $contaJogadores = DB::table('jogadores')->where('usuario_id', $usuario_autenticado_id)->where('deleted_at', null)->count();
        $contaTimes = DB::table('times')->where('usuario_id', $usuario_autenticado_id)->where('deleted_at', null)->count();
        $contaJogos = DB::table('jogos')->where('usuario_id', $usuario_autenticado_id)->where('deleted_at', null)->count();
        $contaEventos = DB::table('eventos')->where('usuario_id', $usuario_autenticado_id)->count();

        $eventosNotificacoes = DB::table('eventos')->where('usuario_id', $usuario_autenticado_id)->whereDate('data_inicial', Carbon::today())->get();
        $eventosNotificacoesContagem = DB::table('eventos')->where('usuario_id', $usuario_autenticado_id)->whereDate('data_inicial', Carbon::today())->count();

        $sexos = Sexos::orderBy('nome_sexo')->get();

        //dd($eventosNotificacoesContagem);


        return view('pagina_inicial', compact('sexos', 'eventosNotificacoes', 'eventosNotificacoesContagem', 'ligas', 'times', 'usuario_autenticado_id', 'usuario_autenticado_nome', 'contaLigas', 'contaJogadores', 'contaTimes', 'contaJogos', 'jogos', 'eventos', 'contaEventos'));
    }

    public function paginaInicialSite() {


        $ligas = DB::table('ligas')->count();
        $times = DB::table('times')->count();
        $usuarios = DB::table('usuarios')->count();
        $jogadores = DB::table('jogadores')->count();

        return view('site', compact('ligas', 'times', 'usuarios', 'jogadores'));
    }

    public function paginaLogin() {

        //  alert()->success('You have been logged out.', 'Good bye!');


        $sexos = Sexos::orderBy('nome_sexo')->get();

        return view('pagina_login', compact('sexos'));
    }

    public function paginaRecuperarSenha() {

        return view('pagina_recuperar_senha');
    }

    public function paginaNovaSenha() {

        return view('pagina_nova_senha');
    }

}
