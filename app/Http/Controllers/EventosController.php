<?php

namespace App\Http\Controllers;

use App\Eventos;
use App\Jogos;
use App\Ligas;
use App\Sexos;
use App\Times;
use Auth;
use Calendar;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EventosController extends Controller {
    /* Página de Listagem de Eventos (Agenda) */

    public function paginaListaAgenda() {

        if (!auth()->guard()->user()) {
            return redirect('pagina.login');
        }

        $usuario_autenticado_id = Auth::guard()->user()->id;
        $usuario_autenticado_nome = Auth::guard()->user()->nome;

        $ligas = Ligas::orderBy('nome_liga')->where('usuario_id', $usuario_autenticado_id)->where('deleted_at', null)->get();
        $times = Times::orderBy('nome_time')->where('usuario_id', $usuario_autenticado_id)->where('deleted_at', null)->get();
        $jogos = Jogos::orderBy('data_jogo')->where('usuario_id', $usuario_autenticado_id)->where('deleted_at', null)->get();

        $eventosNotificacoes = DB::table('eventos')->where('usuario_id', $usuario_autenticado_id)->whereDate('data_inicial', Carbon::today())->get();
        $eventosNotificacoesContagem = DB::table('eventos')->where('usuario_id', $usuario_autenticado_id)->whereDate('data_inicial', Carbon::today())->count();

        $sexos = Sexos::orderBy('nome_sexo')->get();

        $agendas = Eventos::get()->where('usuario_id', $usuario_autenticado_id);
        $agendas_lista = [];

        foreach ($agendas as $key => $agenda) {
            $agendas_lista[] = Calendar::event(
                            $agenda->nome_evento, false, new \DateTime($agenda->data_inicial), new \DateTime($agenda->data_final), $agenda->mensagem
            );
        }

        $detalhes_calendario = Calendar::addEvents($agendas_lista)->setCallbacks([
            'eventClick' => '

            function(event, jsEvent, view) {

                $("#nomeEvento").html(event.title);

                $("#descricaoEvento").html(event.id);





                $("#eventUrl").attr("href",event.url);
                $("#modalCalendario").modal();
            }',
        ]);


        return view('lista_agendas', compact('agendas', 'sexos', 'eventosNotificacoes', 'eventosNotificacoesContagem', 'detalhes_calendario', 'usuario_autenticado_id', 'usuario_autenticado_nome', 'ligas', 'times', 'jogos'));
    }

    /* Salvar Compromisso Na Agenda */

    public function salvarAgenda(Request $request) {

        if (!auth()->guard()->user()) {
            return redirect('pagina.login');
        }

        $usuario_autenticado_id = Auth::guard()->user()->id;
        $usuario_autenticado_nome = Auth::guard()->user()->nome;

        $request->validate([
            'nome_evento' => 'required|max:255',
            'data_inicial' => 'required',
            'data_final' => 'required',
            'mensagem' => 'required|max:255',
        ]);

        $agendas = Eventos::create([
                    'nome_evento' => $request['nome_evento'],
                    'data_inicial' => $request['data_inicial'],
                    'data_final' => $request['data_final'],
                    'mensagem' => $request['mensagem'],
                    'usuario_id' => $usuario_autenticado_id,
        ]);

        if ($agendas) {

            alert()->success('Evento Cadastrado Com Sucesso.');

            return redirect()->route('lista.agendas');
        }
    }

    /* Deletar Compromisso Na Agenda */

    public function deletarCompromisso($id) {

        if (!auth()->guard()->user()) {
            return redirect('pagina.login');
        }

        $agenda = Eventos::find($id);
        if ($agenda->delete()) {

            alert()->error('Evento Deletado Com Sucesso.');

            return redirect()->route('lista.agendas');
        }
    }

}
