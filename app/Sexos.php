<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sexos extends Model
{

    public $timestamps = false;
    protected $fillable = array('nome_sexo');

}
