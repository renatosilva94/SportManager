<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ligas extends Model
{

    use SoftDeletes;

    public $timestamps = false;
    protected $fillable = array('nome_liga', 'usuario_id');

    public function times()
    {
        return $this->belongsToMany('App\Times', 'liga_time');
    }

}
