<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecuperarSenha extends Model
{

    protected $fillable = [
        'email', 'token',
    ];

}
