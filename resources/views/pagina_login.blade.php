<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>SportManager</title>

        <!-- Bootstrap core CSS -->

        <link href="css/bootstrap.min.css" rel="stylesheet">

        <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
        <link href="css/animate.min.css" rel="stylesheet">

        <!-- Custom styling plus plugins -->
        <link href="css/custom.css" rel="stylesheet">
        <link href="css/icheck/flat/green.css" rel="stylesheet">


        <script src="js/jquery.min.js"></script>




        <script>
            function verifica() {
                senha = document.getElementById("senha").value;
                forca = 0;
                mostra = document.getElementById("mostra");
                if ((senha.length >= 4) && (senha.length <= 7)) {
                    forca += 1;
                } else if (senha.length > 7) {
                    forca += 2;
                }
                if (senha.match(/[a-z]+/)) {
                    forca += 10;
                }
                if (senha.match(/[A-Z]+/)) {
                    forca += 10;
                }
                if (senha.match(/d+/)) {
                    forca += 15;
                }
                if (senha.match(/W+/)) {
                    forca += 25;
                }
                return mostra_res();
            }
            function mostra_res() {
                if (forca < 10) {
                    mostra.innerHTML = '<tr><td bgcolor="red" width="' + forca + '"></td><td> &nbsp; Fraca </td></tr>';
                } else if ((forca >= 10) && (forca < 15)) {
                    mostra.innerHTML = '<tr><td bgcolor="yellow" width="' + forca + '"></td><td> &nbsp; Justa </td></tr>';
                    ;
                } else if ((forca >= 15) && (forca < 20)) {
                    mostra.innerHTML = '<tr><td bgcolor="blue" width="' + forca + '"></td><td> &nbsp; Forte </td></tr>';
                } else {
                    mostra.innerHTML = '<tr><td bgcolor="green" width="' + forca + '"></td><td> &nbsp; Excelente </td></tr>';
                }
            }
        </script>

        <!--[if lt IE 9]>
              <script src="../assets/js/ie8-responsive-file-warning.js"></script>
              <![endif]-->

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
                <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
              <![endif]-->

    </head>




    <body style="background:#F7F7F7;">




        @include('sweetalert::alert')





        <div class="">
            <a class="hiddenanchor" id="toregister"></a>
            <a class="hiddenanchor" id="tologin"></a>

            <div id="wrapper">

                <div id="login" class="animate form">
                    <section class="login_content">
                        <form method="POST" action="{{route('login.efetuar')}}">






                            {{ csrf_field() }}



                            <h1>Efetuar Login</h1>


                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>



                            <div>




                                <input type="text" class="form-control" placeholder="E-Mail" name="email" required="" />
                            </div>
                            <div>
                                <input type="password" class="form-control" placeholder="Senha" name="password" required=""  />
                            </div>
                            <a href="{{ url('/auth/facebook') }}" class="btn btn-facebook"><i class="fa fa-facebook"></i> Facebook</a>

                            <div>
                                <button class="btn btn-default submit" type="submit">Entrar</button>

                                <a class="reset_pass" href="{{route('pagina.recuperar.senha')}}">Esqueceu sua senha?</a>
                            </div>
                            <div class="clearfix"></div>
                            <div class="separator">

                                <p class="change_link">Novo no site?
                                    <a href="#toregister" class="to_register"> <b>Crie uma conta</b> </a>
                                </p>
                                <div class="clearfix"></div>
                                <br />

                            </div>
                        </form>


                        <!-- form -->
                    </section>
                    <!-- content -->
                </div>















                <div id="register" class="animate form">
                    <section class="login_content">
                        <form method="POST" action="{{route('cadastro.efetuar')}}">
                            {{ csrf_field() }}

                            <h1>Criar Conta</h1>
                            <div>
                                <input type="text" class="form-control" placeholder="E-Mail" name="email" required="" />
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <input type="password" id="senha" onkeyup="javascript:verifica()"  class="form-control" placeholder="Senha (Mínimo 8 Caracteres)" name="password" required="" />
                                </div>
                                <div class="col-sm-6">

                                    <table id="mostra"></table>
                                </div>
                            </div>

                            <div>
                                <input type="text" class="form-control" placeholder="Nome" name="nome" required="" />
                            </div>


                            <div>
                                <input type="text" class="form-control" placeholder="Sobrenome" name="sobrenome" required="" />
                            </div>




                            <div>
                                <select class="form-control" id="sexo_id" name="sexo_id">
                                    @foreach($sexos as $sexo)
                                    <option value="{{$sexo->id}}" name="sexo_id">{{$sexo->nome_sexo}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <br>

                            <div>
                                <input type="date" class="form-control" placeholder="Data de Nascimento" name="data_nascimento" required="" />
                            </div>

                            <br>
                            <div>
                                <button class="btn btn-default submit" type="submit">Cadastrar-Se</button>
                                <a href="{{ url('/auth/facebook') }}" class="btn btn-facebook"><i class="fa fa-facebook"></i> Facebook</a>
                            </div>
                            <div class="clearfix"></div>
                            <div class="separator">

                                <p class="change_link">Já é usuário ?
                                    <a href="#tologin" class="to_register"> <b>Faça o Login</b> </a>
                                </p>
                                <div class="clearfix"></div>
                                <br />

                            </div>
                        </form>
                        <!-- form -->
                    </section>
                    <!-- content -->
                </div>
            </div>
        </div>

    </body>

</html>
