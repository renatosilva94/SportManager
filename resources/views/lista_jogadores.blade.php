@extends('dashboard')

@section('conteudo')





<div class="modal fade" id="modalNovoJogador" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel"><b>Novo Jogador</b></h3>
            </div>
            <div class="modal-body">
                <form method="post" action="{{route('salvar.jogador')}}" enctype="multipart/form-data">

                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Nome:</label>
                        <input type="text" class="form-control" id="nome" name="nome" required>
                    </div>

                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Sobrenome:</label>
                        <input type="text" class="form-control" id="sobrenome" name="sobrenome" required>
                    </div>



                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">CPF:</label>
                        <input type="text" class="form-control" id="cpf" name="cpf">
                    </div>

                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">RG:</label>
                        <input type="text" class="form-control" id="rg" name="rg">
                    </div>

                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">E-Mail:</label>
                        <input type="text" class="form-control" id="email" name="email" required>
                    </div>

                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Função:</label>
                        <input type="text" class="form-control" id="funcao" name="funcao">
                    </div>

                    <div class="form-group">
                        <label for="imagem_jogador"> Imagem do Jogador: </label>
                        <input type="file" id="imagem_jogador" name="imagem_jogador"
                               onchange="previewFile()"
                               class="form-control">
                    </div>


                    <div class="col-sm-6">

                        {!!"<img src='imagens_jogadores/sem_foto.png' height='150px' width='150px' id='imagem_jogador_preview' alt='Foto do Jogador' class='img-circle'>"!!}

                    </div>

                    <script>
                        function previewFile() {
                            var preview = document.getElementById('imagem_jogador_preview');
                            var file = document.getElementById('imagem_jogador').files[0];
                            var reader = new FileReader();

                            reader.onloadend = function () {
                                preview.src = reader.result;
                            };

                            if (file) {
                                reader.readAsDataURL(file);
                            } else {
                                preview.src = "";
                            }
                        }

                    </script>



            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-dark" data-dismiss="modal">Fechar</button>
                <button type="submit" class="btn btn-primary">Salvar Jogador</button>

                </form>

            </div>
        </div>
    </div>
</div>















<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="dashboard_graph">

            <div class="row x_title">
                <div class="col-md-6">
                    <h3>Jogadores <small> Listagem </small></h3>
                </div>


                <br>
                <br>


                <div class="col-md-12">








                    <div class='col-sm-4'>
                        <div class="form-group">
                            <input type="text" class="form-control" id="pesquisa_nome_jogador"
                                   name="pesquisa_nome_jogador" placeholder="Pesquisar (Nome do Jogador)">
                        </div>
                    </div>

                    <script>
                        $('#pesquisa_nome_jogador').on('keyup', function () {

                            $value = $(this).val();

                            $.ajax({

                                type: 'get',

                                url: '{{URL::to('buscaJogadoresAjax')}}',

                                data: {'buscaJogadoresAjax': $value},

                                success: function (data) {

                                    $('tbody').html(data);

                                }

                            });



                        })
                    </script>




                    <div class='col-sm-4'>
                        <label> &nbsp; </label>
                        <label> &nbsp; </label>

                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalNovoJogador">Novo Jogador <span class="glyphicon glyphicon-plus" style="color:white"></span></button>
                        <a href="{{route('pdf.lista.jogadores')}}" class="btn btn-info">Gerar Pdf <span class="glyphicon glyphicon-file" style="color:white"></span></a>
                        <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modalRestaurarJogadores">Restaurar Jogadores <span class="glyphicon glyphicon-time" style="color:white"></span> </button>

                    </div>

















                </div>







            </div>
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12">



            @if (count($jogadores)==0)
            <div class="alert alert-danger">
                Não existem jogadores com os filtros informados.
            </div>
            @endif

            <table class="table table-striped table-bordered nowrap">
                <thead>
                    <tr>
                        <th style="text-align: center">Foto</th>

                        <th style="text-align: center">Nome</th>
                        <th style="text-align: center">Sobrenome</th>
                        <th style="text-align: center">Função</th>

                        <th style="text-align: center; color: #007f00"><b>Ausencias</b></th>
                        <th style="text-align: center; color: #ccac00"><b>Cartões Amarelos</b></th>
                        <th style="text-align: center; color: #cc0000"><b>Cartões Vermelhos</b></th>
                        <th style="text-align: center; color: #120a8f">Gols</th>



                        <th style="text-align: center">E-Mail</th>




                        <th style="text-align: center">Ações</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($jogadores as $jogador)







                    <!-- Editar Jogador -->

                <div class="modal fade" id="modalEditarJogador{{$jogador->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h3 class="modal-title" id="exampleModalLabel"><b>Editar Jogador</b></h3>
                            </div>
                            <div class="modal-body">
                                <form method="post" action="{{route('atualizar.jogador', $jogador->id)}}" enctype="multipart/form-data">

                                    {{ csrf_field() }}

                                    <div class="form-group">
                                        <label for="recipient-name" class="col-form-label">Nome:</label>
                                        <input type="text" class="form-control" id="nome" name="nome" value="{{$jogador->nome or old('nome')}}"
                                               required>
                                    </div>

                                    <div class="form-group">
                                        <label for="recipient-name" class="col-form-label">Sobrenome:</label>
                                        <input type="text" class="form-control" id="sobrenome" name="sobrenome" value="{{$jogador->sobrenome or old('sobrenome')}}" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="recipient-name" class="col-form-label">CPF:</label>
                                        <input type="text" class="form-control" id="cpf" name="cpf" value="{{$jogador->cpf or old('cpf')}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="recipient-name" class="col-form-label">RG:</label>
                                        <input type="text" class="form-control" id="rg" name="rg" value="{{$jogador->rg or old('rg')}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="recipient-name" class="col-form-label">E-Mail:</label>
                                        <input type="text" class="form-control" id="email" name="email" value="{{$jogador->email or old('email')}}" required>
                                    </div>


                                    <div class="form-group">
                                        <label for="recipient-name" class="col-form-label">Função:</label>
                                        <input type="text" class="form-control" id="funcao" name="funcao" value="{{$jogador->funcao or old('funcao')}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="imagem_jogador"> Imagem do Jogador: </label>
                                        <input type="file" id="imagem_jogador_editar" name="imagem_jogador"
                                               class="form-control">
                                    </div>


                                    <div class="col-sm-6">

                                        @php
                                        if(file_exists(public_path('imagens_jogadores/'.$jogador->id.'.png'))){
                                        $imagem_jogador = '../imagens_jogadores/'.$jogador->id.'.png';
                                        } else {
                                        $imagem_jogador = '../imagens_jogadores/sem_foto.png';
                                        }
                                        @endphp

                                        {!!"<img src=$imagem_jogador height='150px' width='150px' id='imagem_jogador_preview_editar' alt='Foto do Jogador' class='img-circle'>"!!}

                                    </div>

                                    <script>
                                            window.onload = () => {
                                                // console.log('asdasd', $('#imagem_liga_editar'));
                                                $('input[type="file"]').on('change', e => {
                                                    const activeModal = $('.modal.fade.in');
                                                    const preview = activeModal.find('#imagem_jogador_preview_editar');
                                                    const file = e.target.files[0];
                                                    const reader = new FileReader();
                                                    
                                                    reader.onload = function () {
                                                        preview.attr('src', reader.result);
                                                    };
                                                    
                                                    if (file) {
                                                        reader.readAsDataURL(file);
                                                    } else {
                                                        preview.src = "";
                                                    }
                                                });
                                                
                                               
                                            }

                                        </script>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-dark" data-dismiss="modal">Fechar</button>
                                <button type="submit" class="btn btn-primary">Atualizar Jogador</button>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>




                <!-- Fim do Editar Jogador -->































                <!-- Modal -->
                <div class="modal fade" id="modalVerTimes{{$jogador->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel"><h3>{{$jogador->nome}} - Times</h3></h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                @if ($jogador->times->count() > 0)

                                <ul>

                                    @foreach($jogador->times as $time)

                                    <li><h5>{{ $time->nome_time }}</h5></li>

                                    @endforeach

                                </ul>

                                @endif

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-dark" data-dismiss="modal">Fechar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Fim da MODAL-->



















                <tr>


                    <td style="text-align: center">


                        @php
                        if(file_exists(public_path('imagens_jogadores/'.$jogador->id.'.png'))){
                        $imagem_jogador = '../imagens_jogadores/'.$jogador->id.'.png';
                        } else {
                        $imagem_jogador = '../imagens_jogadores/sem_foto.png';
                        }
                        @endphp

                        {!!"<a href=$imagem_jogador data-lightbox='roadtrip' data-lightbox-gallery='gallery1' data-lightbox-hidpi=$imagem_jogador>"!!}
                            {!!"<img src=$imagem_jogador id='imagem' class='img-circle' width='50' height='50' alt='Imagem do Jogador' data-zoom-image=$imagem_jogador>"!!}
                        </a>

                    </td>







                    <td style="text-align: center">{{$jogador->nome}}</td>

                    <td style="text-align: center">{{$jogador->sobrenome}}</td>
                    <td style="text-align: center">{{$jogador->funcao}}</td>


                    <td style="text-align: center; color: #007f00"><a href="{{route('remover.ausencia', $jogador->id)}}" class="btn btn-success btn-xs" id="removerAusencia">-</a>&nbsp;<b>{{$jogador->ausente}}</b>&nbsp;&nbsp;<a href="{{route('adicionar.ausencia', $jogador->id)}}" class="btn btn-success btn-xs" id="adicionarAusencia">+</a> </td>

                    <td style="text-align: center; color: #ccac00"><a href="{{route('remover.cartao.amarelo', $jogador->id)}}" class="btn btn-warning btn-xs">-</a>&nbsp;<b>{{$jogador->cartao_amarelo}}</b>&nbsp;&nbsp;<a href="{{route('adicionar.cartao.amarelo', $jogador->id)}}" class="btn btn-warning btn-xs">+</a> </td>





                    <td style="text-align: center; color: #cc0000"><a href="{{route('remover.cartao.vermelho', $jogador->id)}}" class="btn btn-danger btn-xs">-</a>&nbsp;<b>{{$jogador->cartao_vermelho}}</b>&nbsp;&nbsp;<a href="{{route('adicionar.cartao.vermelho', $jogador->id)}}" class="btn btn-danger btn-xs">+</a></td>

                    <td style="text-align: center; color: #120a8f"><a href="{{route('remover.gol', $jogador->id)}}" class="btn btn-info btn-xs">-</a>&nbsp;<b> {{$jogador->gols}} </b>&nbsp;&nbsp;<a href="{{route('adicionar.gol', $jogador->id)}}" class="btn btn-info btn-xs">+</a>  </td>






                    <td style="text-align: center">{{$jogador->email}}</td>




                    <td style="text-align: center">

                        <form style="display: inline"
                              method="post"
                              action="{{route('deletar.jogador', $jogador->id)}}"
                              onsubmit="return confirm('Confirma Exclusão do Jogador?')">
                            {{method_field('delete')}}
                            {{csrf_field()}}

                            <input title="Deletar Jogador" type="image" src="icones/deletar.png" width="30px" height="30px" >





                        </form> &nbsp;&nbsp;


                        <img title="Ver Times Vinculados" src="icones/times.png" width="30px" height="30px" data-toggle="modal" data-target="#modalVerTimes{{$jogador->id}}" >






                        &nbsp;&nbsp; <img title="Editar Jogador" src="icones/editar.png" width="30px" height="30px" data-toggle="modal" data-target="#modalEditarJogador{{$jogador->id}}" >  &nbsp;&nbsp;






                    </td>
                    @endforeach

                    <div class="modal fade" id="modalRestaurarJogadores" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel" style="text-align: left" ><b>Restaurar Jogadores</b></h3>
            </div>
            <div class="modal-body">
                
            @foreach($jogadoresDeletados as $jogadorDeletado)
                
                @if($jogadorDeletado->trashed())
                <ul style="text-align: left;">
                <li>{{$jogadorDeletado->nome}} {{$jogadorDeletado->sobrenome}} </li>
                <li>
                    
                <form method="post"
                                  action="{{route('restaurar.jogador', $jogadorDeletado->id)}}"
                                  onsubmit="return confirm('Deseja Restaurar Essa Jogador ?')">

                                {{csrf_field()}}

                    
                    
                    <input type="submit" class="btn btn-dark btn-xs" value="Restaurar">
                    
                    
                    
                    
                    </li>
                </ul>
                @endif

            @endforeach

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-dark" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

                </tr>


                </tbody>
            </table>

            {{ $jogadores->links() }}

            <h5>
                Legenda
            </h5>

            <p><img title="Editar Jogador" src="icones/editar.png" width="20px" height="20px"> Editar Jogador | <img title="Deletar Jogador" src="icones/deletar.png" width="20px" height="20px"> Deletar Jogador | <img title="Ver Times Vinculados" src="icones/times.png" width="20px" height="20px"> Ver Times Vinculados </p>


        </div>





    </div>

    <div class="clearfix"></div>
</div>
</div>

</div>
<br />


@endsection
