<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>SportManager</title>

        <!-- Bootstrap core CSS -->

        <link href="css/bootstrap.min.css" rel="stylesheet">

        <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
        <link href="css/animate.min.css" rel="stylesheet">

        <!-- Custom styling plus plugins -->
        <link href="css/custom.css" rel="stylesheet">
        <link href="css/icheck/flat/green.css" rel="stylesheet">


        <script src="js/jquery.min.js"></script>



        <script>
            function verifica() {
                senha = document.getElementById("senha").value;
                forca = 0;
                mostra = document.getElementById("mostra");
                if ((senha.length >= 4) && (senha.length <= 7)) {
                    forca += 10;
                } else if (senha.length > 7) {
                    forca += 25;
                }
                if (senha.match(/[a-z]+/)) {
                    forca += 10;
                }
                if (senha.match(/[A-Z]+/)) {
                    forca += 20;
                }
                if (senha.match(/d+/)) {
                    forca += 20;
                }
                if (senha.match(/W+/)) {
                    forca += 25;
                }
                return mostra_res();
            }
            function mostra_res() {
                if (forca < 30) {
                    mostra.innerHTML = '<tr><td bgcolor="red" width="' + forca + '"></td><td> &nbsp; Fraca </td></tr>';
                } else if ((forca >= 30) && (forca < 60)) {
                    mostra.innerHTML = '<tr><td bgcolor="yellow" width="' + forca + '"></td><td> &nbsp; Justa </td></tr>';
                    ;
                } else if ((forca >= 60) && (forca < 85)) {
                    mostra.innerHTML = '<tr><td bgcolor="blue" width="' + forca + '"></td><td> &nbsp; Forte </td></tr>';
                } else {
                    mostra.innerHTML = '<tr><td bgcolor="green" width="' + forca + '"></td><td> &nbsp; Excelente </td></tr>';
                }
            }
        </script>

        <!--[if lt IE 9]>
              <script src="../assets/js/ie8-responsive-file-warning.js"></script>
              <![endif]-->

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
                <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
              <![endif]-->

    </head>




    <body style="background:#F7F7F7;">


        @include('sweetalert::alert')


        <div class="">
            <a class="hiddenanchor" id="toregister"></a>
            <a class="hiddenanchor" id="tologin"></a>


            <div id="wrapper">

                <div id="login" class="animate form">
                    <section class="login_content">
                        <form method="POST" action="/password/enviarEmailRecuperacao">
                            {{ csrf_field() }}

                            <h1>Recuperar Senha</h1>





                            <div>
                                <input type="text" class="form-control" placeholder="E-Mail" name="email" required="" />
                            </div>


                            <div>
                                <button class="btn btn-default submit" type="submit">Enviar Link de Recuperação</button>
                                <a href="{{route('pagina.login')}}" class="to_register"> <b>Voltar</b> </a>

                            </div>

                            <div class="clearfix"></div>
                            <div class="separator">



                                <div class="clearfix"></div>
                                <br />

                            </div>
                        </form>


                        <!-- form -->
                    </section>
                    <!-- content -->
                </div>

















                </body>

                </html>
