<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>SportManager - Dashboard</title>

        <!-- Bootstrap core CSS -->

        <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
        <script src="{{asset('js/jquery.min.js')}}"></script>

        <link href="{{asset('fonts/css/font-awesome.min.css')}}" rel="stylesheet">
        <link href="{{asset('css/animate.min.css')}}" rel="stylesheet">


        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/lang/pt-br.js"></script>


        <!-- Custom styling plus plugins -->
        <link href="{{asset('css/custom.css')}}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{asset('css/maps/jquery-jvectormap-2.0.3.css')}}" />
        <link href="{{asset('css/icheck/flat/green.css')}}" rel="stylesheet" />
        <link href="{{asset('css/floatexamples.css')}}" rel="stylesheet" type="text/css" />

        <script src="{{asset('js/nprogress.js')}}"></script>
        <script src="{{asset('js/easypie/jquery.easypiechart.min.js')}}"></script>

        <link href="{{asset('css/lightbox.css')}}" rel="stylesheet">
        <script src="{{asset('js/lightbox.js')}}"></script>


        <script>
$(function () {
 $('.chart').easyPieChart({
     easing: 'easeOutBounce',
     lineWidth: '6',
     barColor: '#75BCDD',
     onStep: function (from, to, percent) {
         $(this.el).find('.percent').text(Math.round(percent));
     }
 });
 var chart = window.chart = $('.chart').data('easyPieChart');
 $('.js_update').on('click', function () {
     chart.update(Math.random() * 200 - 100);
 });

 //hover and retain popover when on popover content
 var originalLeave = $.fn.popover.Constructor.prototype.leave;
 $.fn.popover.Constructor.prototype.leave = function (obj) {
     var self = obj instanceof this.constructor ?
             obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type)
     var container, timeout;

     originalLeave.call(this, obj);

     if (obj.currentTarget) {
         container = $(obj.currentTarget).siblings('.popover')
         timeout = self.timeout;
         container.one('mouseenter', function () {
             //We entered the actual popover – call off the dogs
             clearTimeout(timeout);
             //Let's monitor popover content instead
             container.one('mouseleave', function () {
                 $.fn.popover.Constructor.prototype.leave.call(self, self);
             });
         })
     }
 };
 $('body').popover({
     selector: '[data-popover]',
     trigger: 'click hover',
     delay: {
         show: 50,
         hide: 400
     }
 });

});
        </script>


        <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Modal de Edição de Foto -->

    <div class="modal fade" id="modalNovaFoto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel"><b>Foto</b></h3>
                </div>
                <div class="modal-body">
                    <form method="post" action="{{route('salvar.foto.perfil')}}" enctype="multipart/form-data">

                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="foto_perfil"> Foto de Perfil: </label>
                            <input type="file" id="foto_perfil" name="foto_perfil"
                                   onchange="previewFile()"
                                   class="form-control">
                        </div>


                        <div class="col-sm-6">

                            {!!"<img src='imagens_usuarios/sem_foto.png' width='150px' height='150px' id='foto_perfil_preview' alt='Foto do Perfil' class='img-circle'>"!!}

                        </div>

                        <script>
                            function previewFile() {
                                var preview = document.getElementById('foto_perfil_preview');
                                var file = document.getElementById('foto_perfil').files[0];
                                var reader = new FileReader();

                                reader.onloadend = function () {
                                    preview.src = reader.result;
                                };

                                if (file) {
                                    reader.readAsDataURL(file);
                                } else {
                                    preview.src = "";
                                }
                            }

                        </script>



                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    <button type="submit" class="btn btn-primary">Salvar Foto</button>

                    </form>

                </div>
            </div>
        </div>
    </div>




    <!-- Fim da Modal de Edição de Foto -->










    <!-- Fim da Modal de Edição de Foto -->











    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

    </script>

</head>


<body class="nav-md">

    @include('sweetalert::alert')


    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">

                    <div class="clearfix"></div>

                    <!-- menu prile quick info -->
                    <div class="profile">
                        <div class="profile_pic">


                            @php
                            if(file_exists(public_path('imagens_usuarios/'. $usuario_autenticado_id.'.jpg'))){
                            $foto = '../imagens_usuarios/'.$usuario_autenticado_id.'.jpg';

                            } else if (!empty ( Auth::user()->avatar )) {

                            $foto = Auth::user()->avatar;

                            }

                            else {
                            $foto = '../imagens_usuarios/sem_foto.png';
                            }
                            @endphp












                            {!!"<img src=$foto id='imagem_perfil' alt='Foto do Usuario' class='img-circle profile_img'>"!!}
                        </div>
                        <div class="profile_info">
                            <span>Bem-Vindo,</span>
                            <h2>{{$usuario_autenticado_nome}}</h2>
                        </div>
                    </div>
                    <!-- /menu prile quick info -->


                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                        <div class="menu_section">
                            <br>
                            <br>

                            <h3>Menu</h3>
                            <ul class="nav side-menu">
                                <li><a href="{{route('pagina.inicial')}}"><i class="fa fa-home"></i> Inicio </li></a>

                                <li><a href="{{route('lista.agendas')}}"><i class="fa fa-bullhorn"></i> Agenda </a> </li>


                                <li><a href="{{route('lista.ligas')}}"><i class="fa fa-sitemap"></i> Ligas </a> </li>

                                <li><a href="{{route('lista.times')}}"><i class="fa fa-futbol-o"></i> Times  </a></li>

                                <li><a href="{{route('lista.jogadores')}}"><i class="fa fa-male"></i> Jogadores </a></li>


                                <li><a><i class="fa fa-line-chart"></i> Placares <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">

                                        @foreach($ligas as $liga)
                                        <li><a href="{{route('lista.placares', $liga->id)}}">{{$liga->nome_liga}}</a></li>
                                        @endforeach


                                    </ul>
                                </li>



                                <li><a href="{{route('lista.jogos')}}"><i class="fa fa-arrows-alt"></i> Jogos </a></li>




                                <li><a><i class="fa fa-sort-amount-desc"></i> Ranking <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="{{route('lista.ranking.times')}}">Ranking de Times</a></li>
                                        <li><a href="{{route('lista.ranking.jogadores')}}">Ranking de Jogadores</a></li>

                                    </ul>
                                </li>


                                <li><a><i class="fa fa-gear"></i> Configurações <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a data-toggle="modal" data-target="#modalEditarPerfil{{$usuario_autenticado_id}}">Editar Perfil</a>
                                        </li>

                                        @if(empty ( Auth::user()->provider ))
                                        <li><a data-toggle="modal" data-target="#modalNovaFoto" data-id="{{$usuario_autenticado_id}}">Editar Foto</a>
                                            @else 

                                            @endif
                                        </li>



                                    </ul>
                                </li>














                            </ul>
                        </div>
                    </div>
                    <!-- /sidebar menu -->

                    <script>
                        function toggleFullScreen() {
                            if ((document.fullScreenElement && document.fullScreenElement !== null) ||
                                    (!document.mozFullScreen && !document.webkitIsFullScreen)) {
                                if (document.documentElement.requestFullScreen) {
                                    document.documentElement.requestFullScreen();
                                } else if (document.documentElement.mozRequestFullScreen) {
                                    document.documentElement.mozRequestFullScreen();
                                } else if (document.documentElement.webkitRequestFullScreen) {
                                    document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
                                }
                            } else {
                                if (document.cancelFullScreen) {
                                    document.cancelFullScreen();
                                } else if (document.mozCancelFullScreen) {
                                    document.mozCancelFullScreen();
                                } else if (document.webkitCancelFullScreen) {
                                    document.webkitCancelFullScreen();
                                }
                            }
                        }

                    </script>

                    <!-- /menu footer buttons -->
                    <div class="sidebar-footer hidden-small">
                        <a data-toggle="tooltip" data-placement="top" title="Configurações">
                            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Tela Inteira" onclick="toggleFullScreen()">
                            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Bloquear">
                            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                        </a>
                        <a href="{{route('efetuar.logout')}}" data-toggle="tooltip" data-placement="top" title="Sair">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                        </a>
                    </div>
                    <!-- /menu footer buttons -->
                </div>
            </div>

            <!-- top navigation -->
            <div class="top_nav">

                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li role="presentation" class="dropdown">
                                <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                                    <i class="fa fa-bell-o fa-lg"></i>

                                    @if(Carbon\Carbon::now()->isMonday())
                                    <span class="badge bg-blue">{{$eventosNotificacoesContagem}}</span>
                                    @elseif(Carbon\Carbon::now()->isTuesday())
                                    <span class="badge bg-red">{{$eventosNotificacoesContagem}}</span>
                                    @elseif(Carbon\Carbon::now()->isWednesday())
                                    <span class="badge bg-green">{{$eventosNotificacoesContagem}}</span>
                                    @elseif(Carbon\Carbon::now()->isThursday())
                                    <span class="badge bg-purple">{{$eventosNotificacoesContagem}}</span>
                                    @elseif(Carbon\Carbon::now()->isFriday())
                                    <span class="badge bg-blue">{{$eventosNotificacoesContagem}}</span>
                                    @elseif(Carbon\Carbon::now()->isSaturday())
                                    <span class="badge bg-red">{{$eventosNotificacoesContagem}}</span>
                                    @elseif(Carbon\Carbon::now()->isSunday())
                                    <span class="badge bg-green">{{$eventosNotificacoesContagem}}</span>

                                    @else 
                                    <span class="badge bg-dark"></span>

                                    @endif

                                </a>
                                <ul id="menu1" class="dropdown-menu list-unstyled msg_list animated fadeInDown" role="menu">

                                    @foreach($eventosNotificacoes as $eventoNotificacao)


                                    <li>
                                        <a>

                                            <span>

                                                <span>{{$eventoNotificacao->nome_evento}}</span>

                                            </span>

                                            <span class="message">
                                                {{$eventoNotificacao->mensagem}}
                                            </span>

                                            <span>Inicio: {{date('d/m/Y H:i', strtotime($eventoNotificacao->data_inicial))}}</span>
                                            <br>
                                            <span>Fim: {{date('d/m/Y H:i', strtotime($eventoNotificacao->data_final))}}</span>
                                        </a>
                                    </li>
                                    @endforeach



                                </ul>


                                </nav>


                                </div>



                                </div>






                                <!-- /top navigation -->


                                <!-- page content -->
                                <div class="right_col" role="main">

                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="dashboard_graph">



                                                @yield('conteudo')




                                                <div class="clearfix"></div>
                                            </div>
                                        </div>

                                    </div>
                                    <br />

                                </div>
                                <!-- /page content -->

                                </div>

                                </div>

                                <div id="custom_notifications" class="custom-notifications dsp_none">
                                    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
                                    </ul>
                                    <div class="clearfix"></div>
                                    <div id="notif-group" class="tabbed_notifications"></div>
                                </div>

                                <script src="{{asset('js/bootstrap.min.js')}}"></script>

                                <!-- gauge js -->
                                <script type="text/javascript" src="{{asset('js/gauge/gauge.min.js')}}"></script>
                                <script type="text/javascript" src="{{asset('js/gauge/gauge_demo.js')}}"></script>
                                <!-- chart js -->
                                <script src="{{asset('js/chartjs/chart.min.js')}}"></script>
                                <!-- bootstrap progress js -->
                                <script src="{{asset('js/progressbar/bootstrap-progressbar.min.js')}}"></script>
                                <script src="{{asset('js/nicescroll/jquery.nicescroll.min.js')}}"></script>
                                <!-- icheck -->
                                <script src="{{asset('js/icheck/icheck.min.js')}}"></script>
                                <!-- daterangepicker -->
                                <script type="text/javascript" src="{{asset('js/moment/moment.min.js')}}"></script>
                                <script type="text/javascript" src="{{asset('js/datepicker/daterangepicker.js')}}"></script>

                                <script src="{{asset('js/custom.js')}}"></script>

                                <!-- flot js -->
                                <!--[if lte IE 8]><script type="text/javascript" src="js/excanvas.min.js"></script><![endif]-->
                                <script type="text/javascript" src="{{asset('js/flot/jquery.flot.js')}}"></script>
                                <script type="text/javascript" src="{{asset('js/flot/jquery.flot.pie.js')}}"></script>
                                <script type="text/javascript" src="{{asset('js/flot/jquery.flot.orderBars.js')}}"></script>
                                <script type="text/javascript" src="{{asset('js/flot/jquery.flot.time.min.js')}}"></script>
                                <script type="text/javascript" src="{{asset('js/flot/date.js')}}"></script>
                                <script type="text/javascript" src="{{asset('js/flot/jquery.flot.spline.js')}}"></script>
                                <script type="text/javascript" src="{{asset('js/flot/jquery.flot.stack.js')}}"></script>
                                <script type="text/javascript" src="{{asset('js/flot/curvedLines.js')}}"></script>
                                <script type="text/javascript" src="{{asset('js/flot/jquery.flot.resize.js')}}"></script>
                                <script>
                                                                            $(document).ready(function () {
                                                                                // [17, 74, 6, 39, 20, 85, 7]
                                                                                //[82, 23, 66, 9, 99, 6, 2]
                                                                                var data1 = [
                                                                                    [gd(2012, 1, 1), 17],
                                                                                    [gd(2012, 1, 2), 74],
                                                                                    [gd(2012, 1, 3), 6],
                                                                                    [gd(2012, 1, 4), 39],
                                                                                    [gd(2012, 1, 5), 20],
                                                                                    [gd(2012, 1, 6), 85],
                                                                                    [gd(2012, 1, 7), 7]
                                                                                ];

                                                                                var data2 = [
                                                                                    [gd(2012, 1, 1), 82],
                                                                                    [gd(2012, 1, 2), 23],
                                                                                    [gd(2012, 1, 3), 66],
                                                                                    [gd(2012, 1, 4), 9],
                                                                                    [gd(2012, 1, 5), 119],
                                                                                    [gd(2012, 1, 6), 6],
                                                                                    [gd(2012, 1, 7), 9]
                                                                                ];
                                                                                $("#canvas_dahs").length && $.plot($("#canvas_dahs"), [
                                                                                    data1, data2
                                                                                ], {
                                                                                    series: {
                                                                                        lines: {
                                                                                            show: false,
                                                                                            fill: true
                                                                                        },
                                                                                        splines: {
                                                                                            show: true,
                                                                                            tension: 0.4,
                                                                                            lineWidth: 1,
                                                                                            fill: 0.4
                                                                                        },
                                                                                        points: {
                                                                                            radius: 0,
                                                                                            show: true
                                                                                        },
                                                                                        shadowSize: 2
                                                                                    },
                                                                                    grid: {
                                                                                        verticalLines: true,
                                                                                        hoverable: true,
                                                                                        clickable: true,
                                                                                        tickColor: "#d5d5d5",
                                                                                        borderWidth: 1,
                                                                                        color: '#fff'
                                                                                    },
                                                                                    colors: ["rgba(38, 185, 154, 0.38)", "rgba(3, 88, 106, 0.38)"],
                                                                                    xaxis: {
                                                                                        tickColor: "rgba(51, 51, 51, 0.06)",
                                                                                        mode: "time",
                                                                                        tickSize: [1, "day"],
                                                                                        //tickLength: 10,
                                                                                        axisLabel: "Date",
                                                                                        axisLabelUseCanvas: true,
                                                                                        axisLabelFontSizePixels: 12,
                                                                                        axisLabelFontFamily: 'Verdana, Arial',
                                                                                        axisLabelPadding: 10
                                                                                                //mode: "time", timeformat: "%m/%d/%y", minTickSize: [1, "day"]
                                                                                    },
                                                                                    yaxis: {
                                                                                        ticks: 8,
                                                                                        tickColor: "rgba(51, 51, 51, 0.06)",
                                                                                    },
                                                                                    tooltip: false
                                                                                });

                                                                                function gd(year, month, day) {
                                                                                    return new Date(year, month - 1, day).getTime();
                                                                                }
                                                                            });
                                </script>

                                <!-- worldmap -->
                                <script type="text/javascript" src="{{asset('js/maps/jquery-jvectormap-2.0.3.min.js')}}"></script>
                                <script type="text/javascript" src="{{asset('js/maps/gdp-data.js')}}"></script>
                                <script type="text/javascript" src="{{asset('js/maps/jquery-jvectormap-world-mill-en.js')}}"></script>
                                <script type="text/javascript" src="{{asset('js/maps/jquery-jvectormap-us-aea-en.js')}}"></script>
                                <!-- pace -->
                                <script src="{{asset('js/pace/pace.min.js')}}"></script>
                                <script>
                                                                            $(function () {
                                                                                $('#world-map-gdp').vectorMap({
                                                                                    map: 'world_mill_en',
                                                                                    backgroundColor: 'transparent',
                                                                                    zoomOnScroll: false,
                                                                                    series: {
                                                                                        regions: [{
                                                                                                values: gdpData,
                                                                                                scale: ['#E6F2F0', '#149B7E'],
                                                                                                normalizeFunction: 'polynomial'
                                                                                            }]
                                                                                    },
                                                                                    onRegionTipShow: function (e, el, code) {
                                                                                        el.html(el.html() + ' (GDP - ' + gdpData[code] + ')');
                                                                                    }
                                                                                });
                                                                            });
                                </script>
                                <!-- skycons -->
                                <script src="{{asset('js/skycons/skycons.min.js')}}"></script>
                                <script>
                                                                            var icons = new Skycons({
                                                                                "color": "#73879C"
                                                                            }),
                                                                                    list = [
                                                                                        "clear-day", "clear-night", "partly-cloudy-day",
                                                                                        "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
                                                                                        "fog"
                                                                                    ],
                                                                                    i;

                                                                            for (i = list.length; i--; )
                                                                                icons.set(list[i], list[i]);

                                                                            icons.play();
                                </script>

                                <!-- dashbord linegraph -->
                                <script>
                                    var doughnutData = [{
                                            value: 30,
                                            color: "#455C73"
                                        }, {
                                            value: 30,
                                            color: "#9B59B6"
                                        }, {
                                            value: 60,
                                            color: "#BDC3C7"
                                        }, {
                                            value: 100,
                                            color: "#26B99A"
                                        }, {
                                            value: 120,
                                            color: "#3498DB"
                                        }];
                                    var myDoughnut = new Chart(document.getElementById("canvas1").getContext("2d")).Doughnut(doughnutData);
                                </script>
                                <!-- /dashbord linegraph -->
                                <!-- datepicker -->
                                <script type="text/javascript">
                                    $(document).ready(function () {

                                        var cb = function (start, end, label) {
                                            console.log(start.toISOString(), end.toISOString(), label);
                                            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                                            //alert("Callback has fired: [" + start.format('MMMM D, YYYY') + " to " + end.format('MMMM D, YYYY') + ", label = " + label + "]");
                                        }

                                        var optionSet1 = {
                                            startDate: moment().subtract(29, 'days'),
                                            endDate: moment(),
                                            minDate: '01/01/2012',
                                            maxDate: '12/31/2015',
                                            dateLimit: {
                                                days: 60
                                            },
                                            showDropdowns: true,
                                            showWeekNumbers: true,
                                            timePicker: false,
                                            timePickerIncrement: 1,
                                            timePicker12Hour: true,
                                            ranges: {
                                                'Today': [moment(), moment()],
                                                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                                                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                                                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                                                'This Month': [moment().startOf('month'), moment().endOf('month')],
                                                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                                            },
                                            opens: 'left',
                                            buttonClasses: ['btn btn-default'],
                                            applyClass: 'btn-small btn-primary',
                                            cancelClass: 'btn-small',
                                            format: 'MM/DD/YYYY',
                                            separator: ' to ',
                                            locale: {
                                                applyLabel: 'Submit',
                                                cancelLabel: 'Clear',
                                                fromLabel: 'From',
                                                toLabel: 'To',
                                                customRangeLabel: 'Custom',
                                                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                                                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                                                firstDay: 1
                                            }
                                        };
                                        $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
                                        $('#reportrange').daterangepicker(optionSet1, cb);
                                        $('#reportrange').on('show.daterangepicker', function () {
                                            console.log("show event fired");
                                        });
                                        $('#reportrange').on('hide.daterangepicker', function () {
                                            console.log("hide event fired");
                                        });
                                        $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
                                            console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
                                        });
                                        $('#reportrange').on('cancel.daterangepicker', function (ev, picker) {
                                            console.log("cancel event fired");
                                        });
                                        $('#options1').click(function () {
                                            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
                                        });
                                        $('#options2').click(function () {
                                            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
                                        });
                                        $('#destroy').click(function () {
                                            $('#reportrange').data('daterangepicker').remove();
                                        });
                                    });
                                </script>
                                <script>
                                    NProgress.done();
                                </script>
                                <!-- /datepicker -->
                                <!-- /footer content -->

                                <!-- Modal de Edição de Perfil -->

                                <div class="modal fade" id="modalEditarPerfil{{$usuario_autenticado_id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h3 class="modal-title" id="exampleModalLabel"><b>Editar Perfil</b></h3>
                                            </div>
                                            <div class="modal-body">
                                                <form method="post" action="{{route('atualizar.meu.perfil', $usuario_autenticado_id)}}">

                                                    {{ csrf_field() }}

                                                    @if(empty ( Auth::user()->provider ))

                                                    <div class="form-group">
                                                        <label for="recipient-name" class="col-form-label">Nome:</label>
                                                        <input type="text" class="form-control" id="nome" name="nome" value="{{Auth::guard()->user()->nome}}"
                                                               >
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="recipient-name" class="col-form-label">Sobrenome:</label>
                                                        <input type="text" class="form-control" id="sobrenome" name="sobrenome" value="{{Auth::guard()->user()->sobrenome}}"
                                                               >
                                                    </div>

                                                    @else

                                                    @endif


                                                    <div class="form-group">
                                                        <label for="recipient-name" class="col-form-label">Data de Nascimento:</label>
                                                        <input type="date" class="form-control" id="data_nascimento" name="data_nascimento" value="{{Auth::guard()->user()->data_nascimento}}"
                                                               >
                                                    </div>


                                                    @if(empty ( Auth::user()->provider ))


                                                    <div class="form-group">
                                                        <label for="recipient-name" class="col-form-label">E-Mail:</label>
                                                        <input type="text" class="form-control" id="email" name="email" value="{{Auth::guard()->user()->email}}"
                                                               >
                                                    </div>

                                                    @else 

                                                    @endif


                                                    <div>
                                                        <label for="sexo_id" class="col-form-label">Sexo:</label>
                                                        <select class="form-control" id="sexo_id" name="sexo_id">
                                                            @foreach($sexos as $sexo)
                                                            <option value="{{$sexo->id}}" name="sexo_id">{{$sexo->nome_sexo}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>


                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                                        <button type="submit" class="btn btn-primary">Atualizar Perfil</button>

                                                </form>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </body>

                                </html>
