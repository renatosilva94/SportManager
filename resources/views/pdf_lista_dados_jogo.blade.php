<html>
   <head>
      <style>
         table {
         font-family: arial;
         border-collapse: collapse;
         width: 100%;
         }
         td, th {
         border: 1px solid #dddddd;
         text-align: left;
         padding: 8px;
         opacity: .7
         }
         tr:nth-child(even) {
         background-color: #dddddd;
         }
         #watermark {
         position: fixed;
         /** 
         Set a position in the page for your image
         This should center it vertically
         **/
         bottom:   10cm;
         left:     5.5cm;
         /** Change image dimensions**/
         width:    10cm;
         height:   8cm;
         /** Your watermark should be behind every content**/
         z-index:  -1000;
         opacity: .3
         }
      </style>
   </head>
   <h3 align="center">Sumula e Relatório da Partida</h3>
   <p align="center">SportManager</p>
   <p align="center">Emissão: <?php date_default_timezone_set("America/Sao_Paulo");
      echo date('d/m/Y H:i:s') ?></p>
   <p align="center">Jogo Nº: {{$id}}</p>
   <br>
   <br>
   <body>
      <div id="watermark">
         <img src="https://i.imgur.com/BNUpuXa.png" height="100%" width="100%" />
      </div>
      <main>
         @foreach($jogos as $jogo)
         Data 
         <p> {{date('d/m/Y', strtotime($jogo->data_jogo))}} </p>
         Time 
         <p> {{$jogo->time_casa_nome->nome_time}} X {{$jogo->time_adversario_nome->nome_time}} </p>
         
         <p> 
         <h2 style="text-align:center;"> Jogadores </h2>
         </p>
         <table class="table table-hover" cellpadding="10" border="1px">
            <thead>
               <tr>
                  <th style="text-align: center">Nº</th>
                  <th style="text-align: center">Nome do Jogador</th>
                  <th style="text-align: center">RG</th>
                  <th style="text-align: center">Ausente</th>
               </tr>
            </thead>
            <tbody>
               @foreach($jogo->jogadores as $jogador)
               <tr>
                  <td style="text-align: center"> {{$jogador->id}} </td>
                  <td style="text-align: center"> {{$jogador->nome}} </td>
                  <td style="text-align: center"> {{$jogador->rg}} </td>
                  @if($jogador->pivot->ausente == 1)
                  <td style="text-align: center"> Sim </td>
                  @else
                  <td style="text-align: center"> Não </td>
                  @endif
                  </td>
                  @endforeach
                  <p> 
                  <h2 style="text-align:center;"> Movimento de Cartões </h2>
                  </p>
               </tr>
            </tbody>
         </table>
         <table class="table table-hover" cellpadding="10" border="1px">
            <thead>
               <tr>
                  <th style="text-align: center">Nome do Jogador</th>
                  <th style="text-align: center">Cartões Amarelos</th>
                  <th style="text-align: center">Cartões Vermelhos</th>
               </tr>
            </thead>
            <tbody>
               @foreach($jogo->jogadores as $jogador)
               <tr>
                  <td style="text-align: center"> {{$jogador->nome}} </td>
                  <td style="text-align: center"> {{$jogador->pivot->cartao_amarelo}} </td>
                  <td style="text-align: center"> {{$jogador->pivot->cartao_vermelho}} </td>
                  </td>
                  @endforeach
                  <p> 
                  <h2 style="text-align:center;"> Movimento de Placar </h2>
                  </p>
               </tr>
            </tbody>
         </table>
         <table class="table table-hover" cellpadding="10" border="1px">
            <thead>
               <tr>
                  <th style="text-align: center">Nome do Jogador</th>
                  <th style="text-align: center">Gols</th>
               </tr>
            </thead>
            <tbody>
               @foreach($jogo->jogadores as $jogador)
               <tr>
                  <td style="text-align: center"> {{$jogador->nome}} </td>
                  <td style="text-align: center"> {{$jogador->pivot->gols}} </td>
                  </td>
                  @endforeach
            </tbody>
         </table>

<br>
         
         Vencedor
         @if($jogo->placar_time_casa > $jogo->placar_time_adversario)
         {{$jogo->time_casa_nome->nome_time}} 
         @elseif($jogo->placar_time_casa < $jogo->placar_time_adversario)
          {{$jogo->time_adversario_nome->nome_time}} 
         @else 
          EMPATE 
         @endif

<br>

         Placar :
         @if($jogo->placar_time_casa > $jogo->placar_time_adversario)
          {{$jogo->placar_time_casa}} 
         @elseif($jogo->placar_time_casa < $jogo->placar_time_adversario)
          {{$jogo->placar_time_adversario}} 
         @else 
          EMPATE 
         @endif

         @endforeach
      </main>
   </body>
</html>
</div>