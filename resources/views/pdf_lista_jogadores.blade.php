<html>
    <head>

        <style>
            table {
                font-family: arial, sans-serif;
                border-collapse: collapse;
                width: 100%;
            }

            td, th {
                border: 1px solid #dddddd;
                text-align: left;
                padding: 8px;
                opacity: .7
            }

            tr:nth-child(even) {
                background-color: #dddddd;

            }

            #watermark {
                position: fixed;

                /** 
                    Set a position in the page for your image
                    This should center it vertically
                **/
                bottom:   10cm;
                left:     5.5cm;

                /** Change image dimensions**/
                width:    10cm;
                height:   8cm;

                /** Your watermark should be behind every content**/
                z-index:  -1000;

                opacity: .3
            }


        </style>

    </head>

    <h3 align="center">Jogadores</h3>
    <p align="center">SportManager</p>
    <p align="center">Emissão: <?php date_default_timezone_set("America/Sao_Paulo");
echo date('d/m/Y H:i:s') ?></p>


    <br>
    <br>

    <body>

        <div id="watermark">
            <img src="https://i.imgur.com/BNUpuXa.png" height="100%" width="100%" />
        </div>

        <main>


            <div class='col-sm-12'>

                <table class="table table-striped table-bordered nowrap" cellpadding="10">
                    <thead>
                        <tr>

                            <th style="text-align: center">Nome</th>
                            <th style="text-align: center">Sobrenome</th>
                            <th style="text-align: center">Função</th>
                            <th style="text-align: center">Ausencias</th>
                            <th style="text-align: center">Cartões Amarelos</th>
                            <th style="text-align: center">Cartões Vermelhos</th>
                            <th style="text-align: center">Gols</th>
                            <th style="text-align: center">E-Mail</th>


                        </tr>
                    </thead>
                    <tbody>



                        @foreach($jogadores as $jogador)

                        <tr>

                            <td  style="text-align: center">{{$jogador->nome}}</td>
                            <td  style="text-align: center">{{$jogador->sobrenome}}</td>
                            <td  style="text-align: center">{{$jogador->funcao}}</td>
                            <td  style="text-align: center">{{$jogador->ausente}}</td>
                            <td  style="text-align: center">{{$jogador->cartao_amarelo}}</td>
                            <td  style="text-align: center">{{$jogador->cartao_vermelho}}</td>
                            <td  style="text-align: center">{{$jogador->gols}}</td>
                            <td  style="text-align: center">{{$jogador->email}}</td>


                            @endforeach
                        </tr>


                    </tbody>
                </table>

        </main>
    </body>
</html>

</div>
