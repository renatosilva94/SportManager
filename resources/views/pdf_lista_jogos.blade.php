<html>
    <head>

        <style>
            table {
                font-family: arial, sans-serif;
                border-collapse: collapse;
                width: 100%;
            }

            td, th {
                border: 1px solid #dddddd;
                text-align: left;
                padding: 8px;
                opacity: .7

            }

            tr:nth-child(even) {
                background-color: #dddddd;
            }

            #watermark {
                position: fixed;

                /** 
                    Set a position in the page for your image
                    This should center it vertically
                **/
                bottom:   10cm;
                left:     5.5cm;

                /** Change image dimensions**/
                width:    10cm;
                height:   8cm;

                /** Your watermark should be behind every content**/
                z-index:  -1000;
                opacity: .3

            }


        </style>

    </head>

    <h3 align="center">Jogos</h3>
    <p align="center">SportManager</p>
    <p align="center">Emissão: <?php date_default_timezone_set("America/Sao_Paulo");
echo date('d/m/Y H:i:s') ?></p>


    <br>
    <br>

    <body>

        <div id="watermark">
            <img src="https://i.imgur.com/BNUpuXa.png" height="100%" width="100%" />
        </div>

        <main>



            <div class='col-sm-12'>

                <table class="table table-hover" cellpadding="10">
                    <thead>
                        <tr>

                            <th style="text-align: center">Data do Jogo</th>
                            <th style="text-align: center">Time da Casa</th>
                            <th style="text-align: center">Placar do Time da Casa</th>
                            <th style="text-align: center">VS</th>
                            <th style="text-align: center">Placar do Time Adversário</th>
                            <th style="text-align: center">Time Adversário</th>

                            <th style="text-align: center">Liga</th>

                        </tr>
                    </thead>
                    <tbody>



                        @foreach($jogos as $jogo)

                        <tr>

                            <td style="text-align: center">{{date('d/m/Y', strtotime($jogo->data_jogo))}}</td>
                            <td style="text-align: center">{{$jogo->time_casa_nome->nome_time}}</td>
                            <td style="text-align: center">{{$jogo->placar_time_casa}}</td>
                            <th style="text-align: center">X</th>
                            <td style="text-align: center">{{$jogo->placar_time_adversario}}</td>
                            <td style="text-align: center">{{$jogo->time_adversario_nome->nome_time}}</td>
                            <td style="text-align: center; color: #006400"><b>{{$jogo->ligas->nome_liga}}</b></td>

                            @endforeach
                        </tr>


                    </tbody>
                </table>


            </div>
