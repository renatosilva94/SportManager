<html>
    <head>

        <style>



            table {
                font-family: arial, sans-serif;
                border-collapse: collapse;
                width: 100%;
                table-layout: fixed;
            }

            td, th {
                border: 1px solid #dddddd;
                text-align: left;
                padding: 8px;
                opacity: .7

            }

            tr:nth-child(even) {
                background-color: #dddddd;
            }

            #watermark {
                position: fixed;

                /** 
                    Set a position in the page for your image
                    This should center it vertically
                **/
                bottom:   10cm;
                left:     5.5cm;

                /** Change image dimensions**/
                width:    10cm;
                height:   8cm;

                /** Your watermark should be behind every content**/
                z-index:  -1000;

                opacity: .3

            }


        </style>

    </head>

    <h3 align="center">Placares ({{$nome_liga}})</h3>
    <p align="center">SportManager</p>
    <p align="center">Emissão: <?php date_default_timezone_set("America/Sao_Paulo");
echo date('d/m/Y H:i:s') ?></p>


    <br>
    <br>

    <body>

        <div id="watermark">
            <img src="https://i.imgur.com/BNUpuXa.png" height="100%" width="100%" />
        </div>

        <main>



            <div class='col-sm-12'>

                <table class="table table-hover" cellpadding="10">
                    <thead>
                        <tr>
                            <th style="text-align: center">Clas</th>
                            <th style="text-align: center">Nome do Time</th>
                            <th style="text-align: center">Pontos</th>
                            <th style="text-align: center">Jogos</th>
                            <th style="text-align: center">Vitórias</th>
                            <th style="text-align: center">Empates</th>
                            <th style="text-align: center">Derrotas</th>
                            <th style="text-align: center">Gols Pró</th>
                            <th style="text-align: center">Gols Contra</th>
                            <th style="text-align: center">Saldo de Gols</th>




                        </tr>
                    </thead>
                    <tbody>


<?php $i = 0 ?>

                        @foreach($placares as $placar)
<?php $i++ ?>

                        <tr>
                            <td style="text-align: center">{{$i}}</td>
                            <td style="text-align: center">{{$placar->times->nome_time}}</td>
                            <td style="text-align: center">{{$placar->pontos}}</td>
                            <td style="text-align: center">{{$placar->jogos}}</td>
                            <td style="text-align: center">{{$placar->vitorias}}</td>
                            <td style="text-align: center">{{$placar->empates}}</td>
                            <td style="text-align: center">{{$placar->derrotas}}</td>
                            <td style="text-align: center">{{$placar->gols_pro}}</td>
                            <td style="text-align: center">{{$placar->gols_contra}}</td>
                            <td style="text-align: center">{{$placar->gols_pro - $placar->gols_contra}}</td>
                            @endforeach
                        </tr>


                    </tbody>
                    </main>
                    </body>
                    </html>



            </div>


