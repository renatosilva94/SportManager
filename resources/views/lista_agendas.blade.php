@extends('dashboard')


@section('conteudo')


<div class="modal fade" id="modalNovaAgenda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel"><b>Novo Compromisso</b></h3>
            </div>
            <div class="modal-body">
                <form method="post" action="{{route('salvar.agenda')}}">

                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Nome do Evento:</label>
                        <input type="text" class="form-control" id="nome_evento" name="nome_evento" required>
                    </div>

                    <div class="form-group">
                        <label for="data_inicial">Data Inicial:</label>
                        <input type="datetime-local" class="form-control" id="data_inicial"
                               name="data_inicial"
                               required value='<?php echo (new \DateTime())->format('Y-m-d'); ?>'>
                    </div>

                    <div class="form-group">
                        <label for="data_final">Data Final:</label>
                        <input type="datetime-local" class="form-control" id="data_final"
                               name="data_final"
                               required value='<?php echo (new \DateTime())->format('Y-m-d'); ?>'>
                    </div>

                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Mensagem:</label>
                        <input type="textarea" class="form-control" id="mensagem" name="mensagem" required>
                    </div>




            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-dark" data-dismiss="modal">Fechar</button>
                <button type="submit" class="btn btn-primary">Salvar Compromisso</button>

                </form>

            </div>
        </div>
    </div>
</div>



<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="dashboard_graph">

            <div class="row x_title">
                <div class="col-md-6">
                    <h3>Agenda <small> Listagem </small></h3>
                </div>


                <br>
                <br>


                <div class="col-md-12">






                    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modalNovaAgenda">Novo Compromisso <span class="glyphicon glyphicon-plus" style="color:white"></span> </button>

                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modalExcluirCompromisso">Excluir Compromisso <span class="glyphicon glyphicon-minus" style="color:white"></span> </button>








                </div>


















                <div id="modalCalendario" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">close</span></button>
                                <h4 id="nomeEvento" class="modal-title"></h4>
                            </div>
                            <div id="descricaoEvento" class="modal-body"> 

                                <h5 id="dataInicial"></h5>
                                <h5 id="dataFinal"></h5>


                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-dark" data-dismiss="modal">Fechar</button>
                            </div>
                        </div>
                    </div>
                </div>






                <div class="col-md-10 col-sm-10 col-xs-10">

                    {!! $detalhes_calendario->calendar() !!}

                    {!! $detalhes_calendario->script() !!}





                </div>








                <!-- Excluir Compromisso -->


                <div class="modal fade" id="modalExcluirCompromisso" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h3 class="modal-title" id="exampleModalLabel"><b>Excluir Compromisso</b></h3>
                            </div>
                            <div class="modal-body">

                                @foreach ($agendas as $agenda) 


                                <ul>
                                    <li>{{$agenda->nome_evento}}</li>
                                    <li>Data Inicial: {{\Carbon\Carbon::parse($agenda->data_inicial)->format('d/m/Y H:i')}}</li>
                                    <li>Data Final: {{\Carbon\Carbon::parse($agenda->data_final)->format('d/m/Y H:i')}}</li>
                                    <li>



                                        <form method="post"
                                              action="{{route('deletar.compromisso', $agenda->id)}}"
                                              onsubmit="return confirm('Confirma Exclusão do Compromisso?')">
                                            {{method_field('delete')}}
                                            {{csrf_field()}}


                                            <input type="submit" class="btn btn-danger btn-xs" value="Deletar">


                                        </form>






                                    </li>


                                </ul>


                                @endforeach 


                            </div>

                                        <div class="modal-footer">
                <button type="button" class="btn btn-dark" data-dismiss="modal">Fechar</button>

                        </div>
                    </div>
                    
                </div>


                <!-- Fim da Modal Excluir Compromisso --> 





                @endsection
