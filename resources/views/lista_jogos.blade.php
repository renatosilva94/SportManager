@extends('dashboard')

@section('conteudo')














































<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="dashboard_graph">

            <div class="row x_title">
                <div class="col-md-6">
                    <h3>Jogos <small> Listagem </small></h3>
                </div>


                <br>
                <br>


                <div class="col-md-12">










                    <div class='col-sm-12'>



                        @if (count($times)==0)

                        @elseif(count($ligas)==0)

                        @else
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalNovoJogo">Novo Jogo <span class="glyphicon glyphicon-plus" style="color:white"></span> </button>
                        @endif


                        <a href="{{route('pdf.lista.jogos')}}" class="btn btn-info">Gerar Pdf <span class="glyphicon glyphicon-file" style="color:white"></span></a>





                    </div>

                    </form>
















                </div>







            </div>
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12">



            @if (count($jogos)==0)
            <div class="alert alert-danger">
                Não existem jogos com os filtros informados.
            </div>
            @endif

            @if (count($times)==0)
            <div class="alert alert-warning">
                Não há times cadastrados, antes de usar o cadastro de jogos você deve <b><a href="{{route('lista.times')}}">cadastrar times</a></b>...
            </div>
            @endif



            @if (count($ligas)==0)
            <div class="alert alert-success">
                Não há ligas cadastradas, antes de usar o cadastro de jogos você deve <b><a href="{{route('lista.ligas')}}">cadastrar ligas</a></b>...
            </div>
            @endif



            <table class="table table-striped table-bordered nowrap">
                <thead>
                    <tr>
                        <th style="text-align: center">Data do Jogo</th>


                        <th style="text-align: center">Time da Casa</th>
                        <th style="text-align: center">Placar do Time da Casa</th>
                        <th style="text-align: center">VS</th>
                        <th style="text-align: center">Placar do Time Adversário</th>
                        <th style="text-align: center">Time Adversário</th>

                        <th style="text-align: center">Liga</th>




                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>


                    <!-- Modal Novo Jogo Beta -->

                <script>
                    $(document).ready(function () {

                        $('.next').click(function () {

                            var nextId = $(this).parents('.tab-pane').next().attr("id");
                            $('[href=#' + nextId + ']').tab('show');
                            return false;

                        })

                        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {

                            //update progress
                            var step = $(e.target).data('step');
                            var percent = (parseInt(step) / 2) * 100;

                            $('.progress-bar').css({width: percent + '%'});
                            $('.progress-bar').text("Etapa " + step + " de 2");

                            //e.relatedTarget // previous tab

                        })

                        $('.first').click(function () {

                            $('#myWizard a:first').tab('show')

                        })

                    });
                </script>


                <div id="modalNovoJogo" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h3 id="myModalLabel">Novo Jogo</h3>
                            </div>
                            <div class="modal-body" id="myWizard">

                                <div class="progress">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="4" style="width: 50%;">
                                        Etapa 1 de 2
                                    </div>
                                </div>

                                <div class="navbar">
                                    <div class="navbar-inner">
                                        <ul class="nav nav-pills">
                                            <li class="active"><a href="#step1" data-toggle="tab" data-step="1">Etapa 1</a></li>
                                            <li><a href="#step2" data-toggle="tab" data-step="2">Etapa 2</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="step1">

                                        <div class="well">

                                            <!-- Informações do Jogo -->
                                            <form method="post" action="{{route('salvar.jogo')}}">

                                                {{ csrf_field() }}

                                                <div class="form-group">
                                                    <label for="data_jogo">Data do Jogo:</label>
                                                    <input type="date" class="form-control" id="data_jogo"
                                                           name="data_jogo"
                                                           required value='<?php echo (new \DateTime())->format('Y-m-d'); ?>'>
                                                </div>

                                                <div class="form-group">
                                                    <label for="liga_id">Nome da Liga Que o Jogo Se Enquadra:</label>
                                                    <select class="form-control" id="liga_id" name="liga_id">
                                                        <option></option>
                                                        @foreach($ligas as $liga)
                                                        <option value="{{$liga->id}}" name="nome_liga">{{$liga->nome_liga}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>



                                                <div class="form-group">
                                                    <label for="time_casa_id">Time da Casa:</label>
                                                    <select class="form-control" id="time_id" name="time_casa_id" disabled="disabled">
                                                        <option></option>
                                                        @foreach($times as $time)
                                                        <option value="{{$time->id}}" name="time_casa_id">{{$time->nome_time}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>


                                                <div class="form-group">
                                                    <label for="placar_time_casa">Placar do Time da Casa:</label>
                                                    <input type="number" class="form-control" id="placar_time_casa"
                                                           name="placar_time_casa"
                                                           value="{{$jogo->placar_time_casa or old('placar_time_casa')}}" min="0"
                                                           required>
                                                </div>

                                                <div class="form-group">
                                                    <label for="time_adversario_id">Time Adversário:</label>
                                                    <select class="form-control" id="time_id_2" name="time_adversario_id" disabled="disabled">
                                                        <option></option>
                                                        @foreach($times as $time)
                                                        <option value="{{$time->id}}" name="time_adversario_id">{{$time->nome_time}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <label for="placar_time_adversario">Placar do Time Adversário:</label>
                                                    <input type="number" class="form-control" id="placar_time_adversario" min="0"
                                                           name="placar_time_adversario"
                                                           value="{{$jogo->placar_time_adversario or old('placar_time_adversario')}}"
                                                           required>
                                                </div>
























                                                <script>




                                                    /*
                                                     $(function() {
                                                     $('#time_id').change(function() {
                                                     dropdownval = $(this).val();
                                                     $('#time_id_2').not(this).find('option[value="' + dropdownval + '"]').hide();
                                                     });
                                                     });
                                                     */






                                                    $('#liga_id').on('change', function () {
                                                        var ligaID = $(this).val();
                                                        if (ligaID) {
                                                            $.ajax({
                                                                type: "GET",
                                                                url: "{{url('ajax/pegar-times-liga')}}?liga_id=" + ligaID,
                                                                success: function (res) {
                                                                    if (res) {

                                                                        $("#time_id").empty();

                                                                        $("#time_id").append('<option></option>');

                                                                        $.each(res, function (key, value) {



                                                                            $("#time_id").append('<option value="' + key + '">' + value + '</option>');

                                                                            $("#time_id").removeAttr("disabled");

                                                                        });

                                                                    } else {
                                                                        $("#time_id").empty();
                                                                    }
                                                                }
                                                            });
                                                        } else {
                                                            $("#time_id").empty();
                                                        }

                                                    });




                                                    $('#liga_id').on('change', function () {
                                                        var ligaID = $(this).val();
                                                        if (ligaID) {
                                                            $.ajax({
                                                                type: "GET",
                                                                url: "{{url('ajax/pegar-times-liga')}}?liga_id=" + ligaID,
                                                                success: function (res) {
                                                                    if (res) {
                                                                        $("#time_id_2").empty();

                                                                        $("#time_id_2").append('<option></option>');

                                                                        $.each(res, function (key, value) {
                                                                            $("#time_id_2").append('<option value="' + key + '">' + value + '</option>');

                                                                            $("#time_id_2").removeAttr("disabled");


                                                                        });

                                                                    } else {
                                                                        $("#time_id_2").empty();
                                                                    }
                                                                }
                                                            });
                                                        } else {
                                                            $("#time_id_2").empty();
                                                        }

                                                    });










                                                    $('#time_id').on('change', function () {
                                                        var timeID = $(this).val();
                                                        if (timeID) {
                                                            $.ajax({
                                                                type: "GET",
                                                                url: "{{url('ajax/pegar-jogadores-time')}}?time_casa_id=" + timeID,
                                                                success: function (res) {
                                                                    if (res) {
                                                                        $("#jogador_id").empty();

                                                                        $("#jogador_id2").empty();

                                                                        
                                                                        $("#jogador_id3").empty();


                                                                        $("#jogador_id4").empty();

                                                                                                                                                $("#jogador_id5").empty();

                                                                                                                                                                                                                                                                                                $("#jogador_id6").empty();

                                                                                                                                                $("#jogador_id7").empty();

                                                                                                                                                $("#jogador_id8").empty();

                                                                                                                                                $("#jogador_id9").empty();

                                                                                                                                                $("#jogador_id10").empty();

                                                                                                                                                $("#jogador_id11").empty();


                                                                        $("#jogador_id").append('<option value="0" ></option>');

$("#jogador_id2").append('<option value="0" ></option>');

$("#jogador_id3").append('<option value="0" ></option>');

$("#jogador_id4").append('<option value="0" ></option>');

$("#jogador_id5").append('<option value="0" ></option>');

$("#jogador_id6").append('<option value="0" ></option>');
$("#jogador_id7").append('<option value="0" ></option>');
$("#jogador_id8").append('<option value="0" ></option>');
$("#jogador_id9").append('<option value="0" ></option>');
$("#jogador_id10").append('<option value="0" ></option>');
$("#jogador_id11").append('<option value="0" ></option>');


                                                                        $.each(res, function (key, value) {


                                                                            $("#jogador_id").append('<option value="' + key + '">' + value + '</option>');

                                                                            $("#jogador_id2").append('<option value="' + key + '">' + value + '</option>');

                                                                                                                                                        $("#jogador_id3").append('<option value="' + key + '">' + value + '</option>');

                                                                            $("#jogador_id4").append('<option value="' + key + '">' + value + '</option>');

                                                                            $("#jogador_id5").append('<option value="' + key + '">' + value + '</option>');

                                                                                                                                                        $("#jogador_id6").append('<option value="' + key + '">' + value + '</option>');

                                                                                                                                                                                                                                    $("#jogador_id7").append('<option value="' + key + '">' + value + '</option>');

                                                                                                                                                                                                                                                                                                                $("#jogador_id8").append('<option value="' + key + '">' + value + '</option>');

                                                                                                                                                                                                                                                                                                                                                                                            $("#jogador_id9").append('<option value="' + key + '">' + value + '</option>');

                                                                            $("#jogador_id10").append('<option value="' + key + '">' + value + '</option>');

                                                                            $("#jogador_id11").append('<option value="' + key + '">' + value + '</option>');

                                                                            $("#jogador_id").removeAttr("disabled");

                                                                            $("#jogador_id2").removeAttr("disabled");

                                                                            $("#jogador_id3").removeAttr("disabled");

                                                                            $("#jogador_id4").removeAttr("disabled");

                                                                            $("#jogador_id5").removeAttr("disabled");

                                                                            $("#jogador_id6").removeAttr("disabled");

                                                                            $("#jogador_id7").removeAttr("disabled");

                                                                            $("#jogador_id8").removeAttr("disabled");

                                                                            $("#jogador_id9").removeAttr("disabled");

                                                                            $("#jogador_id10").removeAttr("disabled");

                                                                            $("#jogador_id11").removeAttr("disabled");


                                                                        });

                                                                    } else {
                                                                        $("#jogador_id").empty();
                                                                    }
                                                                }
                                                            });
                                                        } else {
                                                            $("#jogador_id").empty();
                                                        }

                                                    });




                                                    $('#time_id_2').on('change', function () {
                                                        var timeID = $(this).val();
                                                        if (timeID) {
                                                            $.ajax({
                                                                type: "GET",
                                                                url: "{{url('ajax/pegar-jogadores-adversarios-time')}}?time_adversario_id=" + timeID,
                                                                success: function (res) {
                                                                    if (res) {

                                                                        $("#jogador_id_1").empty();
                                                                        $("#jogador_id_2").empty();
                                                                        $("#jogador_id_3").empty();
                                                                        $("#jogador_id_4").empty();
                                                                        $("#jogador_id_5").empty();
                                                                        $("#jogador_id_6").empty();
                                                                        $("#jogador_id_7").empty();
                                                                        $("#jogador_id_8").empty();
                                                                        $("#jogador_id_9").empty();
                                                                        $("#jogador_id_10").empty();
                                                                        $("#jogador_id_11").empty();


                                                                        $("#jogador_id_1").append('<option value="0"></option>');

                                                                        
                                                                        $("#jogador_id_2").append('<option value="0"></option>');
                                                                        
                                                                        $("#jogador_id_3").append('<option value="0"></option>');
                                                                        
                                                                        $("#jogador_id_4").append('<option value="0"></option>');
                                                                        
                                                                        $("#jogador_id_5").append('<option value="0"></option>');
                                                                        
                                                                        $("#jogador_id_6").append('<option value="0"></option>');
                                                                        
                                                                        $("#jogador_id_7").append('<option value="0"></option>');
                                                                        
                                                                        $("#jogador_id_8").append('<option value="0"></option>');
                                                                        
                                                                        $("#jogador_id_9").append('<option value="0"></option>');
                                                                        
                                                                        $("#jogador_id_10").append('<option value="0"></option>');
                                                                        
                                                                        $("#jogador_id_11").append('<option value="0"></option>');

                                                                        $.each(res, function (key, value) {



                                                                            $("#jogador_id_1").append('<option value="' + key + '">' + value + '</option>');


                                                                            $("#jogador_id_2").append('<option value="' + key + '">' + value + '</option>');


                                                                            $("#jogador_id_3").append('<option value="' + key + '">' + value + '</option>');

                                                                            
                                                                            $("#jogador_id_4").append('<option value="' + key + '">' + value + '</option>');

                                                                            
                                                                            $("#jogador_id_5").append('<option value="' + key + '">' + value + '</option>');

                                                                            
                                                                            $("#jogador_id_6").append('<option value="' + key + '">' + value + '</option>');

                                                                            
                                                                            $("#jogador_id_7").append('<option value="' + key + '">' + value + '</option>');

                                                                            
                                                                            $("#jogador_id_8").append('<option value="' + key + '">' + value + '</option>');

                                                                            
                                                                            $("#jogador_id_9").append('<option value="' + key + '">' + value + '</option>');

                                                                            
                                                                            $("#jogador_id_10").append('<option value="' + key + '">' + value + '</option>');

                                                                            
                                                                            $("#jogador_id_11").append('<option value="' + key + '">' + value + '</option>');

                                                                            $("#jogador_id_1").removeAttr("disabled");

                                                                            $("#jogador_id_2").removeAttr("disabled");

                                                                            $("#jogador_id_3").removeAttr("disabled");

                                                                            $("#jogador_id_4").removeAttr("disabled");

                                                                                                                                                        $("#jogador_id_5").removeAttr("disabled");

                                                                                                                                                                                                                                    $("#jogador_id_6").removeAttr("disabled");

                                                                            $("#jogador_id_7").removeAttr("disabled");

                                                                                                                                                        $("#jogador_id_8").removeAttr("disabled");

                                                                                                                                                                                                                                    $("#jogador_id_9").removeAttr("disabled");
                                                                        });

                                                                            $("#jogador_id_10").removeAttr("disabled");

                                                                                                                                                        $("#jogador_id_11").removeAttr("disabled");
                                                                    } else {
                                                                        $("#jogador_id_2").empty();
                                                                    }
                                                                }
                                                            });
                                                        } else {
                                                            $("#jogador_id_2").empty();
                                                        }

                                                    });







                                                </script>





                                                <!-- Fim das Informações -->





                                        </div>


                                    </div>

                                    <div class="tab-pane fade" id="step2">

                                        <div class="">


















                                            <!-- Jogadores do Time da Casa -->

                                            <div id="sections">

                                                <div class="section">



<!-- Modelo de Formulário Time Casa --> 

                                                    <div class="form-group col-sm-12" id="formulariocasa">
                                                        <label for="jogador_id">Jogadores do Time da Casa:</label>

                                                        <select class="form-control" id="jogador_id" name="jogadorescasa[1][jogador_id]" disabled="disabled">
                                                            <option></option>
                                                            @foreach($jogadores as $jogador)
                                                            <option value="{{$jogador->id}}" >{{$jogador->nome}}</option>
                                                            @endforeach


                                                        </select>

                                                        



                                                        <div class="form-check col-sm-12">
                                                            <input type="checkbox" class="form-check-input" id="ausente" value="0">
                                                            <label class="form-check-label" for="ausente">Jogador Ausente ? </label>
                                                        </div>

                                                        
                                                        <div class="form-group col-sm-3">
                                                            <label for="gols">Gols:</label>
                                                            <input type="number" class="form-control" id="gols"
                                                                   name="jogadorescasa[1][gols]" min="0"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_amarelo">Cartões Amarelos:</label>
                                                            <input type="number" class="form-control" id="cartao_amarelo"
                                                                   name="jogadorescasa[1][cartao_amarelo]" min="0" max="2"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_vermelho">Cartões Vermelhos:</label>
                                                            <input type="number" class="form-control" id="cartao_vermelho"
                                                                   name="jogadorescasa[1][cartao_vermelho]" min="0" max="1"
                                                                   value="0">

                                                                   <input type="hidden" name="jogadorescasa[1][ausente]" id="ausenteId">

                                                        </div>




<script>
    $('#ausente').on('change', function () {
        this.value = this.checked ? 1 : 0;
        $('#ausenteId').val(this.value);
    }).change();
</script>

</div>
<!-- Fim do Modelo --> 

<!-- Modelo de Formulário Time Casa --> 

                                                    <div class="form-group col-sm-12" id="formulariocasa">
                                                        <label for="jogador_id">Jogadores do Time da Casa:</label>

                                                        <select class="form-control" id="jogador_id2" name="jogadorescasa[2][jogador_id]" disabled="disabled">
                                                            <option></option>
                                                            @foreach($jogadores as $jogador)
                                                            <option value="{{$jogador->id}}" >{{$jogador->nome}}</option>
                                                            @endforeach


                                                        </select>

                                                        



                                                        <div class="form-check col-sm-12">
                                                            <input type="checkbox" class="form-check-input" id="ausente2" value="0">
                                                            <label class="form-check-label" for="ausente">Jogador Ausente ? </label>
                                                        </div>

                                                        
                                                        <div class="form-group col-sm-3">
                                                            <label for="gols">Gols:</label>
                                                            <input type="number" class="form-control" id="gols"
                                                                   name="jogadorescasa[2][gols]" min="0"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_amarelo">Cartões Amarelos:</label>
                                                            <input type="number" class="form-control" id="cartao_amarelo"
                                                                   name="jogadorescasa[2][cartao_amarelo]" min="0" max="2"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_vermelho">Cartões Vermelhos:</label>
                                                            <input type="number" class="form-control" id="cartao_vermelho"
                                                                   name="jogadorescasa[2][cartao_vermelho]" min="0" max="1"
                                                                   value="0">

                                                                   <input type="hidden" name="jogadorescasa[2][ausente]" id="ausenteId2">

                                                        </div>




<script>
    $('#ausente2').on('change', function () {
        this.value = this.checked ? 1 : 0;
        $('#ausenteId2').val(this.value);
    }).change();
</script>

</div>
<!-- Fim do Modelo --> 

<!-- Modelo de Formulário Time Casa --> 

                                                    <div class="form-group col-sm-12" id="formulariocasa">
                                                        <label for="jogador_id">Jogadores do Time da Casa:</label>

                                                        <select class="form-control" id="jogador_id3" name="jogadorescasa[3][jogador_id]" disabled="disabled">
                                                            <option></option>
                                                            @foreach($jogadores as $jogador)
                                                            <option value="{{$jogador->id}}" >{{$jogador->nome}}</option>
                                                            @endforeach


                                                        </select>

                                                        



                                                        <div class="form-check col-sm-12">
                                                            <input type="checkbox" class="form-check-input" id="ausente3" value="0">
                                                            <label class="form-check-label" for="ausente">Jogador Ausente ? </label>
                                                        </div>

                                                        
                                                        <div class="form-group col-sm-3">
                                                            <label for="gols">Gols:</label>
                                                            <input type="number" class="form-control" id="gols"
                                                                   name="jogadorescasa[3][gols]" min="0"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_amarelo">Cartões Amarelos:</label>
                                                            <input type="number" class="form-control" id="cartao_amarelo"
                                                                   name="jogadorescasa[3][cartao_amarelo]" min="0" max="2"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_vermelho">Cartões Vermelhos:</label>
                                                            <input type="number" class="form-control" id="cartao_vermelho"
                                                                   name="jogadorescasa[3][cartao_vermelho]" min="0" max="1"
                                                                   value="0">

                                                                   <input type="hidden" name="jogadorescasa[3][ausente]" id="ausenteId3">

                                                        </div>




<script>
    $('#ausente3').on('change', function () {
        this.value = this.checked ? 1 : 0;
        $('#ausenteId3').val(this.value);
    }).change();
</script>

</div>
<!-- Fim do Modelo --> 

<!-- Modelo de Formulário Time Casa --> 

                                                    <div class="form-group col-sm-12" id="formulariocasa">
                                                        <label for="jogador_id">Jogadores do Time da Casa:</label>

                                                        <select class="form-control" id="jogador_id4" name="jogadorescasa[4][jogador_id]" disabled="disabled">
                                                            <option></option>
                                                            @foreach($jogadores as $jogador)
                                                            <option value="{{$jogador->id}}" >{{$jogador->nome}}</option>
                                                            @endforeach


                                                        </select>

                                                        



                                                        <div class="form-check col-sm-12">
                                                            <input type="checkbox" class="form-check-input" id="ausente4" value="0">
                                                            <label class="form-check-label" for="ausente">Jogador Ausente ? </label>
                                                        </div>

                                                        
                                                        <div class="form-group col-sm-3">
                                                            <label for="gols">Gols:</label>
                                                            <input type="number" class="form-control" id="gols"
                                                                   name="jogadorescasa[4][gols]" min="0"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_amarelo">Cartões Amarelos:</label>
                                                            <input type="number" class="form-control" id="cartao_amarelo"
                                                                   name="jogadorescasa[4][cartao_amarelo]" min="0" max="2"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_vermelho">Cartões Vermelhos:</label>
                                                            <input type="number" class="form-control" id="cartao_vermelho"
                                                                   name="jogadorescasa[4][cartao_vermelho]" min="0" max="1"
                                                                   value="0">

                                                                   <input type="hidden" name="jogadorescasa[4][ausente]" id="ausenteId4">

                                                        </div>




<script>
    $('#ausente4').on('change', function () {
        this.value = this.checked ? 1 : 0;
        $('#ausenteId4').val(this.value);
    }).change();
</script>

</div>
<!-- Fim do Modelo --> 


<!-- Modelo de Formulário Time Casa --> 

                                                    <div class="form-group col-sm-12" id="formulariocasa">
                                                        <label for="jogador_id">Jogadores do Time da Casa:</label>

                                                        <select class="form-control" id="jogador_id5" name="jogadorescasa[5][jogador_id]" disabled="disabled">
                                                            <option></option>
                                                            @foreach($jogadores as $jogador)
                                                            <option value="{{$jogador->id}}" >{{$jogador->nome}}</option>
                                                            @endforeach


                                                        </select>

                                                        



                                                        <div class="form-check col-sm-12">
                                                            <input type="checkbox" class="form-check-input" id="ausente5" value="0">
                                                            <label class="form-check-label" for="ausente">Jogador Ausente ? </label>
                                                        </div>

                                                        
                                                        <div class="form-group col-sm-3">
                                                            <label for="gols">Gols:</label>
                                                            <input type="number" class="form-control" id="gols"
                                                                   name="jogadorescasa[5][gols]" min="0"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_amarelo">Cartões Amarelos:</label>
                                                            <input type="number" class="form-control" id="cartao_amarelo"
                                                                   name="jogadorescasa[5][cartao_amarelo]" min="0" max="2"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_vermelho">Cartões Vermelhos:</label>
                                                            <input type="number" class="form-control" id="cartao_vermelho"
                                                                   name="jogadorescasa[5][cartao_vermelho]" min="0" max="1"
                                                                   value="0">

                                                                   <input type="hidden" name="jogadorescasa[5][ausente]" id="ausenteId5">

                                                        </div>




<script>
    $('#ausente5').on('change', function () {
        this.value = this.checked ? 1 : 0;
        $('#ausenteId5').val(this.value);
    }).change();
</script>

</div>
<!-- Fim do Modelo --> 




<!-- Modelo de Formulário Time Casa --> 

                                                    <div class="form-group col-sm-12" id="formulariocasa">
                                                        <label for="jogador_id">Jogadores do Time da Casa:</label>

                                                        <select class="form-control" id="jogador_id6" name="jogadorescasa[6][jogador_id]" disabled="disabled">
                                                            <option></option>
                                                            @foreach($jogadores as $jogador)
                                                            <option value="{{$jogador->id}}" >{{$jogador->nome}}</option>
                                                            @endforeach


                                                        </select>

                                                        



                                                        <div class="form-check col-sm-12">
                                                            <input type="checkbox" class="form-check-input" id="ausente6" value="0">
                                                            <label class="form-check-label" for="ausente">Jogador Ausente ? </label>
                                                        </div>

                                                        
                                                        <div class="form-group col-sm-3">
                                                            <label for="gols">Gols:</label>
                                                            <input type="number" class="form-control" id="gols"
                                                                   name="jogadorescasa[6][gols]" min="0"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_amarelo">Cartões Amarelos:</label>
                                                            <input type="number" class="form-control" id="cartao_amarelo"
                                                                   name="jogadorescasa[6][cartao_amarelo]" min="0" max="2"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_vermelho">Cartões Vermelhos:</label>
                                                            <input type="number" class="form-control" id="cartao_vermelho"
                                                                   name="jogadorescasa[6][cartao_vermelho]" min="0" max="1"
                                                                   value="0">

                                                                   <input type="hidden" name="jogadorescasa[6][ausente]" id="ausenteId6">

                                                        </div>




<script>
    $('#ausente6').on('change', function () {
        this.value = this.checked ? 1 : 0;
        $('#ausenteId6').val(this.value);
    }).change();
</script>

</div>
<!-- Fim do Modelo --> 


<!-- Modelo de Formulário Time Casa --> 

                                                    <div class="form-group col-sm-12" id="formulariocasa">
                                                        <label for="jogador_id">Jogadores do Time da Casa:</label>

                                                        <select class="form-control" id="jogador_id7" name="jogadorescasa[7][jogador_id]" disabled="disabled">
                                                            <option></option>
                                                            @foreach($jogadores as $jogador)
                                                            <option value="{{$jogador->id}}" >{{$jogador->nome}}</option>
                                                            @endforeach


                                                        </select>

                                                        



                                                        <div class="form-check col-sm-12">
                                                            <input type="checkbox" class="form-check-input" id="ausente7" value="0">
                                                            <label class="form-check-label" for="ausente">Jogador Ausente ? </label>
                                                        </div>

                                                        
                                                        <div class="form-group col-sm-3">
                                                            <label for="gols">Gols:</label>
                                                            <input type="number" class="form-control" id="gols"
                                                                   name="jogadorescasa[7][gols]" min="0"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_amarelo">Cartões Amarelos:</label>
                                                            <input type="number" class="form-control" id="cartao_amarelo"
                                                                   name="jogadorescasa[7][cartao_amarelo]" min="0" max="2"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_vermelho">Cartões Vermelhos:</label>
                                                            <input type="number" class="form-control" id="cartao_vermelho"
                                                                   name="jogadorescasa[7][cartao_vermelho]" min="0" max="1"
                                                                   value="0">

                                                                   <input type="hidden" name="jogadorescasa[7][ausente]" id="ausenteId7">

                                                        </div>




<script>
    $('#ausente7').on('change', function () {
        this.value = this.checked ? 1 : 0;
        $('#ausenteId7').val(this.value);
    }).change();
</script>

</div>
<!-- Fim do Modelo --> 


<!-- Modelo de Formulário Time Casa --> 

                                                    <div class="form-group col-sm-12" id="formulariocasa">
                                                        <label for="jogador_id">Jogadores do Time da Casa:</label>

                                                        <select class="form-control" id="jogador_id8" name="jogadorescasa[8][jogador_id]" disabled="disabled">
                                                            <option></option>
                                                            @foreach($jogadores as $jogador)
                                                            <option value="{{$jogador->id}}" >{{$jogador->nome}}</option>
                                                            @endforeach


                                                        </select>

                                                        



                                                        <div class="form-check col-sm-12">
                                                            <input type="checkbox" class="form-check-input" id="ausente8" value="0">
                                                            <label class="form-check-label" for="ausente">Jogador Ausente ? </label>
                                                        </div>

                                                        
                                                        <div class="form-group col-sm-3">
                                                            <label for="gols">Gols:</label>
                                                            <input type="number" class="form-control" id="gols"
                                                                   name="jogadorescasa[8][gols]" min="0"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_amarelo">Cartões Amarelos:</label>
                                                            <input type="number" class="form-control" id="cartao_amarelo"
                                                                   name="jogadorescasa[8][cartao_amarelo]" min="0" max="2"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_vermelho">Cartões Vermelhos:</label>
                                                            <input type="number" class="form-control" id="cartao_vermelho"
                                                                   name="jogadorescasa[8][cartao_vermelho]" min="0" max="1"
                                                                   value="0">

                                                                   <input type="hidden" name="jogadorescasa[8][ausente]" id="ausenteId8">

                                                        </div>




<script>
    $('#ausente8').on('change', function () {
        this.value = this.checked ? 1 : 0;
        $('#ausenteId8').val(this.value);
    }).change();
</script>

</div>
<!-- Fim do Modelo --> 

<!-- Modelo de Formulário Time Casa --> 

                                                    <div class="form-group col-sm-12" id="formulariocasa">
                                                        <label for="jogador_id">Jogadores do Time da Casa:</label>

                                                        <select class="form-control" id="jogador_id9" name="jogadorescasa[9][jogador_id]" disabled="disabled">
                                                            <option></option>
                                                            @foreach($jogadores as $jogador)
                                                            <option value="{{$jogador->id}}" >{{$jogador->nome}}</option>
                                                            @endforeach


                                                        </select>

                                                        



                                                        <div class="form-check col-sm-12">
                                                            <input type="checkbox" class="form-check-input" id="ausente9" value="0">
                                                            <label class="form-check-label" for="ausente">Jogador Ausente ? </label>
                                                        </div>

                                                        
                                                        <div class="form-group col-sm-3">
                                                            <label for="gols">Gols:</label>
                                                            <input type="number" class="form-control" id="gols"
                                                                   name="jogadorescasa[9][gols]" min="0"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_amarelo">Cartões Amarelos:</label>
                                                            <input type="number" class="form-control" id="cartao_amarelo"
                                                                   name="jogadorescasa[9][cartao_amarelo]" min="0" max="2"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_vermelho">Cartões Vermelhos:</label>
                                                            <input type="number" class="form-control" id="cartao_vermelho"
                                                                   name="jogadorescasa[9][cartao_vermelho]" min="0" max="1"
                                                                   value="0">

                                                                   <input type="hidden" name="jogadorescasa[9][ausente]" id="ausenteId9">

                                                        </div>




<script>
    $('#ausente9').on('change', function () {
        this.value = this.checked ? 1 : 0;
        $('#ausenteId9').val(this.value);
    }).change();
</script>

</div>
<!-- Fim do Modelo --> 

<!-- Modelo de Formulário Time Casa --> 

                                                    <div class="form-group col-sm-12" id="formulariocasa">
                                                        <label for="jogador_id">Jogadores do Time da Casa:</label>

                                                        <select class="form-control" id="jogador_id10" name="jogadorescasa[10][jogador_id]" disabled="disabled">
                                                            <option></option>
                                                            @foreach($jogadores as $jogador)
                                                            <option value="{{$jogador->id}}" >{{$jogador->nome}}</option>
                                                            @endforeach


                                                        </select>

                                                        



                                                        <div class="form-check col-sm-12">
                                                            <input type="checkbox" class="form-check-input" id="ausente10" value="0">
                                                            <label class="form-check-label" for="ausente">Jogador Ausente ? </label>
                                                        </div>

                                                        
                                                        <div class="form-group col-sm-3">
                                                            <label for="gols">Gols:</label>
                                                            <input type="number" class="form-control" id="gols"
                                                                   name="jogadorescasa[10][gols]" min="0"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_amarelo">Cartões Amarelos:</label>
                                                            <input type="number" class="form-control" id="cartao_amarelo"
                                                                   name="jogadorescasa[10][cartao_amarelo]" min="0" max="2"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_vermelho">Cartões Vermelhos:</label>
                                                            <input type="number" class="form-control" id="cartao_vermelho"
                                                                   name="jogadorescasa[10][cartao_vermelho]" min="0" max="1"
                                                                   value="0">

                                                                   <input type="hidden" name="jogadorescasa[10][ausente]" id="ausenteId10">

                                                        </div>




<script>
    $('#ausente10').on('change', function () {
        this.value = this.checked ? 1 : 0;
        $('#ausenteId10').val(this.value);
    }).change();
</script>

</div>
<!-- Fim do Modelo --> 

<!-- Modelo de Formulário Time Casa --> 

                                                    <div class="form-group col-sm-12" id="formulariocasa">
                                                        <label for="jogador_id">Jogadores do Time da Casa:</label>

                                                        <select class="form-control" id="jogador_id11" name="jogadorescasa[11][jogador_id]" disabled="disabled">
                                                            <option></option>
                                                            @foreach($jogadores as $jogador)
                                                            <option value="{{$jogador->id}}" >{{$jogador->nome}}</option>
                                                            @endforeach


                                                        </select>

                                                        



                                                        <div class="form-check col-sm-12">
                                                            <input type="checkbox" class="form-check-input" id="ausente11" value="0">
                                                            <label class="form-check-label" for="ausente">Jogador Ausente ? </label>
                                                        </div>

                                                        
                                                        <div class="form-group col-sm-3">
                                                            <label for="gols">Gols:</label>
                                                            <input type="number" class="form-control" id="gols"
                                                                   name="jogadorescasa[11][gols]" min="0"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_amarelo">Cartões Amarelos:</label>
                                                            <input type="number" class="form-control" id="cartao_amarelo"
                                                                   name="jogadorescasa[11][cartao_amarelo]" min="0" max="2"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_vermelho">Cartões Vermelhos:</label>
                                                            <input type="number" class="form-control" id="cartao_vermelho"
                                                                   name="jogadorescasa[11][cartao_vermelho]" min="0" max="1"
                                                                   value="0">

                                                                   <input type="hidden" name="jogadorescasa[11][ausente]" id="ausenteId11">

                                                        </div>




<script>
    $('#ausente11').on('change', function () {
        this.value = this.checked ? 1 : 0;
        $('#ausenteId11').val(this.value);
    }).change();
</script>

</div>
<!-- Fim do Modelo --> 








<!--

                                                        <div class="col-sm-1">
                                                            <button type="" class="remove btn btn-block btn-danger btn-sm"> <b>-</b> </button>
                                                        </div>

                                                        <div class="col-sm-1">
                                                            <button type="" class="addsection btn btn-block btn-primary btn-sm"> <b>+</b> </button>
                                                        </div>

-->





                                                    </div>















                                                </div>

</div>




                                                <!-- Inicio do Script de Adição de Formulário -->

                                                <script>



                                                    //define template
                                                    var template = $('#sections .section .formulariocasa:first').children().clone();







                                                //define counter
                                                    var sectionsCount = 1;


                                                //add new section
                                                    $('body').on('click', '.addsection', function () {

                                                        sectionsCount++;


                                                        const clone = $('#step2 .section').children().clone();


                                                       clone.find('#jogador_id').attr('name', `jogadorescasa[${sectionsCount}][jogador_id]`);

                                                        clone.find('#ausenteId').attr('name', `jogadorescasa[${sectionsCount}][ausente]`);
                                                        
                                                        clone.find('#gols').attr('name', `jogadorescasa[${sectionsCount}][gols]`);

                                                        clone.find('#cartao_amarelo').attr('name', `jogadorescasa[${sectionsCount}][cartao_amarelo]`);


                                                        clone.find('#cartao_vermelho').attr('name', `jogadorescasa[${sectionsCount}][cartao_vermelho]`);


                                                       $('#step2 #sections');

                                                       $('#step2 #sections').append(clone[0]);

                                                       return false;
                                                    });

                                                //remove section
                                                    $('#sections').on('click', '.remove', function () {
                                                        //fade out section
                                                        $(this).parent().fadeOut(300, function () {
                                                            //remove parent element (main section)
                                                            $(this).parent().empty();
                                                            return false;
                                                        });
                                                        return false;
                                                    });
                                                </script>
                                                <!-- Fim do Script -->






















                                                <!-- Jogadores do Time Adversário -->

                                                <div id="sections2">

                                                    <div class="section2">

<!-- Modelo Form Adversario -->

                                                        <!-- Modelo de Formulário Time Adversario --> 

                                                    <div class="form-group col-sm-12" id="formularioadversario">
                                                        <label for="jogador_id">Jogadores do Time Adversario:</label>

                                                        <select class="form-control" id="jogador_id_1" name="jogadoresadversario[1][jogador_id]" disabled="disabled">
                                                            <option></option>
                                                            @foreach($jogadores as $jogador)
                                                            <option value="{{$jogador->id}}" >{{$jogador->nome}}</option>
                                                            @endforeach


                                                        </select>

                                                        



                                                        <div class="form-check col-sm-12">
                                                            <input type="checkbox" class="form-check-input" id="ausente_1" value="0">
                                                            <label class="form-check-label" for="ausente">Jogador Ausente ? </label>
                                                        </div>

                                                        
                                                        <div class="form-group col-sm-3">
                                                            <label for="gols">Gols:</label>
                                                            <input type="number" class="form-control" id="gols"
                                                                   name="jogadoresadversario[1][gols]" min="0"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_amarelo">Cartões Amarelos:</label>
                                                            <input type="number" class="form-control" id="cartao_amarelo"
                                                                   name="jogadoresadversario[1][cartao_amarelo]" min="0" max="2"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_vermelho">Cartões Vermelhos:</label>
                                                            <input type="number" class="form-control" id="cartao_vermelho"
                                                                   name="jogadoresadversario[1][cartao_vermelho]" min="0" max="1"
                                                                   value="0">

                                                                   <input type="hidden" name="jogadoresadversario[1][ausente]" id="ausenteId_1">

                                                        </div>




<script>
    $('#ausente_1').on('change', function () {
        this.value = this.checked ? 1 : 0;
        $('#ausenteId_1').val(this.value);
    }).change();
</script>

</div>
<!-- Fim do Modelo --> 


<!-- Modelo de Formulário Time Adversario --> 

                                                    <div class="form-group col-sm-12" id="formularioadversario">
                                                        <label for="jogador_id">Jogadores do Time Adversario:</label>

                                                        <select class="form-control" id="jogador_id_2" name="jogadoresadversario[2][jogador_id]" disabled="disabled">
                                                            <option></option>
                                                            @foreach($jogadores as $jogador)
                                                            <option value="{{$jogador->id}}" >{{$jogador->nome}}</option>
                                                            @endforeach


                                                        </select>

                                                        



                                                        <div class="form-check col-sm-12">
                                                            <input type="checkbox" class="form-check-input" id="ausente_2" value="0">
                                                            <label class="form-check-label" for="ausente">Jogador Ausente ? </label>
                                                        </div>

                                                        
                                                        <div class="form-group col-sm-3">
                                                            <label for="gols">Gols:</label>
                                                            <input type="number" class="form-control" id="gols"
                                                                   name="jogadoresadversario[2][gols]" min="0"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_amarelo">Cartões Amarelos:</label>
                                                            <input type="number" class="form-control" id="cartao_amarelo"
                                                                   name="jogadoresadversario[2][cartao_amarelo]" min="0" max="2"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_vermelho">Cartões Vermelhos:</label>
                                                            <input type="number" class="form-control" id="cartao_vermelho"
                                                                   name="jogadoresadversario[2][cartao_vermelho]" min="0" max="1"
                                                                   value="0">

                                                                   <input type="hidden" name="jogadoresadversario[2][ausente]" id="ausenteId_2">

                                                        </div>




<script>
    $('#ausente_2').on('change', function () {
        this.value = this.checked ? 1 : 0;
        $('#ausenteId_2').val(this.value);
    }).change();
</script>

</div>
<!-- Fim do Modelo --> 


<!-- Modelo de Formulário Time Adversario --> 

                                                    <div class="form-group col-sm-12" id="formularioadversario">
                                                        <label for="jogador_id">Jogadores do Time Adversario:</label>

                                                        <select class="form-control" id="jogador_id_3" name="jogadoresadversario[3][jogador_id]" disabled="disabled">
                                                            <option></option>
                                                            @foreach($jogadores as $jogador)
                                                            <option value="{{$jogador->id}}" >{{$jogador->nome}}</option>
                                                            @endforeach


                                                        </select>

                                                        



                                                        <div class="form-check col-sm-12">
                                                            <input type="checkbox" class="form-check-input" id="ausente_3" value="0">
                                                            <label class="form-check-label" for="ausente">Jogador Ausente ? </label>
                                                        </div>

                                                        
                                                        <div class="form-group col-sm-3">
                                                            <label for="gols">Gols:</label>
                                                            <input type="number" class="form-control" id="gols"
                                                                   name="jogadoresadversario[3][gols]" min="0"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_amarelo">Cartões Amarelos:</label>
                                                            <input type="number" class="form-control" id="cartao_amarelo"
                                                                   name="jogadoresadversario[3][cartao_amarelo]" min="0" max="2"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_vermelho">Cartões Vermelhos:</label>
                                                            <input type="number" class="form-control" id="cartao_vermelho"
                                                                   name="jogadoresadversario[3][cartao_vermelho]" min="0" max="1"
                                                                   value="0">

                                                                   <input type="hidden" name="jogadoresadversario[3][ausente]" id="ausenteId_3">

                                                        </div>




<script>
    $('#ausente_3').on('change', function () {
        this.value = this.checked ? 1 : 0;
        $('#ausenteId_3').val(this.value);
    }).change();
</script>

</div>
<!-- Fim do Modelo --> 

<!-- Modelo de Formulário Time Adversario --> 

                                                    <div class="form-group col-sm-12" id="formularioadversario">
                                                        <label for="jogador_id">Jogadores do Time Adversario:</label>

                                                        <select class="form-control" id="jogador_id_4" name="jogadoresadversario[4][jogador_id]" disabled="disabled">
                                                            <option></option>
                                                            @foreach($jogadores as $jogador)
                                                            <option value="{{$jogador->id}}" >{{$jogador->nome}}</option>
                                                            @endforeach


                                                        </select>

                                                        



                                                        <div class="form-check col-sm-12">
                                                            <input type="checkbox" class="form-check-input" id="ausente_4" value="0">
                                                            <label class="form-check-label" for="ausente">Jogador Ausente ? </label>
                                                        </div>

                                                        
                                                        <div class="form-group col-sm-3">
                                                            <label for="gols">Gols:</label>
                                                            <input type="number" class="form-control" id="gols"
                                                                   name="jogadoresadversario[4][gols]" min="0"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_amarelo">Cartões Amarelos:</label>
                                                            <input type="number" class="form-control" id="cartao_amarelo"
                                                                   name="jogadoresadversario[4][cartao_amarelo]" min="0" max="2"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_vermelho">Cartões Vermelhos:</label>
                                                            <input type="number" class="form-control" id="cartao_vermelho"
                                                                   name="jogadoresadversario[4][cartao_vermelho]" min="0" max="1"
                                                                   value="0">

                                                                   <input type="hidden" name="jogadoresadversario[4][ausente]" id="ausenteId_4">

                                                        </div>




<script>
    $('#ausente_4').on('change', function () {
        this.value = this.checked ? 1 : 0;
        $('#ausenteId_4').val(this.value);
    }).change();
</script>

</div>
<!-- Fim do Modelo --> 

<!-- Modelo de Formulário Time Adversario --> 

                                                    <div class="form-group col-sm-12" id="formularioadversario">
                                                        <label for="jogador_id">Jogadores do Time Adversario:</label>

                                                        <select class="form-control" id="jogador_id_5" name="jogadoresadversario[5][jogador_id]" disabled="disabled">
                                                            <option></option>
                                                            @foreach($jogadores as $jogador)
                                                            <option value="{{$jogador->id}}" >{{$jogador->nome}}</option>
                                                            @endforeach


                                                        </select>

                                                        



                                                        <div class="form-check col-sm-12">
                                                            <input type="checkbox" class="form-check-input" id="ausente_5" value="0">
                                                            <label class="form-check-label" for="ausente">Jogador Ausente ? </label>
                                                        </div>

                                                        
                                                        <div class="form-group col-sm-3">
                                                            <label for="gols">Gols:</label>
                                                            <input type="number" class="form-control" id="gols"
                                                                   name="jogadoresadversario[5][gols]" min="0"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_amarelo">Cartões Amarelos:</label>
                                                            <input type="number" class="form-control" id="cartao_amarelo"
                                                                   name="jogadoresadversario[5][cartao_amarelo]" min="0" max="2"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_vermelho">Cartões Vermelhos:</label>
                                                            <input type="number" class="form-control" id="cartao_vermelho"
                                                                   name="jogadoresadversario[5][cartao_vermelho]" min="0" max="1"
                                                                   value="0">

                                                                   <input type="hidden" name="jogadoresadversario[5][ausente]" id="ausenteId_5">

                                                        </div>




<script>
    $('#ausente_5').on('change', function () {
        this.value = this.checked ? 1 : 0;
        $('#ausenteId_5').val(this.value);
    }).change();
</script>

</div>
<!-- Fim do Modelo --> 

<!-- Modelo de Formulário Time Adversario --> 

                                                    <div class="form-group col-sm-12" id="formularioadversario">
                                                        <label for="jogador_id">Jogadores do Time Adversario:</label>

                                                        <select class="form-control" id="jogador_id_6" name="jogadoresadversario[6][jogador_id]" disabled="disabled">
                                                            <option></option>
                                                            @foreach($jogadores as $jogador)
                                                            <option value="{{$jogador->id}}" >{{$jogador->nome}}</option>
                                                            @endforeach


                                                        </select>

                                                        



                                                        <div class="form-check col-sm-12">
                                                            <input type="checkbox" class="form-check-input" id="ausente_6" value="0">
                                                            <label class="form-check-label" for="ausente">Jogador Ausente ? </label>
                                                        </div>

                                                        
                                                        <div class="form-group col-sm-3">
                                                            <label for="gols">Gols:</label>
                                                            <input type="number" class="form-control" id="gols"
                                                                   name="jogadoresadversario[6][gols]" min="0"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_amarelo">Cartões Amarelos:</label>
                                                            <input type="number" class="form-control" id="cartao_amarelo"
                                                                   name="jogadoresadversario[6][cartao_amarelo]" min="0" max="2"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_vermelho">Cartões Vermelhos:</label>
                                                            <input type="number" class="form-control" id="cartao_vermelho"
                                                                   name="jogadoresadversario[6][cartao_vermelho]" min="0" max="1"
                                                                   value="0">

                                                                   <input type="hidden" name="jogadoresadversario[6][ausente]" id="ausenteId_6">

                                                        </div>




<script>
    $('#ausente_6').on('change', function () {
        this.value = this.checked ? 1 : 0;
        $('#ausenteId_6').val(this.value);
    }).change();
</script>

</div>
<!-- Fim do Modelo --> 

<!-- Modelo de Formulário Time Adversario --> 

                                                    <div class="form-group col-sm-12" id="formularioadversario">
                                                        <label for="jogador_id">Jogadores do Time Adversario:</label>

                                                        <select class="form-control" id="jogador_id_7" name="jogadoresadversario[7][jogador_id]" disabled="disabled">
                                                            <option></option>
                                                            @foreach($jogadores as $jogador)
                                                            <option value="{{$jogador->id}}" >{{$jogador->nome}}</option>
                                                            @endforeach


                                                        </select>

                                                        



                                                        <div class="form-check col-sm-12">
                                                            <input type="checkbox" class="form-check-input" id="ausente_7" value="0">
                                                            <label class="form-check-label" for="ausente">Jogador Ausente ? </label>
                                                        </div>

                                                        
                                                        <div class="form-group col-sm-3">
                                                            <label for="gols">Gols:</label>
                                                            <input type="number" class="form-control" id="gols"
                                                                   name="jogadoresadversario[7][gols]" min="0"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_amarelo">Cartões Amarelos:</label>
                                                            <input type="number" class="form-control" id="cartao_amarelo"
                                                                   name="jogadoresadversario[7][cartao_amarelo]" min="0" max="2"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_vermelho">Cartões Vermelhos:</label>
                                                            <input type="number" class="form-control" id="cartao_vermelho"
                                                                   name="jogadoresadversario[7][cartao_vermelho]" min="0" max="1"
                                                                   value="0">

                                                                   <input type="hidden" name="jogadoresadversario[7][ausente]" id="ausenteId_7">

                                                        </div>




<script>
    $('#ausente_7').on('change', function () {
        this.value = this.checked ? 1 : 0;
        $('#ausenteId_7').val(this.value);
    }).change();
</script>

</div>
<!-- Fim do Modelo --> 

<!-- Modelo de Formulário Time Adversario --> 

                                                    <div class="form-group col-sm-12" id="formularioadversario">
                                                        <label for="jogador_id">Jogadores do Time Adversario:</label>

                                                        <select class="form-control" id="jogador_id_8" name="jogadoresadversario[8][jogador_id]" disabled="disabled">
                                                            <option></option>
                                                            @foreach($jogadores as $jogador)
                                                            <option value="{{$jogador->id}}" >{{$jogador->nome}}</option>
                                                            @endforeach


                                                        </select>

                                                        



                                                        <div class="form-check col-sm-12">
                                                            <input type="checkbox" class="form-check-input" id="ausente_8" value="0">
                                                            <label class="form-check-label" for="ausente">Jogador Ausente ? </label>
                                                        </div>

                                                        
                                                        <div class="form-group col-sm-3">
                                                            <label for="gols">Gols:</label>
                                                            <input type="number" class="form-control" id="gols"
                                                                   name="jogadoresadversario[8][gols]" min="0"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_amarelo">Cartões Amarelos:</label>
                                                            <input type="number" class="form-control" id="cartao_amarelo"
                                                                   name="jogadoresadversario[8][cartao_amarelo]" min="0" max="2"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_vermelho">Cartões Vermelhos:</label>
                                                            <input type="number" class="form-control" id="cartao_vermelho"
                                                                   name="jogadoresadversario[8][cartao_vermelho]" min="0" max="1"
                                                                   value="0">

                                                                   <input type="hidden" name="jogadoresadversario[8][ausente]" id="ausenteId_8">

                                                        </div>




<script>
    $('#ausente_8').on('change', function () {
        this.value = this.checked ? 1 : 0;
        $('#ausenteId_8').val(this.value);
    }).change();
</script>

</div>
<!-- Fim do Modelo --> 

<!-- Modelo de Formulário Time Adversario --> 

                                                    <div class="form-group col-sm-12" id="formularioadversario">
                                                        <label for="jogador_id">Jogadores do Time Adversario:</label>

                                                        <select class="form-control" id="jogador_id_9" name="jogadoresadversario[9][jogador_id]" disabled="disabled">
                                                            <option></option>
                                                            @foreach($jogadores as $jogador)
                                                            <option value="{{$jogador->id}}" >{{$jogador->nome}}</option>
                                                            @endforeach


                                                        </select>

                                                        



                                                        <div class="form-check col-sm-12">
                                                            <input type="checkbox" class="form-check-input" id="ausente_9" value="0">
                                                            <label class="form-check-label" for="ausente">Jogador Ausente ? </label>
                                                        </div>

                                                        
                                                        <div class="form-group col-sm-3">
                                                            <label for="gols">Gols:</label>
                                                            <input type="number" class="form-control" id="gols"
                                                                   name="jogadoresadversario[9][gols]" min="0"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_amarelo">Cartões Amarelos:</label>
                                                            <input type="number" class="form-control" id="cartao_amarelo"
                                                                   name="jogadoresadversario[9][cartao_amarelo]" min="0" max="2"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_vermelho">Cartões Vermelhos:</label>
                                                            <input type="number" class="form-control" id="cartao_vermelho"
                                                                   name="jogadoresadversario[9][cartao_vermelho]" min="0" max="1"
                                                                   value="0">

                                                                   <input type="hidden" name="jogadoresadversario[9][ausente]" id="ausenteId_9">

                                                        </div>




<script>
    $('#ausente_9').on('change', function () {
        this.value = this.checked ? 1 : 0;
        $('#ausenteId_9').val(this.value);
    }).change();
</script>

</div>
<!-- Fim do Modelo --> 

<!-- Modelo de Formulário Time Adversario --> 

                                                    <div class="form-group col-sm-12" id="formularioadversario">
                                                        <label for="jogador_id">Jogadores do Time Adversario:</label>

                                                        <select class="form-control" id="jogador_id_10" name="jogadoresadversario[10][jogador_id]" disabled="disabled">
                                                            <option></option>
                                                            @foreach($jogadores as $jogador)
                                                            <option value="{{$jogador->id}}" >{{$jogador->nome}}</option>
                                                            @endforeach


                                                        </select>

                                                        



                                                        <div class="form-check col-sm-12">
                                                            <input type="checkbox" class="form-check-input" id="ausente_10" value="0">
                                                            <label class="form-check-label" for="ausente">Jogador Ausente ? </label>
                                                        </div>

                                                        
                                                        <div class="form-group col-sm-3">
                                                            <label for="gols">Gols:</label>
                                                            <input type="number" class="form-control" id="gols"
                                                                   name="jogadoresadversario[10][gols]" min="0"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_amarelo">Cartões Amarelos:</label>
                                                            <input type="number" class="form-control" id="cartao_amarelo"
                                                                   name="jogadoresadversario[10][cartao_amarelo]" min="0" max="2"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_vermelho">Cartões Vermelhos:</label>
                                                            <input type="number" class="form-control" id="cartao_vermelho"
                                                                   name="jogadoresadversario[10][cartao_vermelho]" min="0" max="1"
                                                                   value="0">

                                                                   <input type="hidden" name="jogadoresadversario[10][ausente]" id="ausenteId_10">

                                                        </div>




<script>
    $('#ausente_10').on('change', function () {
        this.value = this.checked ? 1 : 0;
        $('#ausenteId_10').val(this.value);
    }).change();
</script>

</div>
<!-- Fim do Modelo --> 

<!-- Modelo de Formulário Time Adversario --> 

                                                    <div class="form-group col-sm-12" id="formularioadversario">
                                                        <label for="jogador_id">Jogadores do Time Adversario:</label>

                                                        <select class="form-control" id="jogador_id_11" name="jogadoresadversario[11][jogador_id]" disabled="disabled">
                                                            <option></option>
                                                            @foreach($jogadores as $jogador)
                                                            <option value="{{$jogador->id}}" >{{$jogador->nome}}</option>
                                                            @endforeach


                                                        </select>

                                                        



                                                        <div class="form-check col-sm-12">
                                                            <input type="checkbox" class="form-check-input" id="ausente_11" value="0">
                                                            <label class="form-check-label" for="ausente">Jogador Ausente ? </label>
                                                        </div>

                                                        
                                                        <div class="form-group col-sm-3">
                                                            <label for="gols">Gols:</label>
                                                            <input type="number" class="form-control" id="gols"
                                                                   name="jogadoresadversario[11][gols]" min="0"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_amarelo">Cartões Amarelos:</label>
                                                            <input type="number" class="form-control" id="cartao_amarelo"
                                                                   name="jogadoresadversario[11][cartao_amarelo]" min="0" max="2"
                                                                   value="0">
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <label for="cartao_vermelho">Cartões Vermelhos:</label>
                                                            <input type="number" class="form-control" id="cartao_vermelho"
                                                                   name="jogadoresadversario[11][cartao_vermelho]" min="0" max="1"
                                                                   value="0">

                                                                   <input type="hidden" name="jogadoresadversario[11][ausente]" id="ausenteId_11">

                                                        </div>




<script>
    $('#ausente_11').on('change', function () {
        this.value = this.checked ? 1 : 0;
        $('#ausenteId_11').val(this.value);
    }).change();
</script>

</div>
<!-- Fim do Modelo --> 


<!--
                                                            <div class="col-sm-1">
                                                                <button type="" class="remove2 btn btn-block btn-danger btn-sm"> <b>-</b> </button>
                                                            </div>

                                                            <div class="col-sm-1">
                                                                <button type="" class="addsection2 btn btn-block btn-primary btn-sm"> <b>+</b> </button>
                                                            </div>


-->



                                                        </div>
                                                    </div>

                                                    <!-- Inicio do Script de Adição de Formulário -->

                                                    <script>



                                                        //define template
                                                        var template2 = $('#sections2 .section2:first').clone();

                                                    //define counter
                                                        var sectionsCount2 = 1;


                                                    //add new section
                                                        $('body').on('click', '.addsection2', function () {

                                                            sectionsCount2++;


                                                            const clone2 = $('#step2 .section2').clone();
                                                            clone2.find('#jogador_id_2').attr('name', `jogador_id_2[${sectionsCount2}]`);
                                                            clone2.find('#ausente2').attr('name', `ausente2[${sectionsCount2}]`);

                                                            clone2.find('#gols2').attr('name', `gols2[${sectionsCount2}]`);
                                                            clone2.find('#cartao_amarelo2').attr('name', `cartao_amarelo2[${sectionsCount2}]`);
                                                            clone2.find('#cartao_vermelho2').attr('name', `cartao_vermelho2[${sectionsCount2}]`);
                                                            $('#step2 #sections2').append(clone2[0]);

                                                            return false;
                                                        });


                                                    //remove section
                                                        $('#sections2').on('click', '.remove2', function () {
                                                            //fade out section
                                                            $(this).parent().fadeOut(300, function () {
                                                                //remove parent element (main section)
                                                                $(this).parent().empty();
                                                                return false;
                                                            });
                                                            return false;
                                                        });
                                                    </script>
                                                    <!-- Fim do Script -->







                                                </div>
                                                <div class="modal-footer">
                                                    <button type="submit" class="btn btn-primary">Salvar</button>
                                                    <button type="button" class="btn btn-dark" data-dismiss="modal">Fechar</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Fim da Modal Novo Jogo Beta -->


                                    @foreach($jogos as $jogo)


                                    <!-- Modal Desfazer Jogo -->

                                    <div class="modal fade" id="modalDesfazerJogo{{$jogo->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">

                                                    <h3 class="modal-title" id="exampleModalLabel"><b>Desfazer Jogo</b></h3>

                                                </div>
                                                <div class="modal-body">
                                                    <form method="post" action="{{route('desfazer.jogo.salvar', $jogo->id)}}">

                                                        {{ csrf_field() }}
                                                        <div class="form-group">
                                                            <label for="data_jogo">Data do Jogo:</label>
                                                            <input type="text" class="form-control" id="data_jogo"
                                                                   name="data_jogo"
                                                                   required value="{{$jogo->data_jogo or old('data_jogo')}}">
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="liga_id">Nome da Liga Que o Jogo Se Enquadra:</label>
                                                            <select class="form-control" id="liga_id" name="liga_id">
                                                                <option ></option>
                                                                @foreach($ligas as $liga)
                                                                <option name="liga_id" value="{{$liga->id}}" {{$liga->id == $jogo->liga_id ? 'selected': ''}}> {{$liga->nome_liga}} </option>
                                                                @endforeach
                                                            </select>
                                                        </div>




                                                        <div class="form-group">
                                                            <label for="time_casa_id">Time da Casa:</label>
                                                            <select class="form-control" id="time_id" name="time_casa_id">
                                                                <option></option>
                                                                @foreach($times as $time)
                                                                <option name="time_casa_id" value="{{$time->id}}" {{$time->id == $jogo->time_casa_id ? 'selected':''}} >{{$time->nome_time}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="placar_time_casa">Placar do Time da Casa:</label>
                                                            <input type="number" class="form-control" id="placar_time_casa"
                                                                   name="placar_time_casa"
                                                                   value="{{$jogo->placar_time_casa or old('placar_time_casa')}}"
                                                                   required>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="time_adversario_id">Time Adversário:</label>
                                                            <select class="form-control" id="time_id_2" name="time_adversario_id">
                                                                <option></option>
                                                                @foreach($times as $time)
                                                                <option name="time_adversario_id" value="{{$time->id}}" {{$time->id == $jogo->time_adversario_id ? 'selected':''}} >{{$time->nome_time}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="placar_time_adversario">Placar do Time Adversário:</label>
                                                            <input type="number" class="form-control" id="placar_time_adversario"
                                                                   name="placar_time_adversario"
                                                                   value="{{$jogo->placar_time_adversario or old('placar_time_adversario')}}"
                                                                   required>
                                                        </div>






                                                        <button type="submit" class="btn btn-primary">Salvar</button>
                                                        <button type="reset" class="btn btn-warning">Limpar</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Fim da Modal Desfazer Jogo -->


















                                    <tr>
                                        <td style="text-align: center">{{date('d/m/Y', strtotime($jogo->data_jogo))}}</td>
                                        <td style="text-align: center">{{$jogo->time_casa_nome->nome_time}}</td>
                                        <td style="text-align: center">{{$jogo->placar_time_casa}}</td>
                                        <th style="text-align: center">X</th>
                                        <td style="text-align: center">{{$jogo->placar_time_adversario}}</td>
                                        <td style="text-align: center">{{$jogo->time_adversario_nome->nome_time}}</td>
                                        <td style="text-align: center; color: #006400"><b>{{$jogo->ligas->nome_liga}}</b></td>



                                        <td>

                                            <img title="Desfazer Jogo" src="../icones/desfazer.png" width="30px" height="30px" data-toggle="modal" data-target="#modalDesfazerJogo{{$jogo->id}}" >

<a href="{{route('pdf.lista.dados.jogo', $jogo->id)}}"><img title="Desfazer Jogo" src="../icones/dados_jogo.png" width="30px" height="30px"></a>

                                             



                                        </td>
                                        @endforeach
                                    </tr>



                                    </tbody>
                                    </table>
                                    {{ $jogos->links() }}

                                    <h5>
                                        Legenda
                                    </h5>

                                    <p><img title="Desfazer Jogo" src="icones/desfazer.png" width="20px" height="20px"> Desfazer Jogo | <img title="Desfazer Jogo" src="icones/dados_jogo.png" width="20px" height="20px"> Relatório de Jogo </p>


                                </div>





                            </div>

                            <div class="clearfix"></div>
                        </div>
                    </div>

                </div>
                <br />













                @endsection