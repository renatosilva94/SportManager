@extends('dashboard')

@section('conteudo')


<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="dashboard_graph">

            <div class="row x_title">
                <div class="col-md-6">
                    <h3>Relatórios</h3>
                </div>
                <div class="col-md-6">

                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">

                <div class="col-md-12">
                    <div class="panel panel-body">



                        <div class="row">

                            <!--
        
                                  <div class="col-sm-4">
        
                                  <div class="tile-stats">
                          <div class="icon"><i class="fa fa-caret-square-o-right"></i></div>
                          <div class="count">179</div>
                          <h3>New Sign ups</h3>
                          <p>Lorem ipsum psdea itgum rixt.</p>
                        </div>
        
                      </div>
                            -->

                            <!-- top tiles -->

                            <div class="row tile_count">


                                <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
                                    <div class="right">
                                        <span class="count_top red"><i class="fa fa-male"></i> Seus Jogadores </span>
                                        <div class="count red">{{$contaJogadores}}</div>

                                    </div>
                                </div>
                                <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
                                    <div class="right">
                                        <span class="count_top blue"><i class="fa fa-sitemap"></i> Suas Ligas</span>
                                        <div class="count blue">{{$contaLigas}}</div>

                                    </div>
                                </div>
                                <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
                                    <div class="right">
                                        <span class="count_top green"><i class="fa fa-futbol-o"></i> Seus Times</span>
                                        <div class="count green"> {{$contaTimes}} </div>

                                    </div>
                                </div>
                                <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
                                    <div class="right">
                                        <span class="count_top purple"><i class="fa fa-arrows-alt"></i> Seus Jogos </span>
                                        <div class="count purple"> {{$contaJogos}} </div>

                                    </div>
                                </div>

                                <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
                                    <div class="right">
                                        <span class="count_top aero"><i class="fa fa-bullhorn"></i> Seus Eventos </span>
                                        <div class="count aero"> {{$contaEventos}} </div>

                                    </div>
                                </div>



                            </div>
                            <!-- /top tiles -->


                            <div class="row">


                                <div class="col-md-6">
                                    <div class="">
                                        <div class="">
                                            <h2>Jogos Recentes</h2>


                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">

                                            @foreach($jogos->take(3) as $jogo)

                                            <article class="media event">
                                                <a class="pull-left date">
                                                    <p class="month">{{\Carbon\Carbon::parse($jogo->data_jogo)->format('M')}} </p>
                                                    <p class="day">{{\Carbon\Carbon::parse($jogo->data_jogo)->format('d')}}</p>
                                                    <p class="month">{{\Carbon\Carbon::parse($jogo->data_jogo)->format('Y')}}</p>

                                                </a>
                                                <div class="media-body">

                                                    @if($jogo->placar_time_casa > $jogo->placar_time_adversario)
                                                    <a class="title" href="#">Vencedor: {{$jogo->time_casa_nome->nome_time}}</a>
                                                    @elseif($jogo->placar_time_casa < $jogo->placar_time_adversario)
                                                    <a class="title" href="#">Vencedor: {{$jogo->time_adversario_nome->nome_time}}</a>
                                                    @else
                                                    <a class="title" href="#">Vencedor: Empate</a>
                                                    @endif


                                                    <p> {{$jogo->time_casa_nome->nome_time}} <b>X</b> {{$jogo->time_adversario_nome->nome_time}} </p>
                                                    <p> {{$jogo->placar_time_casa}} <b>X</b> {{$jogo->placar_time_adversario}} </p>

                                                </div>
                                            </article>

                                            @endforeach

                                        </div>
                                    </div>
                                </div>





                                <div class="col-md-6">
                                    <div class="">
                                        <div class="">
                                            <h2>Eventos Próximos</h2>


                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">

                                            @foreach($eventos->take(3) as $evento)

                                            <article class="media event">
                                                <a class="pull-left date">
                                                    <p class="month">{{\Carbon\Carbon::parse($evento->data_inicial)->format('M')}} </p>
                                                    <p class="day">{{\Carbon\Carbon::parse($evento->data_inicial)->format('d')}}</p>
                                                    <p class="month">{{\Carbon\Carbon::parse($evento->data_inicial)->format('Y')}}</p>

                                                </a>
                                                <div class="media-body">

                                                    <p> {{$evento->nome_evento}} </p>
                                                    <p class="day">Término: {{\Carbon\Carbon::parse($evento->data_final)->format('d/m/Y')}}</p>

                                                </div>
                                            </article>

                                            @endforeach

                                        </div>
                                    </div>
                                </div>









                            </div>





                        </div>












                        <div class="clearfix"></div>
                    </div>









                </div>
            </div>

        </div>

        <div class="clearfix"></div>
    </div>
</div>

</div>
<br />

<!-- footer content -->




@endsection
