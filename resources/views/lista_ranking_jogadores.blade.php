@extends('dashboard')

@section('conteudo')



<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="dashboard_graph">

            <div class="row x_title">
                <div class="col-md-6">
                    <h3>Jogadores <small> Ranking </small></h3>
                </div>


                <br>
                <br>


                <div class="col-md-12">








                    <div class='col-sm-4'>
                        <label> &nbsp; </label>
                        <label> &nbsp; </label>


                    </div>

















                </div>







            </div>
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12">



            @if (count($jogadores)==0)
            <div class="alert alert-danger">
                Não existem dados no ranking.
            </div>
            @endif

            <table class="table table-striped table-bordered nowrap">
                <thead>
                    <tr>
                        <th style="text-align: center">Posição</th>

                        <th style="text-align: center">Foto</th>

                        <th style="text-align: center">Nome</th>
                        <th style="text-align: center">Sobrenome</th>

                        <th style="text-align: center; color: #ccac00"><b>Cartões Amarelos</b> <a href="{{url('lista.ranking.jogadores?order=cartao_amarelo')}}"  class="glyphicon glyphicon glyphicon-list"></a> </th>
                        <th style="text-align: center; color: #cc0000"><b>Cartões Vermelhos</b> <a href="{{url('lista.ranking.jogadores?order=cartao_vermelho')}}" class="glyphicon glyphicon glyphicon-list"></a> </th>
                        <th style="text-align: center; color: #120a8f"><b>Gols</b> <a href="{{url('lista.ranking.jogadores?order=gols')}}" class="glyphicon glyphicon-list"></a></th>
                        <th style="text-align: center; color: #007f00"><b>Ausencias</b> <a href="{{url('lista.ranking.jogadores?order=ausente')}}" class="glyphicon glyphicon-list"></a></th>


                    </tr>




                </thead>
                <tbody>

                    <?php $i = 0 ?>

                    @foreach($jogadores as $jogador)

                    <?php $i++ ?>






                    <tr>

                        <td style="text-align: center">{{$i}}</td>


                        <td style="text-align: center">


                            @php
                            if(file_exists(public_path('imagens_jogadores/'.$jogador->id.'.png'))){
                            $imagem_jogador = '../imagens_jogadores/'.$jogador->id.'.png';
                            } else {
                            $imagem_jogador = '../imagens_jogadores/sem_foto.png';
                            }
                            @endphp

                            {!!"<a href=$imagem_jogador data-lightbox='roadtrip' data-lightbox-gallery='gallery1' data-lightbox-hidpi=$imagem_jogador>"!!}
                                {!!"<img src=$imagem_jogador id='imagem' class='img-circle' width='50' height='50' alt='Imagem do Jogador' data-zoom-image=$imagem_jogador>"!!}
                            </a>

                        </td>







                        <td style="text-align: center">{{$jogador->nome}}</td>

                        <td style="text-align: center">{{$jogador->sobrenome}}</td>

                        <td style="text-align: center">{{$jogador->cartao_amarelo}}</td>

                        <td style="text-align: center">{{$jogador->cartao_vermelho}}</td>


                        <td style="text-align: center">{{$jogador->gols}}</td>

                        <td style="text-align: center">{{$jogador->ausente}}</td>






                        @endforeach
                    </tr>


                </tbody>
            </table>

            {{ $jogadores->links() }}



        </div>




    </div>

    <div class="clearfix"></div>
</div>
</div>

</div>
<br />


@endsection