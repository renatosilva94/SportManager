@extends('dashboard')

@section('conteudo')





<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="dashboard_graph">

            <div class="row x_title">
                <div class="col-md-6">
                    <h3>Times <small> Ranking </small></h3>
                </div>

                <br>
                <br>
                <div class="col-md-12">






                    <div class='col-sm-4'>
                        <label> &nbsp; </label>
                        <label> &nbsp; </label>


                    </div>









                </div>
            </div>

            <div class='col-sm-12'>

                @if (count($times)==0)
                <div class="alert alert-danger">
                    Não há dados no ranking...
                </div>
                @endif




                <table class="table table-striped table-bordered nowrap">
                    <thead>
                        <tr>
                            <th style="text-align: center">Posição</th>
                            <th style="text-align: center">Logo do Time</th>

                            <th style="text-align: center">Nome do Time</th>
                            <th style="text-align: center">Vitorias <a href="{{url('lista.ranking.times?order=vitoria')}}"  class="glyphicon glyphicon glyphicon-list"></a></th>
                            <th style="text-align: center">Empates <a href="{{url('lista.ranking.times?order=empate')}}"  class="glyphicon glyphicon glyphicon-list"></a></th>
                            <th style="text-align: center">Derrotas <a href="{{url('lista.ranking.times?order=derrota')}}"  class="glyphicon glyphicon glyphicon-list"></a></th>



                        </tr>
                    </thead>
                    <tbody>

                        <?php $i = 0 ?>


                        @foreach($times as $time)

                        <?php $i++ ?>
















                        <tr>

                            <td style="text-align: center">{{$i}}</td>


                            <td style="text-align: center">


                                @php
                                if(file_exists(public_path('imagens_times/'.$time->id.'.png'))){
                                $imagem_time = '../imagens_times/'.$time->id.'.png';
                                } else {
                                $imagem_time = '../imagens_times/sem_foto.png';
                                }
                                @endphp

                                {!!"<a href=$imagem_time data-lightbox='roadtrip' data-lightbox-gallery='gallery1' data-lightbox-hidpi=$imagem_time>"!!}
                                    {!!"<img src=$imagem_time id='imagem' class='img-circle' width='75' height='75' alt='Imagem do Time' data-zoom-image=$imagem_time>"!!}
                                </a>

                            </td>







                            <td style="text-align: center">{{$time->nome_time}}</td>
                            <td style="text-align: center"> {{$time->vitoria}} </td>
                            <td style="text-align: center"> {{$time->empate}} </td>
                            <td style="text-align: center"> {{$time->derrota}} </td>





                            @endforeach
                        </tr>


                    </tbody>
                </table>

                {{ $times->links() }}



            </div>




            @endsection