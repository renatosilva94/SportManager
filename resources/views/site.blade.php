<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <!-- Theme Made By www.w3schools.com - No Copyright -->
        <title>SportManager</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="{{ asset('css/inicio.css') }}">
    </head>
    <body>

        <!-- Navbar -->
        <nav class="navbar navbar-inverse">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>                        
                    </button>
                    <a class="navbar-brand" href="#">SportManager</a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="{{route('pagina.login')}}"><span class="glyphicon glyphicon-log-in"></span>Login</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- Primeiro Container -->
        <div class="container-fluid bg-1 text-center">
            <!--<img class="logo" src="" width="20%" height="20%" alt="Logo" >-->
            <h1>SportManager</h1>
            <h3>Gerencie suas partidas de futebol</h3>
            <p>&nbsp;</p>
        </div>

        <!-- Segundo Container (Grid) -->
        <div class="container-fluid bg-3 text-center">    
            <div class="row">
                <div class="col-sm-4">
                    <p>Gerencie Jogadores</p>
                    <i class="material-icons">person_add</i>
                </div>
                <div class="col-sm-4"> 
                    <p>Crie Partidas</p>
                    <i class="material-icons">group</i>
                </div>
                <div class="col-sm-4"> 
                    <p>Visualize Rankings</p>
                    <i class="material-icons">assignment</i>
                </div>
            </div>
        </div>

        <!-- Terceiro Container --> 

        <div class="container-fluid bg-5 text-center">    
            <div class="row">
                <div class="col-sm-3">
                    <p>Com o SportManager você pode criar suas próprias ligas e através atribuir times a elas.</p>


                </div>
                <div class="col-sm-3"> 
                    <p>Basta um cadastro para ter acesso a vários recursos disponíveis através de uma <i>dashboard.</i> </p>


                </div>
                <div class="col-sm-3"> 
                    <p>Alguns dos recursos disponíveis são o controle ligas, times, jogadores, subligas, ranking.</p>


                </div>
                <div class="col-sm-3"> 
                    <p>Tudo isso disponível de maneira gratuita para você que é arbitro ou gerencia um determinado jogo de futebol.</p>


                </div>
            </div>
        </div>

        <!-- Quarto Container (Grid) -->
        <div class="container-fluid bg-6 text-center">    
            <div class="row">
                <div class="col-sm-3">
                    <p>Ligas</p>
                    <i class="material-icons">zoom_out_map</i>
                    <p>{{$ligas}}</p>
                </div>
                <div class="col-sm-3">
                    <p>Times</p>
                    <i class="material-icons">public</i>
                    <p>{{$times}}</p>
                </div>
                <div class="col-sm-3"> 
                    <p>Usuarios</p>
                    <i class="material-icons">face</i>
                    <p>{{$usuarios}}</p>
                </div>
                <div class="col-sm-3"> 
                    <p>Jogadores</p>
                    <i class="material-icons">directions_walk</i>
                    <p>{{$jogadores}}</p>
                </div>
            </div>
        </div>





        <!-- Footer -->
        <footer class="container-fluid bg-4 text-center">

            <!-- Copyright -->
            <div class="footer-copyright text-center py-3">2018 SportManager 

            </div>
            <!-- Copyright -->

        </footer>
        <!-- Footer -->
        <!-- FIM -->



    </body>
</html>
