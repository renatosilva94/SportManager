@extends('dashboard')

@section('conteudo')




<a href="{{route('pdf.lista.placares', $id)}}" class="btn btn-info">Gerar Pdf <span class="glyphicon glyphicon-file" style="color:white"></span></a>







<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="dashboard_graph">

            <div class="row x_title">
                <div class="col-md-6">
                    <h3>Placares - {{$nome_liga}} <small> Listagem </small></h3>
                </div>
                <div class="col-md-6">

                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">



                <table class="table table-striped table-bordered nowrap">
                    <thead>
                        <tr>

                            <th>Classificação</th>
                            <th>Nome do Time</th>
                            <th>Pontos</th>
                            <th>Jogos</th>
                            <th>Vitórias</th>
                            <th>Empates</th>
                            <th>Derrotas</th>
                            <th>Gols Pró</th>
                            <th>Gols Contra</th>
                            <th>Saldo de Gols</th>



                        </tr>
                    </thead>
                    <tbody>


                        <?php $i = 0 ?>

                        @foreach($placares as $placar)
                        <?php $i++ ?>

                        <tr>
                            <td>{{$i}}</td>
                            <td>

                                @php
                                if(file_exists(public_path('imagens_times/'.$placar->times->id.'.png'))){
                                $imagem_time = '../imagens_times/'.$placar->times->id.'.png';
                                } else {
                                $imagem_time = '../imagens_times/sem_foto.png';
                                }
                                @endphp

                                {!!"<a href=$imagem_time data-lightbox='roadtrip' data-lightbox-gallery='gallery1' data-lightbox-hidpi=$imagem_time>"!!}
                                    {!!"<img src=$imagem_time id='imagem' class='img-circle' width='75' height='75' alt='Imagem do Time' data-zoom-image=$imagem_time>"!!}
                                </a>

                                {{$placar->times->nome_time}}


                            </td>
                            <td>{{$placar->pontos}}</td>
                            <td>{{$placar->jogos}}</td>
                            <td>{{$placar->vitorias}}</td>
                            <td>{{$placar->empates}}</td>
                            <td>{{$placar->derrotas}}</td>
                            <td>{{$placar->gols_pro}}</td>
                            <td>{{$placar->gols_contra}}</td>
                            <td>{{$placar->gols_pro - $placar->gols_contra}}</td>




                            @endforeach
                        </tr>



                    </tbody>
                </table>




            </div>




        </div>

        <div class="clearfix"></div>
    </div>
</div>

</div>
<br />


@endsection