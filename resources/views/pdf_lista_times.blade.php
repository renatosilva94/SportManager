<html>
    <head>

        <style>
            table {
                font-family: arial, sans-serif;
                border-collapse: collapse;
                width: 100%;
            }

            td, th {
                border: 1px solid #dddddd;
                text-align: left;
                padding: 8px;
                opacity: .7

            }

            tr:nth-child(even) {
                background-color: #dddddd;
            }

            #watermark {
                position: fixed;

                /** 
                    Set a position in the page for your image
                    This should center it vertically
                **/
                bottom:   10cm;
                left:     5.5cm;

                /** Change image dimensions**/
                width:    10cm;
                height:   8cm;

                /** Your watermark should be behind every content**/
                z-index:  -1000;

                opacity: .3

            }


        </style>

    </head>

    <h3 align="center">Times</h3>
    <p align="center">SportManager</p>
    <p align="center">Emissão: <?php date_default_timezone_set("America/Sao_Paulo");
echo date('d/m/Y H:i:s') ?></p>
    <br>
    <br>

    <body>

        <div id="watermark">
            <img src="https://i.imgur.com/BNUpuXa.png" height="100%" width="100%" />
        </div>

        <main>


            <div class='col-sm-12'>

                <table class="table table-hover" cellpadding="10">
                    <thead>
                        <tr>

                            <th style="text-align: center">Nome do Time</th>

                        </tr>
                    </thead>
                    <tbody>



                        @foreach($times as $time)

                        <tr>

                            <td>{{$time->nome_time}}</td>
                            @endforeach
                        </tr>


                    </tbody>
                </table>
        </main>
    </body>
</html>

</div>
