@extends('dashboard')

@section('conteudo')

<div class="modal fade" id="modalNovaLiga" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel"><b>Nova Liga</b></h3>
            </div>
            <div class="modal-body">
                <form method="post" action="{{route('salvar.liga')}}" enctype="multipart/form-data">

                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Nome da Liga:</label>
                        <input type="text" class="form-control" id="nome_liga" name="nome_liga">
                    </div>

                    <div class="form-group">
                        <label for="imagem_liga"> Imagem da Liga: </label>
                        <input type="file" id="imagem_liga" name="imagem_liga"
                               onchange="previewFile()"
                               class="form-control">
                    </div>


                    <div class="col-sm-6">

                        {!!"<img src='imagens_ligas/sem_foto.png' id='imagem_liga_preview' height='150px' width='150px' alt='Foto da Liga' class='img-circle'>"!!}

                    </div>

                    <script>
                        function previewFile() {
                            var preview = document.getElementById('imagem_liga_preview');
                            var file = document.getElementById('imagem_liga').files[0];
                            var reader = new FileReader();

                            reader.onloadend = function () {
                                preview.src = reader.result;
                            };

                            if (file) {
                                reader.readAsDataURL(file);
                            } else {
                                preview.src = "";
                            }
                        }

                    </script>



            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-dark" data-dismiss="modal">Fechar</button>
                <button type="submit" class="btn btn-primary">Salvar Liga</button>

                </form>

            </div>
        </div>
    </div>
</div>


































<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="dashboard_graph">

            <div class="row x_title">
                <div class="col-md-6">
                    <h3>Ligas <small> Listagem </small></h3>
                </div>

                <br>
                <br>
                <div class="col-md-12">



                    <div class='col-sm-4'>
                        <div class="form-group">
                            <input type="text" class="form-control" id="pesquisa_nome_liga"
                                   name="pesquisa_nome_liga" placeholder="Pesquisar (Nome da Liga)">
                        </div>
                    </div>

                    <script>
                        $('#pesquisa_nome_liga').on('keyup', function () {

                            $value = $(this).val();

                            $.ajax({

                                type: 'get',

                                url: '{{URL::to('buscaLigasAjax')}}',

                                data: {'buscaLigasAjax': $value},

                                success: function (data) {

                                    $('tbody').html(data);

                                }

                            });



                        })
                    </script>



                    <div class='col-sm-4'>
                        <label> &nbsp; </label>
                        <label> &nbsp; </label>












                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalNovaLiga">Nova Liga <span class="glyphicon glyphicon-plus" style="color:white"></span> </button>


                        <a href="{{route('pdf.lista.ligas')}}" class="btn btn-info">Gerar Pdf <span class="glyphicon glyphicon-file" style="color:white"></span></a>

                        <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modalRestaurarLigas">Restaurar Ligas <span class="glyphicon glyphicon-time" style="color:white"></span> </button>


                    </div>



                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">



                @if (count($ligas)==0)
                <div class="alert alert-danger">
                    Não existem ligas com os filtros informados.
                </div>
                @endif

                <table class="table table-striped table-bordered nowrap">
                    <thead>
                        <tr>
                            <th style="text-align: center">Nome da Liga</th>
                            <th style="text-align: center">Logotipo</th>
                            <th style="text-align: center">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($ligas as $liga)



                        <!-- Modal Editar Ligas -->

                    <div class="modal fade" id="modalEditarLiga{{$liga->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h3 class="modal-title" id="exampleModalLabel"><b>Editar Liga</b></h3>
                                </div>
                                <div class="modal-body">
                                    <form method="post" action="{{route('atualizar.liga', $liga->id)}}" enctype="multipart/form-data">

                                        {{ csrf_field() }}

                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Nome da Liga:</label>
                                            <input type="text" class="form-control" id="nome_liga" name="nome_liga" value="{{$liga->nome_liga or old('nome_liga')}}">
                                        </div>

                                        <div class="form-group">
                                            <label for="imagem_liga"> Imagem da Liga: </label>
                                            <input type="file" id="imagem_liga_editar" name="imagem_liga"
                                                   class="form-control">
                                                   <!-- onChange="previewFileEditar()" -->
                                        </div>


                                        <div class="col-sm-6">

                                            @php
                                            if(file_exists(public_path('imagens_ligas/'.$liga->id.'.png'))){
                                            $imagem_liga = '../imagens_ligas/'.$liga->id.'.png';
                                            } else {
                                            $imagem_liga = '../imagens_ligas/sem_foto.png';
                                            }
                                            @endphp

                                            {!!"<img src=$imagem_liga id='imagem_liga_preview_editar' height='150px' width='150px' alt='Foto da Liga' class='img-circle'>"!!}

                                        </div>



                                        <script>
                                            window.onload = () => {
                                                // console.log('asdasd', $('#imagem_liga_editar'));
                                                $('input[type="file"]').on('change', e => {
                                                    const activeModal = $('.modal.fade.in');
                                                    const preview = activeModal.find('#imagem_liga_preview_editar');
                                                    const file = e.target.files[0];
                                                    const reader = new FileReader();
                                                    
                                                    reader.onload = function () {
                                                        preview.attr('src', reader.result);
                                                    };
                                                    
                                                    if (file) {
                                                        reader.readAsDataURL(file);
                                                    } else {
                                                        preview.src = "";
                                                    }
                                                });
                                                
                                               
                                            }

                                        </script>



                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-dark" data-dismiss="modal">Fechar</button>
                                    <button type="submit" class="btn btn-primary">Atualizar Liga</button>

                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>




                    <!-- Fim da Modal Editar Ligas -->







                    <tr>
                        <td style="text-align: center">{{$liga->nome_liga}}</td>
                        <td style="text-align: center">


                            @php
                            if(file_exists(public_path('imagens_ligas/'.$liga->id.'.png'))){
                            $imagem_liga = '../imagens_ligas/'.$liga->id.'.png';
                            } else {
                            $imagem_liga = '../imagens_ligas/sem_foto.png';
                            }
                            @endphp

                            {!!"<a href=$imagem_liga data-lightbox='roadtrip' data-lightbox-gallery='gallery1' data-lightbox-hidpi=$imagem_liga>"!!}
                                {!!"<img src=$imagem_liga id='imagem' class='img-circle' width='50' height='50' alt='Imagem da Liga' data-zoom-image=$imagem_liga>"!!}
                            </a>

                        </td>


                        <td style="text-align: center">

                            
                            <form method="post"
                                  action="{{route('deletar.liga', $liga->id)}}"
                                  onsubmit="return confirm('Confirma Exclusão da Liga? Obs: Essa Ação Vai Excluir Os Jogos Que Envolvem Essa Liga !')">
                                {{method_field('delete')}}
                                {{csrf_field()}}



                                <input title="Deletar Liga" type="image" src="icones/deletar.png" width="30px" height="30px" >

                            </form>

<img title="Editar Liga" src="icones/editar.png" width="30px" height="30px" data-toggle="modal" data-target="#modalEditarLiga{{$liga->id}}" >











                        </td>
                        @endforeach

                        <div class="modal fade" id="modalRestaurarLigas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel" style="text-align: left" ><b>Restaurar Ligas</b></h3>
            </div>
            <div class="modal-body">
                
            @foreach($ligasDeletadas as $ligaDeletada)
                
                @if($ligaDeletada->trashed())
                <ul style="text-align: left;">
                <li>{{$ligaDeletada->nome_liga}}</li>
                <li>
                    
                <form method="post"
                                  action="{{route('restaurar.liga', $ligaDeletada->id)}}"
                                  onsubmit="return confirm('Deseja Restaurar Essa Liga ?')">

                                {{csrf_field()}}

                    
                    
                    <input type="submit" class="btn btn-dark btn-xs" value="Restaurar">
                    
                    
                    
                    
                    </li>
                </ul>
                @endif

            @endforeach

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-dark" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
                    </tr>


                    </tbody>
                </table>

                {{ $ligas->links() }}


                <h5>
                    Legenda
                </h5>

                <p><img title="Editar Liga" src="icones/editar.png" width="20px" height="20px"> Editar Liga | <img title="Deletar Liga" src="icones/deletar.png" width="20px" height="20px"> Deletar Liga</p>


            </div>

            <div class="clearfix"></div>
        </div>
    </div>

</div>
<br />


@endsection
