@extends('dashboard')

@section('conteudo')

<!-- Modal Novo Time -->

<div class="modal fade" id="modalNovoTime" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel"><b>Novo Time</b></h3>
            </div>
            <div class="modal-body">
                <form method="post" action="{{route('salvar.time')}}" enctype="multipart/form-data">

                    {{ csrf_field() }}





                    <div class="form-group">
                        <label for="nome_time">Nome do Time:</label>
                        <input type="text" class="form-control" id="nome_time"
                               name="nome_time"
                               value="{{$time->nome_time or old('nome_time')}}"
                               required>
                    </div>



                    <div class="form-group">
                        <label for="ligas_id">Ligas Vinculadas</label>
                        <select multiple class="form-control" id="ligas_id" name="ligas_id[]" required>
                            @foreach($ligas as $liga)
                            <option value="{{$liga->id}}" name="ligas_id"> {{$liga->nome_liga}}</option>
                            @endforeach

                        </select>
                    </div>

                    <div class="form-group">
                        <label for="imagem_liga"> Imagem do Time: </label>
                        <input type="file" id="imagem_time" name="imagem_time"
                               onchange="previewFile()"
                               class="form-control">
                    </div>




                    <div class="col-sm-6">

                        {!!"<img src='imagens_times/sem_foto.png' id='imagem_time_preview' alt='Foto do Time' width='150px' height='150px' class='img-circle'>"!!}

                    </div>




                    <div class="row">
                        <div class="col-sm-12">



                            <div class="form-group">
                                <label for="jogadores_id">Jogadores</label>
                                <select multiple class="form-control" id="jogadores_id" name="jogadores_id[]" required>
                                    @foreach($jogadores as $jogador)
                                    <option value="{{$jogador->id}}" name="jogadores_id"> {{$jogador->nome}}</option>
                                    @endforeach

                                </select>
                            </div>



                        </div>


                    </div>




                    <script>
                        function previewFile() {
                            var preview = document.getElementById('imagem_time_preview');
                            var file = document.getElementById('imagem_time').files[0];
                            var reader = new FileReader();

                            reader.onloadend = function () {
                                preview.src = reader.result;
                            };

                            if (file) {
                                reader.readAsDataURL(file);
                            } else {
                                preview.src = "";
                            }
                        }

                    </script>



            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-dark" data-dismiss="modal">Fechar</button>
                <button type="submit" class="btn btn-primary">Salvar Time</button>

                </form>

            </div>
        </div>
    </div>
</div>




<!-- Fim da Modal Novo Time -->










<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="dashboard_graph">

            <div class="row x_title">
                <div class="col-md-6">
                    <h3>Times <small> Listagem </small></h3>
                </div>

                <br>
                <br>
                <div class="col-md-12">




                    <div class='col-sm-4'>
                        <div class="form-group">
                            <input type="text" class="form-control" id="pesquisa_nome_time"
                                   name="pesquisa_nome_time" placeholder="Pesquisar (Nome do Time)">
                        </div>
                    </div>

                    <script>
                        $('#pesquisa_nome_time').on('keyup', function () {

                            $value = $(this).val();

                            $.ajax({

                                type: 'get',

                                url: '{{URL::to('buscaTimesAjax')}}',

                                data: {'buscaTimesAjax': $value},

                                success: function (data) {

                                    $('tbody').html(data);

                                }

                            });



                        })
                    </script>


                    <div class='col-sm-4'>
                        <label> &nbsp; </label>
                        <label> &nbsp; </label>

                        @if (count($jogadores)==0)

                        @elseif(count($ligas)==0)

                        @else
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalNovoTime">Novo Time <span class="glyphicon glyphicon-plus" style="color:white"></span></button>
                        @endif

                        <a href="{{route('pdf.lista.times')}}" class="btn btn-info">Gerar Pdf <span class="glyphicon glyphicon-file" style="color:white"></span></a>
                        
                        <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modalRestaurarTimes">Restaurar Times <span class="glyphicon glyphicon-time" style="color:white"></span> </button>


                    </div>









                </div>
            </div>

            <div class='col-sm-12'>

                @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                        <li>
                            {{$error}}
                        </li>
                        @endforeach
                    </ul>
                </div>
                @endif


                @if (count($times)==0)
                <div class="alert alert-danger">
                    Não há times com os filtros informados...
                </div>
                @endif

                @if (count($jogadores)==0)
                <div class="alert alert-info">
                    Não há jogadores cadastrados, antes de usar o cadastro de times você deve <b><a href="{{route('lista.jogadores')}}">cadastrar jogadores</a></b>...
                </div>
                @endif


                @if (count($ligas)==0)
                <div class="alert alert-success">
                    Não há ligas cadastradas, antes de usar o cadastro de times você deve <b><a href="{{route('lista.ligas')}}">cadastrar ligas</a></b>...
                </div>
                @endif

                <table class="table table-striped table-bordered nowrap">
                    <thead>
                        <tr>
                            <th>Logo do Time</th>

                            <th>Nome do Time</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>

                    <script>



                        $(document).ready(function () {

                            $('.next').click(function () {

                                var nextId = $(this).parents('.tab-pane').next().attr("id");
                                $('[href=#' + nextId + ']').tab('show');
                                return false;


                            })

                            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {

                                //update progress
                                var step = $(e.target).data('step');
                                var percent = (parseInt(step) / 3) * 100;

                                $('.progress-bar').css({width: percent + '%'});
                                $('.progress-bar').text("Etapa " + step + " de 3");

                                //e.relatedTarget // previous tab
                                const parent = $(e.currentTarget.closest('.navbar'));
                                parent.find(`a[href="#step${step}"]`).tab('show');

                                $('.tab-content .tab-pane').removeClass('active');
                                $('.tab-content #step' + step).addClass('active');

                                // console.log($('[href=#step' + step + ']').tab('show'));
                            });

                            $('.first').click(function () {

                                $('#myWizard a:first').tab('show')

                            });

                        });
                    </script>

                    @foreach($times as $time)









                    <!-- Modal Editar Informações dos Times (Informações ,Ligas, Jogadores) -->








                    <div id="modalEditarOutrasInformacoes{{$time->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel">Editar Informações do {{$time->nome_time}}</h3>
                                </div>
                                <div class="modal-body" id="myWizard">





                                    <div class="progress">
                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="4" style="width: 50%;">
                                            Etapa 1 de 3
                                        </div>
                                    </div>

                                    <div class="navbar">
                                        <div class="navbar-inner">
                                            <ul class="nav nav-pills">
                                                <li class="active"><a href="#step1" data-toggle="tab" data-step="1">Informações do Time</a></li>
                                                <li><a href="#step2" data-toggle="tab" data-step="2">Ligas</a></li>

                                                <li><a href="#step3" data-toggle="tab" data-step="3">Jogadores</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="tab-content">



                                        <div class="tab-pane fade in active" id="step1">

                                            <div class="">

                                                <form method="post" action="{{route('atualizar.time', $time->id)}}" enctype="multipart/form-data">

                                                    {{ csrf_field() }}





                                                    <div class="form-group">
                                                        <label for="nome_time">Nome do Time:</label>
                                                        <input type="text" class="form-control" id="nome_time"
                                                               name="nome_time"
                                                               value="{{$time->nome_time or old('nome_time')}}"
                                                               required>
                                                    </div>









                                                    <div class="form-group">
                                                        <label for="imagem_time"> Imagem do Time: </label>
                                                        <input type="file" id="imagem_time_editar" name="imagem_time"
                                                                   
                                                               class="form-control">
                                                    </div>




                                                    @php
                                                    if(file_exists(public_path('imagens_times/'.$time->id.'.png'))){
                                                    $imagem_time = '../imagens_times/'.$time->id.'.png';
                                                    } else {
                                                    $imagem_time = '../imagens_times/sem_foto.png';
                                                    }
                                                    @endphp

                                                    <div class="col-sm-6">

                                                        {!!"<img src=$imagem_time id='imagem_time_preview_editar' alt='Foto do Time' width='150px' height='150px' class='img-circle'>"!!}

                                                    </div>

<script>
                                            window.onload = () => {
                                                // console.log('asdasd', $('#imagem_liga_editar'));
                                                $('input[type="file"]').on('change', e => {
                                                    const activeModal = $('.modal.fade.in');
                                                    const preview = activeModal.find('#imagem_time_preview_editar');
                                                    const file = e.target.files[0];
                                                    const reader = new FileReader();
                                                    
                                                    reader.onload = function () {
                                                        preview.attr('src', reader.result);
                                                    };
                                                    
                                                    if (file) {
                                                        reader.readAsDataURL(file);
                                                    } else {
                                                        preview.src = "";
                                                    }
                                                });
                                                
                                               
                                            }

                                        </script>







                                                    <button type="submit" class="btn btn-primary">Salvar Time</button>
                                                    <button type="button" class="btn btn-dark" data-dismiss="modal">Fechar</button>

                                                </form>



                                            </div>
                                        </div>












                                        <div class="tab-pane fade in" id="step2">

                                            <div class="">


                                                <form method="post" action="{{route('remover.time.liga', $time->id)}}">



                                                    {{ csrf_field() }}


                                                    <div class="form-group">
                                                        <label for="ligas_id">Remover Ligas</label>
                                                        <select multiple class="form-control" id="ligas_id" name="ligas_id[]">




                                                            @foreach($time->ligas as $liga)

                                                            <option name="ligas_id" value="{{$liga->id}}"> {{$liga->nome_liga}} </option>

                                                            @endforeach






                                                        </select>




                                                    </div>

                                                    <button type="submit" class="btn btn-danger">Remover Ligas</button>


                                                </form>































                                                <form method="post" action="{{route('atualizar.time.liga', $time->id)}}">



                                                    {{ csrf_field() }}


                                                    <div class="form-group">
                                                        <label for="ligas_id">Adicionar Ligas</label>
                                                        <select multiple class="form-control" id="ligas_id" name="ligas_id[]">




                                                            @foreach($ligas as $liga)

                                                            <option name="ligas_id" value="{{$liga->id}}"> {{$liga->nome_liga}} </option>

                                                            @endforeach






                                                        </select>




                                                    </div>

                                                    <button type="submit" class="btn btn-success">Atualizar Ligas</button>
                                                    <button type="button" class="btn btn-dark" data-dismiss="modal">Fechar</button>


                                                </form>



                                            </div>


                                        </div>

                                        <div class="tab-pane fade in" id="step3">

                                            <div class="">









                                                <form method="post" action="{{route('remover.time.jogador', $time->id)}}">



                                                    {{ csrf_field() }}




                                                    <div class="form-group">
                                                        <label for="jogadores_id">Remover Jogadores</label>
                                                        <select multiple class="form-control" id="jogadores_id" name="jogadores_id[]">
                                                            @foreach($time->jogadores as $jogador)
                                                            <option value="{{$jogador->id}}" name="jogadores_id"> {{$jogador->nome}}</option>
                                                            @endforeach

                                                        </select>
                                                    </div>



                                                    <button type="submit" class="btn btn-danger">Remover Jogadores</button>

                                                </form>






























                                                <form method="post" action="{{route('atualizar.time.jogador', $time->id)}}">



                                                    {{ csrf_field() }}




                                                    <div class="form-group">
                                                        <label for="jogadores_id">Adicionar Jogadores</label>
                                                        <select multiple class="form-control" id="jogadores_id" name="jogadores_id[]">
                                                            @foreach($jogadores as $jogador)
                                                            <option value="{{$jogador->id}}" name="jogadores_id"> {{$jogador->nome}}</option>
                                                            @endforeach

                                                        </select>
                                                    </div>


                                            </div>

                                            <button type="submit" class="btn btn-success">Atualizar Jogadores</button>
                                            <button type="button" class="btn btn-dark" data-dismiss="modal">Fechar</button>

                                            </form>

                                        </div>
                                    </div>

                                    <div class="modal-footer">


                                    </div>
                                </div>
                            </div>


                            <!-- Fim da Modal de Editar Informações dos Times -->









































































































































                            <tr>



                                <td style="text-align: center">


                                    @php
                                    if(file_exists(public_path('imagens_times/'.$time->id.'.png'))){
                                    $imagem_time = '../imagens_times/'.$time->id.'.png';
                                    } else {
                                    $imagem_time = '../imagens_times/sem_foto.png';
                                    }
                                    @endphp

                                    {!!"<a href=$imagem_time data-lightbox='roadtrip' data-lightbox-gallery='gallery1' data-lightbox-hidpi=$imagem_time>"!!}
                                        {!!"<img src=$imagem_time id='imagem' class='img-circle' width='50' height='50' alt='Imagem do Time' data-zoom-image=$imagem_time>"!!}
                                    </a>

                                </td>







                                <td style="text-align: center">{{$time->nome_time}}</td>




                                <td style="text-align: center">


                                    <form
                                        method="post"
                                        action="{{route('deletar.time', $time->id)}}"
                                        onsubmit="return confirm('Confirma Exclusão do Time?  Obs: Essa Ação Vai Excluir Os Jogos Que Envolvem Esse Time !')">
                                        {{method_field('delete')}}
                                        {{csrf_field()}}

                                        <input title="Deletar Time" type="image" src="icones/deletar.png" width="30px" height="30px" >



                                    </form>







                                    <img title="Editar Outras Informações" src="icones/editar_extras.png" width="30px" height="30px" data-toggle="modal" data-target="#modalEditarOutrasInformacoes{{$time->id}}">








                                </td>
                                @endforeach

                                <div class="modal fade" id="modalRestaurarTimes" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel" style="text-align: left" ><b>Restaurar Times</b></h3>
            </div>
            <div class="modal-body">
                
            @foreach($timesDeletados as $timeDeletado)
                
                @if($timeDeletado->trashed())
                <ul style="text-align: left;">
                <li>{{$timeDeletado->nome_time}}</li>
                <li>
                    
                <form method="post"
                                  action="{{route('restaurar.time', $timeDeletado->id)}}"
                                  onsubmit="return confirm('Deseja Restaurar Essa Time ?')">

                                {{csrf_field()}}

                    
                    
                    <input type="submit" class="btn btn-dark btn-xs" value="Restaurar">
                    
                    
                    
                    
                    </li>
                </ul>
                @endif

            @endforeach

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-dark" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
                            </tr>


                            </tbody>
                            </table>

                            {{ $times->links() }}


                            <h5>
                                Legenda
                            </h5>

                            <p><img title="Editar Informações do Time" src="icones/editar_extras.png" width="20px" height="20px"> Editar Informações do Time | <img title="Deletar Time" src="icones/deletar.png" width="20px" height="20px"> Deletar Time </p>


                        </div>




                        @endsection
