<?php

use Illuminate\Database\Seeder;
use App\Sexos;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
                $this->call('SexosTableSeeder');

    }
}
