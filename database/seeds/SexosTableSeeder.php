<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class SexosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sexos')->insert([
            'nome_sexo' => 'Feminino'
        ]);

        DB::table('sexos')->insert([
            'nome_sexo' => 'Masculino'
        ]);

        DB::table('sexos')->insert([
            'nome_sexo' => 'Outro'
        ]);



    }
}
