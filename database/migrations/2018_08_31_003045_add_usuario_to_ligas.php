<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsuarioToLigas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
                 Schema::table('ligas', function (Blueprint $table) {

         $table->integer('usuario_id')->nullable()->unsigned();

            $table->foreign('usuario_id')
                    ->references('id')->on('usuarios')
                    ->onDelete('cascade');

                            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
