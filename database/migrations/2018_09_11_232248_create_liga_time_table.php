<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLigaTimeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('liga_time', function (Blueprint $table) {
          
            $table->integer('ligas_id')->unsigned();
            $table->integer('times_id')->unsigned();

           // $table->primary(['time_id', 'jogador_id']);

            $table->foreign('times_id')->references('id')->on('times')->onUpdated('cascade')->onDelete('cascade');

        $table->foreign('ligas_id')->references('id')->on('ligas')->onUpdated('cascade')->onDelete('cascade');



            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('liga_time');
    }
}
