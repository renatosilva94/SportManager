<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlacaresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('placares', function (Blueprint $table) {
            $table->increments('id');
            $table->softDeletes();
            $table->integer('pontos');
                    $table->integer('jogos');
                                $table->integer('vitorias');
                                            $table->integer('derrotas');
                                                        $table->integer('empates');
                                                                                $table->integer('gols_pro');
                                                                                            $table->integer('gols_contra');
                                                                                                        $table->integer('saldo_gols');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('placares');
    }
}
