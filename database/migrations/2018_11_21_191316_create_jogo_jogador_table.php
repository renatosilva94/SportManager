<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJogoJogadorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jogo_jogador', function (Blueprint $table) {
            

            $table->integer('jogos_id')->unsigned();
            $table->integer('jogadores_id')->unsigned();
            $table->integer('gols')->unsigned();
            $table->integer('cartao_amarelo')->unsigned();
            $table->integer('cartao_vermelho')->unsigned();
            $table->integer('ausente')->unsigned();
            

           // $table->primary(['time_id', 'jogador_id']);

            $table->foreign('jogos_id')->references('id')->on('jogos')->onUpdated('cascade')->onDelete('cascade');

            $table->foreign('jogadores_id')->references('id')->on('jogadores')->onUpdated('cascade')->onDelete('cascade');
            


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jogo_jogador');
    }
}
