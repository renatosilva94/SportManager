# SportManager - Controle para árbitros de futebol


---

## Instalação

1 - Efetuar o Clone do Projeto na pasta htdocs do xampp.

```sh
git clone https://gitlab.com/renatosilva94/SportManager.git
```
   
2 - Acessar o PhpMyAdmin e criar uma tabela chamada sportmanager com padrão utf8_general_ci.
   
3 - Abrir o CMD e navegar até a pasta do projeto.
   
```sh
cd C:\xampp\htdocs\PASTA_DO_PROJETO'
```
  
4 - Executar o seguinte comando quando estiver na pasta do projeto  

```sh
php artisan serve
```
  
4 - Depois de executado os comandos acima deve ser aberta uma nova janela no CMD e navegar novamente até a pasta do projeto
  
5 - Caso aconteça o erro "failed opening required bootstrap" execute o seguinte comando  

```sh
composer update --no-scripts  
```
  
6 - Executar o comando de criação de tabelas

```sh
php artisan migrate
```

7 - Executar o comando de população de tabelas

```sh
php artisan db:seed
```  
  
8 - Acessar um navegador, como o Google Chrome e navegar até o seguinte endereço

```sh
localhost:8000/
```

## Projeto Web

```sh
https://sportmanager.herokuapp.com/pagina.login
```

---



## Licença

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

Projeto licenciado de acordo com a licença Apache 2.0.
