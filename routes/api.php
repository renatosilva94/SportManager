<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/

Route::group([    
    'namespace' => 'Auth',    
    'middleware' => 'api',    
    'prefix' => 'password'
], function () {    
    Route::post('enviarEmailRecuperacao', 'RecuperarSenhaController@enviarEmailRecuperacao');
    Route::get('encontrarToken/{token}', 'RecuperarSenhaController@encontrarToken');
    Route::post('resetarSenha', 'RecuperarSenhaController@resetarSenha');
});
