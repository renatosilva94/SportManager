<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Auth::routes();

/*Rota Inicial*/
Route::get('/', function () {
    //return view('pagina_login');
    return redirect('pagina.site');
});

/*Página Inicial do Site*/
Route::get('pagina.site', 'ViewController@paginaInicialSite')->name('pagina.site');

/*Página Inicial*/
Route::get('pagina.inicial', 'ViewController@paginaInicialDashboard')->name('pagina.inicial');

/*Página de Login*/
Route::get('pagina.login', 'ViewController@paginaLogin')->name('pagina.login');

/*Página de Recuperação de Senha*/
Route::get('pagina.recuperar.senha', 'ViewController@paginaRecuperarSenha')->name('pagina.recuperar.senha');

/*Pagina de Setar Nova Senha*/
Route::get('pagina.nova.senha/{token}', 'ViewController@paginaNovaSenha')->name('pagina.nova.senha');

/*Listagem de Ligas*/
Route::get('lista.ligas', 'LigasController@paginaListaLigas')->name('lista.ligas');

/*Salvar Liga*/
Route::post('salvar.liga', 'LigasController@salvarLiga')->name('salvar.liga');

/*Listagem de Times*/
Route::get('lista.times', 'TimesController@paginaListaTimes')->name('lista.times');

/*Salvar Time*/
Route::post('salvar.time', 'TimesController@salvarTime')->name('salvar.time');

/*Listagem de Jogadores*/
Route::get('lista.jogadores', 'JogadoresController@paginaListaJogadores')->name('lista.jogadores');

/*Salvar Jogador*/
Route::post('salvar.jogador', 'JogadoresController@salvarJogador')->name('salvar.jogador');

/*Adicionar Cartão Vermelho*/
Route::get('adicionar.cartao.vermelho/{id}', 'JogadoresController@adicionarCartaoVermelho')->name('adicionar.cartao.vermelho');

/*Remover Cartão Vermelho*/
Route::get('remover.cartao.vermelho/{id}', 'JogadoresController@removerCartaoVermelho')->name('remover.cartao.vermelho');

/*Adicionar Cartão Amarelo*/
Route::get('adicionar.cartao.amarelo/{id}', 'JogadoresController@adicionarCartaoAmarelo')->name('adicionar.cartao.amarelo');

/*Remover Cartão Amarelo*/
Route::get('remover.cartao.amarelo/{id}', 'JogadoresController@removerCartaoAmarelo')->name('remover.cartao.amarelo');

/*Remover e Adicionar Ausencia*/
Route::get('adicionar.ausencia/{id}', 'JogadoresController@adicionarAusencia')->name('adicionar.ausencia');
Route::get('remover.ausencia/{id}', 'JogadoresController@removerAusencia')->name('remover.ausencia');


/*Listagem de Placares por ID*/
Route::get('lista.placares/{id}', 'PlacaresController@paginaListaPlacares')->name('lista.placares');

/*Salvar Usuario*/
Route::post('cadastro.efetuar', 'UsuariosController@efetuarCadastro')->name('cadastro.efetuar');

/*Efetuar o Login*/

Route::post('login.efetuar', 'UsuariosController@efetuarLogin')->name('login.efetuar');



/*Efetuar o Logout*/
Route::get('efetuar.logout', 'UsuariosController@efetuarLogout')->name('efetuar.logout');

/*Salvar Foto*/
Route::post('salvar.foto.perfil', 'UsuariosController@salvarFoto')->name('salvar.foto.perfil');

/*Listagem de Jogos*/
Route::get('lista.jogos', 'JogosController@paginaListaJogos')->name('lista.jogos');

/*Salvar Jogo*/
Route::post('salvar.jogo', 'JogosController@salvarJogo')->name('salvar.jogo');

/*Ajax Pegar Times por Liga*/
Route::get('ajax/pegar-times-liga', 'TimesController@getTimesPorLigaAjax');

/*Ajax Pegar Jogadores por Time*/
Route::get('ajax/pegar-jogadores-time', 'TimesController@getJogadoresPorTimeAjax');

/* Ajax Pegar Jogadores Advesarios por Time */
Route::get('ajax/pegar-jogadores-adversarios-time', 'TimesController@getJogadoresAdversariosPorTimeAjax');

/*Listagem da Agenda*/
Route::get('lista.agendas', 'EventosController@paginaListaAgenda')->name('lista.agendas');
Route::post('salvar.agenda', 'EventosController@salvarAgenda')->name('salvar.agenda');


/*Listagem de Placares em PDF por ID*/
Route::get('pdf.lista.placares/{id}', 'PlacaresController@pdfListarPlacares')->name('pdf.lista.placares');

/*Listagem de Ligas em PDF*/
Route::get('pdf.lista.ligas', 'LigasController@pdfListarLigas')->name('pdf.lista.ligas');

/*Listagem de Times em PDF*/
Route::get('pdf.lista.times', 'TimesController@pdfListarTimes')->name('pdf.lista.times');

/*Listagem de Jogos em PDF*/
Route::get('pdf.lista.jogos', 'JogosController@pdfListarJogos')->name('pdf.lista.jogos');

/*Listagem de Jogadores em PDF */
Route::get('pdf.lista.jogadores', 'JogadoresController@pdfListarJogadores')->name('pdf.lista.jogadores');

/*Listagem de Dados de Jogo em PDF */
Route::get('pdf.lista.dados.jogo/{id}', 'JogosController@pdfListaDadosJogo')->name('pdf.lista.dados.jogo');


/*Desfazer Jogo*/
Route::post('desfazer.jogo.salvar/{id}', 'JogosController@desfazerJogoSalvar')->name('desfazer.jogo.salvar');

/*Atualizar Ligas*/

Route::post('atualizar.liga/{id}', 'LigasController@atualizarLiga')->name('atualizar.liga');

/*Atualizar Times*/
Route::post('atualizar.time/{id}', 'TimesController@atualizarTime')->name('atualizar.time');
Route::post('atualizar.time.liga/{id}', 'TimesController@atualizarTimeLiga')->name('atualizar.time.liga');
Route::post('atualizar.time.jogador/{id}', 'TimesController@atualizarTimeJogador')->name('atualizar.time.jogador');
Route::post('remover.time.liga/{id}', 'TimesController@removerTimeLiga')->name('remover.time.liga');
Route::post('remover.time.jogador/{id}', 'TimesController@removerTimeJogador')->name('remover.time.jogador');


/*Atualizar Meu Perfil*/
Route::post('atualizar.meu.perfil/{id}', 'UsuariosController@atualizarMeuPerfil')->name('atualizar.meu.perfil');



/*Atualizar Jogador*/

Route::post('atualizar.jogador/{id}', 'JogadoresController@atualizarJogador')->name('atualizar.jogador');

/*Convidar Jogadores E-Mail*/
//Route::post('convidar.jogadores', 'EmailController@convidarJogadores');

/*Listagem do Ranking*/
Route::get('lista.ranking.jogadores', 'JogadoresController@paginaListaRankingJogadores')->name('lista.ranking.jogadores');

Route::get('lista.ranking.times', 'TimesController@paginaListaRankingTimes')->name('lista.ranking.times');

/*Adicionar Cartão Vermelho*/
Route::get('adicionar.gol/{id}', 'JogadoresController@adicionarGol')->name('adicionar.gol');

/*Remover Cartão Vermelho*/
Route::get('remover.gol/{id}', 'JogadoresController@removerGol')->name('remover.gol');

/*Deletar Liga*/
Route::delete('deletar.liga/{id}', 'LigasController@deletarLiga')->name('deletar.liga');

/* Restaurar Liga */
Route::post('restaurar.liga/{id}', 'LigasController@restaurarLiga')->name('restaurar.liga');


/*Deletar Time*/
Route::delete('deletar.time/{id}', 'TimesController@deletarTime')->name('deletar.time');

/* Restaurar Time */
Route::post('restaurar.time/{id}', 'TimesController@restaurarTime')->name('restaurar.time');


/*Deletar Jogador*/
Route::delete('deletar.jogador/{id}', 'JogadoresController@deletarJogador')->name('deletar.jogador');

/* Restaurar Time */
Route::post('restaurar.jogador/{id}', 'JogadoresController@restaurarJogador')->name('restaurar.jogador');


/*Salvar Jogo Aleatório*/
Route::post('sortear.jogo', 'JogosController@sortearJogos')->name('sortear.jogo');

/*Buscar Ligas AJAX Pagina*/
Route::get('/buscaLigasAjax', 'BuscaAjaxController@buscaLigasAjax');

/*Buscar Times AJAX Pagina*/
Route::get('/buscaTimesAjax', 'BuscaAjaxController@buscaTimesAjax');

/* Buscar Jogadores AJAX Pagina */
Route::get('/buscaJogadoresAjax', 'BuscaAjaxController@buscaJogadoresAjax');

/*Rotas Redes Sociais*/
Route::get('auth/{provider}', 'UsuariosController@redirectToProvider');
Route::get('auth/{provider}/callback', 'UsuariosController@handleProviderCallback');


Route::delete('deletar.compromisso/{id}', 'EventosController@deletarCompromisso')->name('deletar.compromisso');
